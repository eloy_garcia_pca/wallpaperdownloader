/**
 * Copyright 2016-2020 Eloy García Almadén <eloy.garcia.pca@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.estoes.wallpaperDownloader.changer;
import java.io.File;
import java.io.IOException;

public class MacOSWallpaperChanger extends WallpaperChanger {
	
	// Constants

	// Attributes

	// Getters & Setters
	
	// Methods
	/**
	 * Constructor
	 */
	public MacOSWallpaperChanger () {
		super();
	}

	@Override
	public void setWallpaper(String wallpaperPath) {
		File wallpaper = new File(wallpaperPath);
		if (wallpaper.exists() && !wallpaper.isDirectory()) {
		    String commandToChangeWallpaper[] = {
		            "osascript", 
		            "-e", "tell application \"Finder\"", 
		            "-e", "set desktop picture to POSIX file \"" + wallpaperPath + "\"",
		            "-e", "end tell"
		    };
		    Runtime runtime = Runtime.getRuntime();
		    try {
				runtime.exec(commandToChangeWallpaper);
			} catch (IOException exception) {
				if (LOG.isInfoEnabled()) {
					LOG.error("Error changing the wallpaper: " + exception.getMessage());
				}
			}	
		} else {
			if (LOG.isInfoEnabled()) {
				LOG.error("Wallpaper " + wallpaperPath + " doesn't exist and can't be set. Skipping...");
			}
		}		
	}

	@Override
	public boolean isWallpaperChangeable() {
		return true;
	}
}