/**
 * Copyright 2016-2021 Eloy García Almadén <eloy.garcia.pca@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.estoes.wallpaperDownloader.util;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.log4j.Logger;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import es.estoes.wallpaperDownloader.changer.LinuxWallpaperChanger;
import es.estoes.wallpaperDownloader.changer.WallpaperChanger;
import es.estoes.wallpaperDownloader.window.DialogManager;
import es.estoes.wallpaperDownloader.window.WallpaperDownloader;
import org.apache.commons.io.comparator.*;

/**
 * This class gathers all the utilities needed for the correct behavior of the application. 
 * It implements a kind of Singleton pattern not pure and only based on static methods. 
 * Actually, there won't be an object of this class ever 
 * @author egarcia
 *
 */
public class WDUtilities {

	// Constants
	protected static final Logger LOG = Logger.getLogger(WDUtilities.class);
	public static final String APP_YES = "yes";
	public static final String APP_NO = "no";
	public static final String PROVIDER_SEPARATOR = ";";
	public static final String QM = "?";
	public static final String EQUAL = "=";
	public static final String AND = "&";
	public static final String URL_SLASH = "/";
	public static final String UNDERSCORE = "_";
	public static final String PERIOD = ".";
	public static final String DASH = "-";
	public static final String COLON = ":";
	public static final String WD_PREFIX = "wd-";
	public static final String DEFAULT_DOWNLOADS_DIRECTORY = "downloads";
	public static final String UNIT_MB = "MB";
	public static final String UNIT_KB = "KB";
	public static final String WD_FAVORITE_PREFIX = "fwd-";
	public static final String WD_ALL = "all";
	public static final int STATUS_CODE_200 = 200;
	public static final CharSequence SNAP_KEY = "snap";
	public static final String OS_LINUX = "Linux";
	public static final String OS_WINDOWS = "Windows";
	public static final String OS_WINDOWS_7 = "Windows 7";
	public static final String OS_WINDOWS_10 = "Windows 10";
	public static final String OS_MACOSX = "Mac OS X";
	public static final String OS_UNKNOWN = "UNKNOWN";
	public static final String DE_UNITY = "Unity";
	public static final String DE_GNOME = "GNOME";
	public static final String DE_KDE = "KDE";
	public static final String DE_MATE = "MATE";
	public static final String DE_XFCE = "XFCE";
	public static final String DE_UNKNOWN = "UNKNOWN";
	public static final String DE_GNOME3 = "GNOME3";
	public static final String DE_CINNAMON = "Cinnamon";
	public static final String DE_PANTHEON = "Pantheon";
	public static final String DE_DEEPIN = "Deepin";
	public static final String DOWNLOADS_DIRECTORY = "downloads_directory";
	public static final String CHANGER_DIRECTORY = "changer_directory";
	public static final String MOVE_DIRECTORY = "move_directory";
	public static final String PLASMA_SCRIPT = "plasma-changer.sh";
	public static final String SCRIPT_LOCATION = "/scripts/";
	public static final String IMG_JPG_SUFFIX = "jpg";
	public static final String IMG_JPEG_SUFFIX = "jpeg";
	public static final String IMG_PNG_SUFFIX = "png";
	public static final String WD_DESKTOP_FILE = "wallpaperdownloader.desktop";
	public static final String WD_SNAP_DESKTOP_FILE = "snap.wallpaperdownloader.desktop";
	public static final String DESKTOP_LOCATION = "/desktop/";
	public static final String WD_ICON_FILE = "wallpaperdownloader.svg";
	public static final String ICON_LOCATION = "/images/desktop/";
	public static final String AMD_ARCHITECTURE = "AMD";
	public static final String ARM_ARCHITECTURE = "ARM";
	private static final String GOOGLE_URL = "https://www.google.com/";
	private static final String PRODUCT = "502055100500307060b0551050404060203020f00070403060e0401055605030506030001030508060906590405020304030253000600090506000b0101040a030d0257030b010d050a0255005b03540304020f010f";
	

	// Attributes
	private static String appPath;
	private static String downloadsPath;
	private static String userConfigurationFilePath;
	private static String operatingSystem;
	private static String architecture;
	private static WallpaperChanger wallpaperChanger;

	// Getters & Setters
	public static String getAppPath() {
		return appPath;
	}

	public static void setAppPath(String appPath) {
		WDUtilities.appPath = appPath;
	}

	public static String getDownloadsPath() {
		return downloadsPath;
	}

	public static void setDownloadsPath(String downloadsPath) {
		WDUtilities.downloadsPath = downloadsPath;
	}

	public static String getUserConfigurationFilePath() {
		return userConfigurationFilePath;
	}

	public static void setUserConfigurationFilePath(
			String userConfigurationFilePath) {
		WDUtilities.userConfigurationFilePath = userConfigurationFilePath;
	}
	
	public static String getBlacklistDirectoryPath() {
		PropertiesManager pm = PropertiesManager.getInstance();
		Path blacklistPath = Paths.get(appPath.toString());
		blacklistPath = blacklistPath.resolve(pm.getProperty("app.blacklist.path"));
		return blacklistPath.toString();
	}

	public static String getOperatingSystem() {
		return operatingSystem;
	}

	public static void setOperatingSystem(String operatingSystem) {
		WDUtilities.operatingSystem = operatingSystem;
	}

	public static WallpaperChanger getWallpaperChanger() {
		return wallpaperChanger;
	}

	public static void setWallpaperChanger(WallpaperChanger wallpaperChanger) {
		WDUtilities.wallpaperChanger = wallpaperChanger;
	}

	/**
	 * Gets architecture.
	 * @return the architecture
	 */
	public static String getArchitecture() {
		return architecture;
	}

	/**
	 * Sets {@link #architecture}.
	 * @param architecture the architecture to set
	 */
	public static void setArchitecture(String architecture) {
		WDUtilities.architecture = architecture;
	}

	// Methods (All the methods are static)

	/**
	 * Constructor
	 */
	private WDUtilities() {

	}

	/**
	 * Get screen resolution.
	 * @return
	 */
	public static String getResolution() {
		return getWidthResolution() + "x" + getHeightResolution();
	}

	/**
	 * Get screen width resolution.
	 * @return
	 */
	public static int getWidthResolution() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Double width = screenSize.getWidth();
		return width.intValue();
	}
	
	/**
	 * Get screen height resolution.
	 * @return
	 */
	public static int getHeightResolution() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Double height = screenSize.getHeight();
		return height.intValue();
	}
	
	/**
	 * Get all the wallpapers from a directory.
	 * @param wallpapersType Type of the wallpapers wanted to retrieve (favorite, non favorite, all)
	 * @param directory Directory to get all the wallpapers
	 * @return a list of File
	 */
	public static List<File> getAllWallpapers(String wallpapersType, String directory) {
		LOG.info("Getting all the wallpapers in " + directory + "...");
		File downloadDirectory;
		if (directory.equals(WDUtilities.DOWNLOADS_DIRECTORY)) {
			downloadDirectory = new File(WDUtilities.getDownloadsPath());
		} else {
			downloadDirectory = new File(directory);
		}
		List<File> wallpapers = new ArrayList<File>();
		if (wallpapersType.equals(WDUtilities.WD_ALL)) {
			wallpapers = (List<File>) FileUtils.listFiles(downloadDirectory, FileFilterUtils.suffixFileFilter(WDUtilities.IMG_JPEG_SUFFIX), null);
			wallpapers.addAll((List<File>) FileUtils.listFiles(downloadDirectory, FileFilterUtils.suffixFileFilter(WDUtilities.IMG_JPG_SUFFIX), null));
			wallpapers.addAll((List<File>) FileUtils.listFiles(downloadDirectory, FileFilterUtils.suffixFileFilter(WDUtilities.IMG_PNG_SUFFIX), null));
		} else {
			wallpapers = (List<File>) FileUtils.listFiles(downloadDirectory, FileFilterUtils.prefixFileFilter(wallpapersType), null);
		}
				
		return wallpapers;
	}

	/**
	 * Move all the wallpapers to a new location.
	 * @param newPath
	 */
	public static void moveDownloadedWallpapers(String newPath, boolean deleteOldFolder) {
		if (!WDUtilities.getDownloadsPath().equals(newPath)) {
			File destDir = new File(newPath);
			File srcDir = new File(WDUtilities.getDownloadsPath());
			// Get all the wallpapers from the current location
			List<File> wallpapers = getAllWallpapers(WDUtilities.WD_ALL, WDUtilities.DOWNLOADS_DIRECTORY);

			// Move every file to the new location
			Iterator<File> wallpaperIterator = wallpapers.iterator();
			while (wallpaperIterator.hasNext()) {
				File wallpaper = wallpaperIterator.next();
				try {
					FileUtils.moveFileToDirectory(wallpaper , destDir, true);
					if (LOG.isInfoEnabled()) {
						LOG.info("Wallpaper " + wallpaper.getAbsolutePath() + " has been moved to " + destDir.getAbsolutePath());
					}
				} catch (IOException e) {
					// Something went wrong
					LOG.error("Error moving file " + wallpaper.getAbsolutePath());
					// Information
					DialogManager info = new DialogManager("Something went wrong. Downloads directory couldn't be changed. Check log for more information.", 2000);
					info.openDialog();
				}
			}

			// Remove old directory if it is necessary
			if (deleteOldFolder) {
				try {
					FileUtils.deleteDirectory(srcDir);
					if (LOG.isInfoEnabled()) {
						LOG.info("Old download folder " + srcDir.getAbsolutePath() + " has been removed");
					}
				} catch (IOException e) {
					// Something went wrong
					LOG.error("The original downloads directory " + WDUtilities.getDownloadsPath() + " couldn't be removed. Please, check also directory " + destDir.getAbsolutePath() + " because all the wallpapers were copied there");
					// Information
					DialogManager info = new DialogManager("Something went wrong. Downloads directory couldn't be changed. Check log for more information.", 2000);
					info.openDialog();
				}
			}

			// Information
			if (WDUtilities.getLevelOfNotifications() > 0) {
				DialogManager info = new DialogManager("Downloads directory has been successfully changed to " + destDir.getAbsolutePath(), 2000);
				info.openDialog();
			}
			if (LOG.isInfoEnabled()) {
				LOG.info("Downloads directory has been successfully changed to " + destDir.getAbsolutePath());
			}

		}
	}

	/**
	 * Calculates the percentage of the space occupied within the directory.
	 * @param directoryPath
	 * @return int
	 */
	public static int getPercentageSpaceOccupied(String directoryPath) {
		PreferencesManager prefm = PreferencesManager.getInstance();
		int downloadsDirectorySize = new Integer(prefm.getPreference("application-max-download-folder-size"));
		long spaceLong = WDUtilities.getDirectorySpaceOccupied(directoryPath, WDUtilities.UNIT_KB);
		// Obtaining percentage
		int percentage = (int) ((spaceLong * 100) / (downloadsDirectorySize * 1024));
		if (percentage > 100) {
			percentage = 100;
		}
		LOG.info("Downloads directory space occupied: " + percentage + " %");
		return percentage;
	}

	/**
	 * Calculate the space occupied within the directory.
	 * @param directoryPath
	 * @param unit
	 * @return
	 */
	public static long getDirectorySpaceOccupied(String directoryPath, String unit) {
		long space = 0;
		File directory = new File(directoryPath);
		// Calculates the space in bytes
		space = FileUtils.sizeOfDirectory(directory);
		if (unit.equals(WDUtilities.UNIT_MB)) {
			// Turning bytes into Megabytes
			space = (space / 1024) / 1024;
		} else if (unit.equals(WDUtilities.UNIT_KB)) {
			// Turning bytes into Kilobytes
			space = space / 1024;
		}
		return space;
	}

	/**
	 * Set wallpapers as favorite.
	 * @param originalAbsolutePath
	 */
	public static void setFavorite(List<String> originalAbsolutePaths) {
		Iterator<String> wallpaperIterator = originalAbsolutePaths.iterator();
		while (wallpaperIterator.hasNext()) {
			String currentWallpaperPath = wallpaperIterator.next();
			File originalWallpaper = new File(currentWallpaperPath);
			String wallpaperOriginalName = originalWallpaper.getName();
			int index = wallpaperOriginalName.indexOf("-");
			String wallpaperFavoriteName = WD_FAVORITE_PREFIX + wallpaperOriginalName.substring(index + 1);
			File favoriteWallpaper = new File(WDUtilities.getDownloadsPath() + File.separatorChar + wallpaperFavoriteName);
			originalWallpaper.renameTo(favoriteWallpaper);

			// Information
			if (WDUtilities.getLevelOfNotifications() > 1) {
				if (!wallpaperIterator.hasNext()) {
					DialogManager info = new DialogManager("Wallpaper/s set as favorite.", 2000);
					info.openDialog();
				}
			}
			WallpaperDownloader.refreshJScrollPane();
			LOG.info(currentWallpaperPath + " wallpaper has been set as favorite");
		}		
	}

	/**
	 * Set wallpapers as no favorite.
	 * @param originalAbsolutePath
	 */
	public static void setNoFavorite(List<String> originalAbsolutePaths) {
		Iterator<String> wallpaperIterator = originalAbsolutePaths.iterator();
		while (wallpaperIterator.hasNext()) {
			String currentWallpaperPath = wallpaperIterator.next();
			File originalWallpaper = new File(currentWallpaperPath);
			String wallpaperOriginalName = originalWallpaper.getName();
			int index = wallpaperOriginalName.indexOf("-");
			String wallpaperNoFavoriteName = WD_PREFIX + wallpaperOriginalName.substring(index + 1);
			File favoriteWallpaper = new File(WDUtilities.getDownloadsPath() + File.separatorChar + wallpaperNoFavoriteName);
			originalWallpaper.renameTo(favoriteWallpaper);
			
			// Information
			if (WDUtilities.getLevelOfNotifications() > 1) {
				if (!wallpaperIterator.hasNext()) {
					DialogManager info = new DialogManager("Wallpaper/s set as no favorite.", 2000);
					info.openDialog();
				}
			}
			WallpaperDownloader.refreshJScrollPane();
			LOG.info(currentWallpaperPath + " wallpaper has been set as no favorite");
		}	
	}

	/**
	 * Remove wallpapers.
	 * @param originalAbsolutePaths
	 */
	public static void removeWallpaper(List<String> originalAbsolutePaths, Boolean notification) {
		Iterator<String> wallpaperIterator = originalAbsolutePaths.iterator();
		while (wallpaperIterator.hasNext()) {
			String currentWallpaperPath = wallpaperIterator.next();
			File originalWallpaper = new File(currentWallpaperPath);
			try {
				// 1.- File is deleted
				FileUtils.forceDelete(originalWallpaper);
				// 2.- File is added to the blacklist
				WDUtilities.addToBlacklist(originalWallpaper.getName());
				// 3.- Information about deleting is displayed if it is required
				if (WDUtilities.getLevelOfNotifications() > 1) {
					if (notification && !wallpaperIterator.hasNext()) {
						DialogManager info = new DialogManager("Wallpaper/s removed.", 2000);
						info.openDialog();
					}
				}
				if (LOG.isInfoEnabled()) {
					LOG.info(currentWallpaperPath + " wallpaper has been removed");
					LOG.info("Refreshing space occupied progress bar...");
				}
				WallpaperDownloader.refreshProgressBar();
				WallpaperDownloader.refreshJScrollPane();
			} catch (IOException e) {
				LOG.error("The wallpaper " + currentWallpaperPath + " couldn't be removed. Error: " + e.getMessage());
			}
		}		
	}

	/**
	 * Adds a file to the blacklist directory.
	 * @param fileName the name of the file
	 */
	public static void addToBlacklist(String fileName) {
		File blacklistedFile = new File(WDUtilities.getBlacklistDirectoryPath() + File.separator + fileName);
		try {
			FileUtils.touch(blacklistedFile);
			if (LOG.isInfoEnabled()) {
				LOG.info(fileName + " added to the blacklist");	
			}
		} catch (IOException exception) {
			if (LOG.isInfoEnabled()) {
				LOG.info("Error adding " + fileName + " to the blacklist. Error: " + exception.getMessage());	
			}
		}
	}

	/**
	 * Get all wallpapers downloaded sorted by date.
	 * @return
	 */
	public static File[] getAllWallpapersSortedByDate(String wallpapersType) {
		List<File> wallpaperList = getAllWallpapers(wallpapersType, WDUtilities.DOWNLOADS_DIRECTORY);
		File[] wallpapers = new File[wallpaperList.size()];
		wallpapers = wallpaperList.toArray(wallpapers);
		Arrays.sort(wallpapers, LastModifiedFileComparator.LASTMODIFIED_COMPARATOR);
		return wallpapers;
	}
		
	/**
	 * It picks a random file from a directory.
	 * @param includeFavoriteWallpapers if it is set to TRUE, this method will pick favorite and not favorite wallpapers
	 * @return
	 */
	public static File pickRandomFile(Boolean includeFavoriteWallpapers, String directory) {
		String wallpapersType = WDUtilities.WD_ALL;
		if (!includeFavoriteWallpapers) {
			wallpapersType = WDUtilities.WD_PREFIX;
		}
		List<File> files = WDUtilities.getAllWallpapers(wallpapersType, directory);
		if (!files.isEmpty()) {
			Random generator = new Random();
			int index = generator.nextInt(files.size());
			return files.get(index);			
		} else {
			return null;
		}
		
	}

	/**
	 * It picks a random image from a directory.
	 * @param directoryPath directory path to get all the images
	 * @return File if there is a wallpaper, null otherwise
	 */
	public static File pickRandomImage(String directoryPath) {

		LOG.info("Getting all the images in " + directoryPath + "...");
		File directory = new File(directoryPath);
		FilenameFilter imageFilenameFilter = new ImageFilenameFilter();
		File[] images = directory.listFiles(imageFilenameFilter);
		if (images != null && images.length > 0) {
			Random generator = new Random();
			int index = generator.nextInt(images.length);
			return images[index];			
		} else {
			return null;
		}
		
	}

	/**
	 * Checks if a wallpaper is blacklisted.
	 * @param wallpaperName wallpaper name
	 * @return boolean
	 */
	public static boolean isWallpaperBlacklisted(String wallpaperName) {
		File blacklistedFile = new File(WDUtilities.getBlacklistDirectoryPath() + File.separator + wallpaperName);
		boolean result = blacklistedFile.exists();
		if (LOG.isInfoEnabled()) {
			LOG.debug("Is " + wallpaperName + " blacklisted?: " + result + ".PS: Only wallpapers not blacklisted will be stored");
		}
		return result;
	}
	
	/**
	 * Checks if a wallpaper is favorite.
	 * @param wallpaperAbsolutePath wallpaper absolute path
	 * @return boolean
	 */
	public static boolean isWallpaperFavorite(String wallpaperAbsolutePath) {
		if (wallpaperAbsolutePath.contains(WD_FAVORITE_PREFIX)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if wallpaperdownloader application has been installed via snap package.
	 * @return boolean
	 */
	public static boolean isSnapPackage() {
		boolean result = Boolean.FALSE;
		PreferencesManager prefm = PreferencesManager.getInstance();
		String downloadsDirectoryString = prefm.getPreference("application-first-time-downloads-folder");
		if (downloadsDirectoryString.contains(WDUtilities.SNAP_KEY)) {
			result = Boolean.TRUE;
		}
		return result;

	}

	/**
	 * Checks if wallpaperdownloader application has been installed via flatpak package.
	 * @return boolean
	 */
	public static boolean isFlatpakPackage() {
		boolean result = Boolean.FALSE;
		// Starting from version 1.2.1, Flatpak will always set the FLATPAK_ID variable.
		// However, some major distros like Ubuntu 18.04 LTS, are still on version 1.0.9.
		// TODO: Change this once most major distros upgrade to Flatpak 1.2.1.
		String flatpakDir = System.getenv("FLATPAK_SANDBOX_DIR");
		if (flatpakDir != null && !flatpakDir.isEmpty()) {
			result = Boolean.TRUE;
		}
		return result;
	}

	/**
	 * Checks if the application can be minimized to system tray.
	 * Plasma 5 and GNOME 3 don't support traditional system tray icon and behavior
	 * @return boolean
	 */
	public static boolean isMinimizable() {
		boolean result = Boolean.FALSE;
		if (WDUtilities.getOperatingSystem().equals(WDUtilities.OS_WINDOWS) || 
			WDUtilities.getOperatingSystem().equals(WDUtilities.OS_WINDOWS_7) || 
			WDUtilities.getOperatingSystem().equals(WDUtilities.OS_WINDOWS_10)) {
			result = Boolean.TRUE;
		} else if (WDUtilities.getOperatingSystem().equals(WDUtilities.OS_MACOSX)) { 
			result = Boolean.FALSE;
		} else if (WDUtilities.getOperatingSystem().equals(WDUtilities.OS_LINUX)) {
			LinuxWallpaperChanger wallpaperChanger = (LinuxWallpaperChanger)WDUtilities.getWallpaperChanger();
			switch (wallpaperChanger.getDesktopEnvironment()) {
			case WDUtilities.DE_UNITY:
				result = Boolean.TRUE;
				break;
			case WDUtilities.DE_MATE:
				result = Boolean.TRUE;
				break;
			case WDUtilities.DE_XFCE:
				result = Boolean.TRUE;
				break;
			case WDUtilities.DE_KDE:
				result = Boolean.TRUE;
				break;
			case WDUtilities.DE_GNOME:
				result = Boolean.TRUE;
				break;
			case WDUtilities.DE_GNOME3:
				result = Boolean.TRUE;
				break;
			case WDUtilities.DE_CINNAMON:
				result = Boolean.TRUE;
				break;
			case WDUtilities.DE_PANTHEON:
				result = Boolean.TRUE;
				break;
			default:
				break;
			}
		} else {
			result = Boolean.FALSE;
		}
		return result;
	}

	/**
	 * Gets changer directory.
	 * @return String
	 */
	public static String getChangerPath() {
		PreferencesManager prefm = PreferencesManager.getInstance();
		String changerDirectoryString = prefm.getPreference("application-changer-folder");
		return changerDirectoryString;
	}

	/**
	 * Gets move favorite directory.
	 * @return String
	 */
	public static String getMoveFavoritePath() {
		PreferencesManager prefm = PreferencesManager.getInstance();
		String moveFavoriteDirectoryString = prefm.getPreference("move-favorite-folder");
		if (moveFavoriteDirectoryString.equals(PreferencesManager.DEFAULT_VALUE)) {
			moveFavoriteDirectoryString = WDUtilities.getDownloadsPath();
		}
		return moveFavoriteDirectoryString;
	}

	/**
	 * Moves all favorite wallpapers to another directory.
	 * @param destDir directory where all the favorite wallpapers are going to be moved.
	 */
	public static void moveFavoriteWallpapers(String destDir) {
		if (!destDir.equals(PreferencesManager.DEFAULT_VALUE)) {
			// Information

			// Get all favorite wallpapers from the current location
			List<File> wallpapers = getAllWallpapers(WDUtilities.WD_FAVORITE_PREFIX, WDUtilities.getDownloadsPath());
			Iterator<File> wallpapersIterator = wallpapers.iterator();
			// Wallpapers will be copied and blacklisted in order to prevent them to be downloaded again
			while (wallpapersIterator.hasNext()) {
				File wallpaper = wallpapersIterator.next();
				File destFile = new File(destDir + File.separator + wallpaper.getName());
				try {
					FileUtils.copyFile(wallpaper, destFile);
					List<String> wallpaperPathList = new ArrayList<String>();
					wallpaperPathList.add(wallpaper.getAbsolutePath());
					WDUtilities.removeWallpaper(wallpaperPathList, Boolean.FALSE);
					if (LOG.isInfoEnabled()) {
						LOG.info(wallpaper.getAbsolutePath() + " favorite wallpaper moved to " + destFile.getAbsolutePath());
					}
				} catch (IOException exception) {
					if (LOG.isInfoEnabled()) {
						LOG.error("Error moving " + wallpaper.getAbsolutePath() + ". Error: " + exception.getMessage());
					}
				}
			}

		}
	}
	
	/**
	 * Gets the level of notifications defined by the user.
	 * @return Integer
	 */
	public static Integer getLevelOfNotifications() {
		PreferencesManager prefm = PreferencesManager.getInstance();
		return new Integer(prefm.getPreference("application-notifications"));
	}

	/**
	 * Checks Internet connectivity.
	 * @return boolean
	 */
	public static boolean checkConnectivity() {
		boolean result = false;
		try {
			Response response = Jsoup.connect(GOOGLE_URL).followRedirects(false).userAgent("Mozilla").execute();
			if (response.statusCode() == 200) {
				result = true;				
			} else {
	            if (LOG.isInfoEnabled()) {
	            	LOG.error("Internet connection is down...");
	            }
			}
		} catch (IOException exception) {
			if (LOG.isInfoEnabled()) {
				LOG.error("Error checking Internet connectivity. Message: " + exception.getMessage());
			}
		}
		return result;
	}

	/**
	 * Gets the i18n bundle for displaying the messages
	 * @return
	 */
	public static ResourceBundle getBundle() {
		PreferencesManager prefm = PreferencesManager.getInstance();
		String language = "en";
		switch (prefm.getPreference("application-i18n")) {
		case "0":
			language = "en";
			break;
		case "1":
			language = "es";
			break;
		default:
			language = "en";
			break;
		}		
		Locale.setDefault(Locale.forLanguageTag(language));
		Locale locale = new Locale(language);
		return ResourceBundle.getBundle("I18n", locale);
	}

	/**
	 * Opens a link in a browser depending on the OS.
	 * @param link link to be opened
	 */
	public static void openLinkOnBrowser(String link) {
		switch (WDUtilities.getOperatingSystem()) {
		case WDUtilities.OS_LINUX:
			Process process;
		      try {
		    	  if (WDUtilities.isSnapPackage()) {
			          process = Runtime.getRuntime().exec("/usr/local/bin/xdg-open " + link);
		    	  } else {
			          process = Runtime.getRuntime().exec("xdg-open " + link);
		    	  }
		          process.waitFor();
		          process.destroy();
		      } catch (Exception exception) {
		    	  if (LOG.isInfoEnabled()) {
		    		LOG.error("Browser couldn't be opened. Error: " + exception.getMessage());  
		    	  }
		      }						
		      break;
		default:
		    if (Desktop.isDesktopSupported()) {
	        try {
	          Desktop.getDesktop().browse(new URI(link));
	        } catch (Exception exception) { 
	        	LOG.error(exception.getMessage()); 
	        }
	     }
			break;
		}		
	}

	/**
	 * Wrapper method to run a program bypassing Flatpak's sandbox.
	 * @param command Command to run
	 * @return Process
	 */
	public static Process execProgram(String command) throws IOException {
		Process process;

		if (WDUtilities.isFlatpakPackage()) {
			command = "flatpak-spawn --host " + command;
		}
		process = Runtime.getRuntime().exec(command);

		return process;
	}

	/**
	 * Checks if the desktop environment is GNOME or derivative.
	 * @return boolean
	 */
	public static boolean isGnomeish() {
		boolean result = Boolean.FALSE;
		if (WDUtilities.getOperatingSystem().equals(WDUtilities.OS_LINUX)) {
			LinuxWallpaperChanger wallpaperChanger = (LinuxWallpaperChanger)WDUtilities.getWallpaperChanger();
			switch (wallpaperChanger.getDesktopEnvironment()) {
			case WDUtilities.DE_UNITY:
				result = Boolean.TRUE;
				break;
			case WDUtilities.DE_MATE:
				result = Boolean.TRUE;
				break;
			case WDUtilities.DE_GNOME:
				result = Boolean.TRUE;
				break;
			case WDUtilities.DE_GNOME3:
				result = Boolean.TRUE;
				break;
			case WDUtilities.DE_CINNAMON:
				result = Boolean.TRUE;
				break;
			case WDUtilities.DE_PANTHEON:
				result = Boolean.TRUE;
				break;
			default:
				break;
			}
		}
		return result;
	}

	/**
	 * Changes multi monitor mode for desktop environments such as GNOME or derivatives.
	 * @param mode
	 */
	public static void changeMultiMonitorModeGnomish(String mode) {
      Process process;
      try {
			LinuxWallpaperChanger wallpaperChanger = (LinuxWallpaperChanger)WDUtilities.getWallpaperChanger();
			String command = "gsettings set org.gnome.desktop.background picture-options \"" + mode + "\"";
			switch (wallpaperChanger.getDesktopEnvironment()) {
			case WDUtilities.DE_MATE:
				command = "gsettings set org.mate.background picture-options \"" + mode + "\"";
				break;
			case WDUtilities.DE_CINNAMON:
				command = "gsettings set org.cinnamon.desktop.background picture-options \"" + mode + "\"";
				break;
			default:
				break;
			}
    	  
          process = WDUtilities.execProgram(command);

    	  BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
    	  BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

    	  // Read the output from the command
    	  String processOutput = null;
    	  while ((processOutput = stdInput.readLine()) != null) {
        	  if (LOG.isInfoEnabled()) {
        		  LOG.info(processOutput);
        	  }
    	  }
			
    	  // Read any errors from the attempted command
    	  while ((processOutput = stdError.readLine()) != null) {
        	  if (LOG.isInfoEnabled()) {
        		  LOG.error(processOutput);
        	  }
    	  }

    	  if (LOG.isInfoEnabled()) {
    		LOG.info("Multi monitor mode changed");  
    	  }

          process.waitFor();
          process.destroy();
      } catch (Exception exception) {
    	  if (LOG.isInfoEnabled()) {
    		LOG.error("Multi monitor mode couldn't be changed. Error: " + exception.getMessage());  
    	  }
      }	
	}

	/**
	 * Gets the path where the autostart file should be located within Linux systems.
	 * @return autostart file path
	 */
	public static String getAutostartFilePath() {
		String path = Paths.get(System.getProperty("user.home")).toString();
		if (WDUtilities.isSnapPackage()) {
			path = WDUtilities.sanitizeSnapDirectory(path);
		}
		path = path + File.separator + ".config" + File.separator + "autostart" + File.separator;
		return path;
	}

	/**
	 * Sanitizes a directory if it is within snap structure.
	 * Snap version will be changed to current directory
	 * @param directory
	 * @return sanitized directory
	 */
	public static String sanitizeSnapDirectory(String directory) {
		 if (directory.contains(WDUtilities.SNAP_KEY)) {
			 // The directory is inside snap structure
			 if (!directory.contains("current")) {
				 // The directory is changed to current directory
				 String[] directoryParts = directory.split(File.separator + WDUtilities.SNAP_KEY + File.separator + "wallpaperdownloader" + File.separator);
				 directory = directoryParts[0] + 
						 							File.separator + 
						 							WDUtilities.SNAP_KEY + 
						 							File.separator + 
						 							"wallpaperdownloader" + 
						 							File.separator +
						 							"current";
				 if (directoryParts[1].contains(File.separator)) {
					 directory = directory + directoryParts[1].substring(directoryParts[1].indexOf(File.separator), directoryParts[1].length());
				 }
			 }
		 }		
		 return directory;
	}

	
	/**
	 * Returns a new array of files that is the concatenation of array1 and array2.
	 * 
	 * @param array1 first array of files
	 * @param array2 second array of files
	 * @return a File[] array
	 */
	public static File[] append(File[] array1, File[] array2) {
		File[] result = new File[array1.length + array2.length];
	    System.arraycopy(array1, 0, result, 0, array1.length);
	    System.arraycopy(array2, 0, result, array1.length, array2.length);
	    return result;
	}

	/**
	 * Obtains XOR product between two Strings and more.
	 * @param clientId client ID
	 * @return client ID after XOR
	 */
	public static String xorPlus(String clientId) {
    	byte[] keyBytes = Base64.getDecoder().decode(clientId);
    	String key = new String(keyBytes).trim();

    	// Converting key to hexadecimal
    	StringBuffer stringBuffer = new StringBuffer();
    	char character[] = key.toCharArray();
    	for(int i = 0; i < character.length; i++) {
    		String charHexadecimal = Integer.toHexString(character[i]);
    		stringBuffer.append(charHexadecimal);
    	}
    	String keyHexadecimalString = stringBuffer.toString();
    	
    	// Converting key to binary
    	String keyBinaryString = new BigInteger(keyHexadecimalString.getBytes()).toString(2);

    	// Converting PRODUCT hexadecimal to binary
    	BigInteger productBinaryBigInteger = new BigInteger(PRODUCT, 16);
    	String productBinaryString = "000" + productBinaryBigInteger.toString(2);

    	// perform XOR operation between key and product with every character in strings
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < productBinaryString.length(); i++) {
            stringBuilder.append(charOf(bitOf(keyBinaryString.charAt(i)) ^ bitOf(productBinaryString.charAt(i))));
        }

        String clientIdBinaryString = stringBuilder.toString();

    	// Converting from binary to hexadecimal
    	BigInteger clientIdBinaryBigInteger = new BigInteger(clientIdBinaryString, 2);
    	String clientIdHexadecimalString = clientIdBinaryBigInteger.toString(16);

    	// Converting from hexadecimal to UTF-8
    	String clientIdHexadecimal = hexToAscii(clientIdHexadecimalString);
    	
		return hexToAscii(clientIdHexadecimal);
	}
    private static boolean bitOf(char in) {
        return (in == '1');
    }

    private static char charOf(boolean in) {
        return (in) ? '1' : '0';
    }
    
    private static String hexToAscii(String hexStr) {
        StringBuilder output = new StringBuilder("");
        
        for (int i = 0; i < hexStr.length(); i += 2) {
            String str = hexStr.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
        }
        
        return output.toString();
    }
}