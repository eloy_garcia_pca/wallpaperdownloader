/**
 * Copyright 2016-2021 Eloy García Almadén <eloy.garcia.pca@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.estoes.wallpaperDownloader.provider;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import es.estoes.wallpaperDownloader.exception.ProviderException;
import es.estoes.wallpaperDownloader.harvest.Harvester;
import es.estoes.wallpaperDownloader.util.ImageUtilities;
import es.estoes.wallpaperDownloader.util.PreferencesManager;
import es.estoes.wallpaperDownloader.util.PropertiesManager;
import es.estoes.wallpaperDownloader.util.WDUtilities;
import es.estoes.wallpaperDownloader.window.WallpaperDownloader;

public class UnsplashProvider extends Provider {
	
	// Constants
	/**
	 * Log.
	 * 
	 * {@value #LOG}
	 */
	private static final Logger LOG = Logger.getLogger(UnsplashProvider.class);
	/**
	 * Client ID.
	 * 
	 * {@value #CLIENT_ID}
	 */
	private static final String CLIENT_ID = "LG1hczg3MTJqaDMxODlqYmFzZDkwODFrMmozOTA4YXNoTkJZaWtocUdIRgo";
	
	// Attributes
	/**
	 * Search results current page.
	 */
	private String page;
	/**
	 * Order to retrieve the results.
	 */
	private String order;

	// Getters & Setters
	
	/**
	 * Gets page.
	 * @return page
	 */
	public String getPage() {
		return this.page;
	}

	/**
	 * Sets {@value #page}.
	 * @param page page
	 */
	public void setPage(String page) {
		this.page = page;
	}

	/**
	 * Gets order.
	 * @return the order
	 */
	public String getOrder() {
		return this.order;
	}

	/**
	 * Sets {@value #order}.
	 * @param order the order to set
	 */
	public void setOrder(String order) {
		this.order = order;
	}

	// Methods
	/**
	 * Constructor
	 */
	public UnsplashProvider () {
		super();
		PropertiesManager pm = PropertiesManager.getInstance();
		this.baseURL = pm.getProperty("provider.unsplash.api.url");
		
		// Initial page
		this.setPage("1");

		// Order
		PreferencesManager prefm = PreferencesManager.getInstance();
		this.order = "relevant";
		switch (new Integer(prefm.getPreference("provider-unsplash-search-type"))) {
			case 0: this.order = "relevant";
					break;
			case 1: this.order = "latest";
					break;			
		}
	}
	
	public void getWallpaper() throws ProviderException {
		if (UnsplashProvider.configurationComplyUnsplashPolicy()) {
			Boolean wallpaperFound = Boolean.FALSE;

			// Obtaining keywords
			this.obtainActiveKeyword();

			String completeURL = this.composeCompleteURL();

			try {
				this.checkAndPrepareDownloadDirectory();	
				if (LOG.isInfoEnabled()) {
					LOG.info("Downloading wallpaper with keyword -> " + this.activeKeyword);
				}
				while (!wallpaperFound) {
					// Calling API
					URL url = new URL(completeURL);
					HttpURLConnection conn = (HttpURLConnection)url.openConnection();
					conn.setRequestMethod("GET");
					conn.connect();
					int responseCode = conn.getResponseCode();
					if(responseCode != 200) {
						// In order to avoid Rate Limit error with the API, if something goes wrong then wallpaperFound will be set to TRUE
						wallpaperFound = Boolean.TRUE;
						if (LOG.isInfoEnabled()) {
							LOG.error("There was a problem downloading a wallpaper. Complete URL -> " + completeURL.split("&client_id")[0] + ". Message: Error trying to get data from Unsplash API. Error code: " + responseCode);
							LOG.error("Skipping this keyword in order to avoid Unsplash API Rate Limit");
						}
					} else {
						// Getting results
						Scanner sc = new Scanner(url.openStream());
						String jsonResponse = "";
						while(sc.hasNext()){
							jsonResponse = jsonResponse + sc.nextLine();
						}
						sc.close();

						// Parsing JSON response
						JSONArray results = new JSONArray();
						JSONParser parser = new JSONParser();
						if (!this.activeKeyword.equals(PreferencesManager.DEFAULT_VALUE)) {
							JSONObject jsonObject = (JSONObject) parser.parse(jsonResponse);
							results = (JSONArray) jsonObject.get("results");
						} else {
							results = (JSONArray) parser.parse(jsonResponse);
						}
						
						if (results.size() > 0) {
							// Wallpapers were found
							// Getting data for results array
							for (int index = 0; index < results.size(); index++) {
								if (WallpaperDownloader.harvester.getStatus().equals(Harvester.STATUS_ENABLED)) {
									// Getting imageObject
									JSONObject imageObject = (JSONObject) results.get(index);
									// Getting ID
									String unsplashId = (String) imageObject.get("id");
									// Getting author name and profile
									JSONObject user = (JSONObject) imageObject.get("user");
									String author = (String) user.get("name");
									JSONObject userLinks = (JSONObject) user.get("links");
									String profileUrl = (String) userLinks.get("html");
									// Getting hotlinked URLs
									// These URLs have to be requested in order to increase the number of downloads
									// in Unsplash. The final URL obtained from this request will be the one used to actually
									// download the image
									JSONObject urls = (JSONObject) imageObject.get("urls");
									String rawWallpaperURL = (String) urls.get("raw");
									String fullWallpaperURL = (String) urls.get("full");

									// Obtaining wallpaper's name
									String wallpaperName = WDUtilities.WD_PREFIX + Provider.UNSPLASH_PREFIX + unsplashId;
									String wallpaperNameFavorite = WDUtilities.WD_FAVORITE_PREFIX + Provider.UNSPLASH_PREFIX + unsplashId;
									File wallpaper = new File(WDUtilities.getDownloadsPath() + File.separator + wallpaperName + WDUtilities.PERIOD + "jpg");
									File wallpaperFavorite = new File(WDUtilities.getDownloadsPath() + File.separator + wallpaperNameFavorite + WDUtilities.PERIOD + "jpg");

									if (!wallpaper.exists() && 
											!wallpaperFavorite.exists() && 
											!WDUtilities.isWallpaperBlacklisted(wallpaperName) && 
											!WDUtilities.isWallpaperBlacklisted(wallpaperNameFavorite)) {

										// Storing the image. It is necessary to download the remote file
										boolean isWallpaperSuccessfullyStored = false;
										String[] userResolution = this.resolution.split("x");
										
										String wallpaperURL = "";
										
										// Checking download policy
										// 0 -> Download any wallpaper and keep the original resolution
										// 1 -> Download any wallpaper and resize it (if it i)s bigger) to the resolution defined
										// 2 -> Download only wallpapers with the resolution set by the user
										switch (this.downloadPolicy) {
										case "0":
											wallpaperURL = fullWallpaperURL;
											isWallpaperSuccessfullyStored = storeRemoteFile(wallpaper, wallpaperURL);
											break;
										default:
											wallpaperURL = rawWallpaperURL + WDUtilities.AND + 
												"w" + WDUtilities.EQUAL + userResolution[0] + 
												WDUtilities.AND +
												"h" + WDUtilities.EQUAL + userResolution[1] + 
												WDUtilities.AND + 
												"fit=fillmax&fill=blur";
											isWallpaperSuccessfullyStored = storeRemoteFile(wallpaper, wallpaperURL);
											break;
										}

										if (!isWallpaperSuccessfullyStored) {
											if (LOG.isInfoEnabled()) {
												LOG.info("Error trying to store wallpaper " + wallpaperURL + ". Skipping...");							
											}
										} else {
											// Inserting metadata within the image
											ImageUtilities.insertMetadata(wallpaper.getAbsolutePath(), profileUrl, author);
											LOG.info("Wallpaper " + wallpaper.getName() + " successfully stored");
											LOG.info("Refreshing space occupied progress bar...");
											WallpaperDownloader.refreshProgressBar();
											WallpaperDownloader.refreshJScrollPane();
											wallpaperFound = Boolean.TRUE;
											// Exit the process because one wallpaper was downloaded successfully
											break;
										}
									} else {
										LOG.info("Wallpaper " + wallpaper.getName() + " is already stored or blacklisted. Skipping...");
									}									
								} else {
									// Harvester is disabled so provider stops getting wallpapers
									if (LOG.isInfoEnabled()) {
										LOG.info("Harvesting process has been disabled. Stopping provider " + this.getClass().getName());
									}
									break;
								}
							}
							
							if (!wallpaperFound) {
								// If no wallpaper is found in this page, the offset is incremented
								this.setPage(this.getPage() + 1);
								completeURL = composeCompleteURL();
							}						
						} else {
							// There aren't wallpapers
							wallpaperFound = Boolean.TRUE;
							// Resetting page
							this.setPage("0");
							if (LOG.isInfoEnabled()) {
								LOG.info("No more wallpapers found. Skipping...");
							}
						}					
					}
				}
			} catch (IOException exception) {
				throw new ProviderException("There was a problem downloading a wallpaper in Unsplash. Message: " + exception.getMessage());
			} catch (ProviderException providerException) {
				throw providerException;
			} catch (ParseException exception) {
				throw new ProviderException("There was a problem downloading a wallpaper in Unsplash. Message: " + exception.getMessage());
			}		
		} else {
			if (LOG.isInfoEnabled()) {
				LOG.info("Sorry, but in order to comply Unsplash policy, you have to set a downloading time of, at least, 1 hour");
			}
		}
	}
		
	private String composeCompleteURL() {
		// Search URL
		// https://api.unsplash.com/search/photos?query=dog&page=1&per_page=20&order_by=latest&client_id=client_id
		// Random search, no key words
		// https://api.unsplash.com/photos?page=1&per_page=20&order_by=latest&client_id=client_id
		String completeUrl = this.baseURL;
		String keywordString = "";
		if (!this.activeKeyword.equals(PreferencesManager.DEFAULT_VALUE)) {
			// Replacing blank spaces with -
			this.activeKeyword = this.activeKeyword.replace(" ", "-");
			// Removing double quotes
			this.activeKeyword = this.activeKeyword.replace("\"", "");
			completeUrl = completeUrl + "search" + WDUtilities.URL_SLASH;
			keywordString = "query" + WDUtilities.EQUAL + this.activeKeyword + WDUtilities.AND;
		}
		completeUrl = completeUrl + "photos" + WDUtilities.QM + keywordString + 
				"page" + WDUtilities.EQUAL + this.page + 
				WDUtilities.AND + "per_page" + WDUtilities.EQUAL + "20" +
				WDUtilities.AND + "order_by" + WDUtilities.EQUAL + this.order + 
				WDUtilities.AND + "client_id" + WDUtilities.EQUAL + WDUtilities.xorPlus(CLIENT_ID); 
		
		return completeUrl;
	}
	
	/**
	 * Checks if the actual user configuration complies Unsplash download policy.
	 * User won't be able to download more than one wallpaper per hour.
	 * 
	 * @return a boolean depending on if the user's configuration complies Unsplash download policy or not
	 */
	public static Boolean configurationComplyUnsplashPolicy () {
		PreferencesManager prefm = PreferencesManager.getInstance();
		// Getting timer
		Long timer;
		Integer timeInterval = new Integer(prefm.getPreference("application-timer"));
		String timeUnit = prefm.getPreference("application-timer-units");
		switch (timeUnit) {
		case "s":
			timer = Long.valueOf(timeInterval * 1000);
			break;
		case "m":
			timer = Long.valueOf(timeInterval * 60 * 1000);
			break;
		case "h":
			timer = Long.valueOf(timeInterval * 60 * 60 * 1000);
			break;
		default:
			timer = Long.valueOf(timeInterval * 1000);
			break;
		}
		return (timer >= 3600000);
	}
}