/**
 * Copyright 2016-2021 Eloy García Almadén <eloy.garcia.pca@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.estoes.wallpaperDownloader.provider;

import java.io.File;
import java.io.IOException;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Element;
import es.estoes.wallpaperDownloader.exception.ProviderException;
import es.estoes.wallpaperDownloader.harvest.Harvester;
import es.estoes.wallpaperDownloader.util.PreferencesManager;
import es.estoes.wallpaperDownloader.util.PropertiesManager;
import es.estoes.wallpaperDownloader.util.WDUtilities;
import es.estoes.wallpaperDownloader.window.WallpaperDownloader;

public class DeviantArtProvider extends Provider {
	
	// Constants
	private static final Logger LOG = Logger.getLogger(DeviantArtProvider.class);

	// Attributes
	private String order;
	private String offset;
	/**
	 * Topic.
	 */
	private String topic;
	/**
	 * Page.
	 */
	private String page;
	
	// Getters & Setters
	
	/**
	 * Gets offset.
	 * @return offset
	 */
	public String getOffset() {
		return this.offset;
	}

	/**
	 * Sets offset.
	 * @param offset offset
	 */
	public void setOffset(String offset) {
		this.offset = offset;
	}

	/**
	 * Gets order.
	 * @return order
	 */
	public String getOrder() {
		return this.order;
	}

	/**
	 * Sets order.
	 * @param order order
	 */
	public void setOrder(String order) {
		this.order = order;
	}

	/**
	 * Gets topic.
	 * @return the topic
	 */
	public String getTopic() {
		return this.topic;
	}

	/**
	 * Sets {@link #topic}.
	 * @param topic the topic to set
	 */
	public void setTopic(String topic) {
		this.topic = topic;
	}

	/**
	 * Gets page.
	 * @return the page
	 */
	public String getPage() {
		return this.page;
	}

	/**
	 * Sets {@link #page}.
	 * @param page the page to set
	 */
	public void setPage(String page) {
		this.page = page;
	}
	
	// Methods
	/**
	 * Constructor
	 */
	public DeviantArtProvider () {
		super();
		PropertiesManager pm = PropertiesManager.getInstance();
		PreferencesManager prefm = PreferencesManager.getInstance();
		baseURL = pm.getProperty("provider.deviantart.baseurl");
		switch (new Integer(prefm.getPreference("wallpaper-devianart-search-type"))) {
			case 0:
				// No topic. The keywords set by the user will be used
				break;
			case 1:
				// 3D
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "3d";
				break;
			case 2:
				// Anime and manga
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "anime-and-manga";
				break;
			case 3:
				// Comics
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "comics";
				break;
			case 4:
				// Digital Art
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "digital-art";
				break;
			case 5:
				// Drawings and paintings
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "drawings-and-paintings";
				break;
			case 6:
				// Fan art
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "fan-art";
				break;
			case 7:
				// Fan fiction
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "fan-fiction";
				break;
			case 8:
				// Fantasy
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "fantasy";
				break;
			case 9:
				// Game art
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "game-art";
				break;
			case 10:
				// Horror
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "horror";
				break;
			case 11:
				// Photo manipulation
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "photo-manipulation";
				break;
			case 12:
				// Photography
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "photography";
				break;
			case 13:
				// Pixel Art
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "pixel-art";
				break;
			case 14:
				// Science fiction
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "science-fiction";
				break;
			case 15:
				// Street photography
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "street-photography";
				break;
			case 16:
				// Traditional art
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "traditional-art";
				break;
			case 17:
				// Wallpaper
				baseURL = baseURL + "topic" + WDUtilities.URL_SLASH + "wallpaper";
				break;
			default:
				// No topic. The keywords set by the user will be used
				break;
		}
		
		// Offset
		this.setOffset("1");
	}
	
	public void getWallpaper() throws ProviderException {
		Boolean wallpaperFound = Boolean.FALSE;
		Boolean containsKeywordsToAvoid = Boolean.FALSE;
		// Obtaining keywords
		this.obtainActiveKeyword();
		// Obtaining keywords to avoid
		this.obtainKeywordsToAvoid();
		String completeURL = this.composeCompleteURL();
		try {
			this.checkAndPrepareDownloadDirectory();	
			if (LOG.isInfoEnabled()) {
				LOG.info("Downloading wallpaper with keyword -> " + this.activeKeyword);
			}
			while (!wallpaperFound) {
				// 1.- Getting HTML document (New method including userAgent and other options)
				Document doc = Jsoup.connect(completeURL).userAgent("Mozilla").timeout(0).followRedirects(true).get();
				// 2.- Getting all thumbnails. They are identified because they have the custom attribute data-hook=deviation_link
				Elements thumbnails = doc.select("[data-hook=deviation_link]");
				
				if (!thumbnails.isEmpty()) {
					// 3.- Getting a wallpaper which is not already stored in the filesystem
					for (Element thumbnail : thumbnails) {
						if (WallpaperDownloader.harvester.getStatus().equals(Harvester.STATUS_ENABLED)) {
							// First, it is necessary to retrieve the <a> element inside the span which contains the link to 
							// downloads page for this wallpaper
							Elements linkURLs = thumbnail.select("a");
							Element linkURL = linkURLs.first();
							// Retrieves the entire document for this link
							Document wallpaperDoc = Jsoup.connect(linkURL.attr("href")).userAgent("Mozilla").timeout(0).followRedirects(true).get();
							// Retrieves the source for the full image
							// apparently to discourage scrapers they don't mark the image with a nice ID anymore							
							// Checking if the current wallpaper contains keywords to avoid
							if (!this.keywordsToAvoid.isEmpty()) {
								// All the tags are identified because they are a HTML elements with tag string within href attribute
								Elements tagsToAvoid = wallpaperDoc.select("[href*=tag]");
								for (Element tag : tagsToAvoid) {
									for (String keywordToAvoid : this.keywordsToAvoid) {
										if (tag.select("span").text().toLowerCase().contains(keywordToAvoid.toLowerCase())) {
											containsKeywordsToAvoid = Boolean.TRUE;
											if (LOG.isInfoEnabled()) {
												LOG.info("This wallpaper contains forbidden keyword " + keywordToAvoid + " and it won't be stored");
											}
											break;
										} else {
											containsKeywordsToAvoid = Boolean.FALSE;
										}
									}
									if (containsKeywordsToAvoid) {
										break;
									}
								}
							}
							
							Elements images = wallpaperDoc.select("img");
							Element image = null;
							for (Element img: images) {
								if (img.attr("src").startsWith("https://images-wixmp")) {
									if (LOG.isDebugEnabled()) {
										LOG.debug("Candidate image located at " + img.attr("src"));
									}
									image = img;
									break;
								}
							}
							if (image == null) {
								if (LOG.isInfoEnabled()) {
									LOG.error("No candidate image found!");
								}
								break;
							}
							String wallpaperURL = image.attr("src");
							int index = wallpaperURL.lastIndexOf(WDUtilities.URL_SLASH);
							// Obtaining wallpaper's name (string after the last slash)
							String wallpaperName = WDUtilities.WD_PREFIX + Provider.DEVIANT_ART_PREFIX + wallpaperURL.substring(index + 1);
							String wallpaperNameFavorite = WDUtilities.WD_FAVORITE_PREFIX + Provider.DEVIANT_ART_PREFIX + wallpaperURL.substring(index + 1);
							// Cleaning the name
							int parameterIndex = wallpaperName.indexOf("?");
							// DeviantArt now sticks a LONG parameter on the end of the file name, which we might use for fetching but don't want to 
							// store as the name in the file system							
							if (parameterIndex != -1) {
								wallpaperName = wallpaperName.substring(0,parameterIndex);
								wallpaperNameFavorite = wallpaperNameFavorite.substring(0,parameterIndex);
							}
							File wallpaper = new File(WDUtilities.getDownloadsPath() + File.separator + wallpaperName);
							File wallpaperFavorite = new File(WDUtilities.getDownloadsPath() + File.separator + wallpaperNameFavorite);
							// Checking if the wallpaper can be stored
							if (!containsKeywordsToAvoid && 
									!wallpaper.exists() && 
									!wallpaperFavorite.exists() && 
									!WDUtilities.isWallpaperBlacklisted(wallpaperName) && 
									!WDUtilities.isWallpaperBlacklisted(wallpaperNameFavorite)) {
								// Storing the image. It is necessary to download the remote file
								boolean isWallpaperSuccessfullyStored = false;
								if (LOG.isDebugEnabled()) {
									LOG.debug("Downloading: " + wallpaperURL);								
								}
								// Checking download policy
								// 0 -> Download any wallpaper and keep the original resolution
								// 1 -> Download any wallpaper and resize it (if it is bigger) to the resolution defined
								// 2 -> Download only wallpapers with the resolution set by the user
								switch (this.downloadPolicy) {
								case "0":
									isWallpaperSuccessfullyStored = storeRemoteFile(wallpaper, wallpaperURL);
									break;
								case "1":
									String[] userResolution = this.resolution.split("x");
									isWallpaperSuccessfullyStored = storeAndResizeRemoteFile(wallpaper, wallpaperURL, 
											Integer.valueOf(userResolution[0]), 
											Integer.valueOf(userResolution[1]));
									break;
								case "2":
									String remoteImageResolution = getRemoteImageResolution(wallpaperURL);
								    if (this.resolution.equals(remoteImageResolution)) {
								    	// Wallpaper resolution fits the one set by the user
										isWallpaperSuccessfullyStored = storeRemoteFile(wallpaper, wallpaperURL);
								    } else {
								    	if (LOG.isInfoEnabled()) {
								    		LOG.info("The image resolution is " + remoteImageResolution + " and user wants images at " + this.resolution + ". This wallpaper will be skipped.");
								    	}
								    	isWallpaperSuccessfullyStored = false;
								    }
									break;
								default:
									break;
								}
								
								if (!isWallpaperSuccessfullyStored) {
									if (LOG.isInfoEnabled()) {
										LOG.error("Error trying to store wallpaper " + wallpaperURL + ". Skipping...");
									}
								} else {
									if (LOG.isInfoEnabled()) {
										LOG.info("Wallpaper " + wallpaper.getName() + " successfully stored");
										LOG.info("Refreshing space occupied progress bar...");
									}
									WallpaperDownloader.refreshProgressBar();
									WallpaperDownloader.refreshJScrollPane();
									wallpaperFound = Boolean.TRUE;
									// Exit the process because one wallpaper was downloaded successfully
									break;
								}
							} else {
								if (LOG.isInfoEnabled()) {
									LOG.info("Wallpaper " + wallpaper.getName() + " is already stored or blacklisted. Skipping...");
								}
							}
						} else {
							// Harvester is disabled so provider stops getting wallpapers
							if (LOG.isInfoEnabled()) {
								LOG.info("Harvesting process has been disabled. Stopping provider " + this.getClass().getName());
							}
							wallpaperFound = Boolean.TRUE;
							break;
						}
					}
					if (!wallpaperFound) {
						// If no wallpaper is found in this page, the offset is incremented
						// For incrementing the offset the number of thumbnails will be used
						Integer newOffset = Integer.parseInt(this.getOffset()) + thumbnails.size();
						if (LOG.isInfoEnabled()) {
							LOG.info("Continuing search at offset " + newOffset);
						}
						this.setOffset(Integer.toString(newOffset));
						completeURL = composeCompleteURL();
					}
				} else {
					wallpaperFound = Boolean.TRUE;
					// Resetting offset
					this.setOffset("0");
					if (LOG.isInfoEnabled()) {
						LOG.info("No more wallpapers found. Skipping...");
					}
				}
			}
		} catch (IOException exception) {
			if (LOG.isInfoEnabled()) {
				LOG.error("Exception processing " + exception);
			}
			throw new ProviderException("There was a problem downloading a wallpaper. Complete URL -> " + completeURL + ". Message: " + exception.getMessage());
		} catch (ProviderException providerException) {
			if (LOG.isInfoEnabled()) {
				LOG.error("Exception processing " + providerException);
			}
			throw providerException;
		}
	}
		
	private String composeCompleteURL() {
		// If user set All, then keywords will be used
		PreferencesManager prefm = PreferencesManager.getInstance();
		String keywordString = "";
		String completeURL = "";
		switch (new Integer(prefm.getPreference("wallpaper-devianart-search-type"))) {
			case 0:
				// No topic. The keywords set by the user will be used
				// If activeKeyword is empty, the search operation will be done within the whole repository
				// https://www.deviantart.com/search?q=naruto&page=1
				if (!this.activeKeyword.equals(PreferencesManager.DEFAULT_VALUE)) {
					// Replacing blank spaces with +
					this.activeKeyword = this.activeKeyword.replace(" ", "+");
					// Example: https://www.deviantart.com/search/deviations?order=popular-all-time&q=mandalorian
					keywordString = "search/deviations" + WDUtilities.QM + "order" + WDUtilities.EQUAL + "popular-all-time" + WDUtilities.AND + "q" + WDUtilities.EQUAL + this.activeKeyword + WDUtilities.AND + "page" + WDUtilities.EQUAL + this.offset;
				}
				break;
			default:
				if (LOG.isInfoEnabled()) {
					LOG.info("A specific topic has been selected so keywords set but user won't be applied");
				}
				keywordString = WDUtilities.QM + "page" + WDUtilities.EQUAL + this.offset;
				break;
		}
		completeURL = this.baseURL + keywordString;
		
		if (LOG.isInfoEnabled()) {
			LOG.info(completeURL);
		}
		return completeURL;
	}
}