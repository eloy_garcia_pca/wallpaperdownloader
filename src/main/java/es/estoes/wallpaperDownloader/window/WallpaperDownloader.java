/**
 * Copyright 2016-2021 Eloy García Almadén <eloy.garcia.pca@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.estoes.wallpaperDownloader.window;

import java.awt.EventQueue;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import javax.swing.border.Border;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import es.estoes.wallpaperDownloader.changer.ChangerDaemon;
import es.estoes.wallpaperDownloader.changer.LinuxWallpaperChanger;
import es.estoes.wallpaperDownloader.harvest.Harvester;
import es.estoes.wallpaperDownloader.item.ComboItem;
import es.estoes.wallpaperDownloader.provider.UnsplashProvider;
import es.estoes.wallpaperDownloader.util.ImageUtilities;
import es.estoes.wallpaperDownloader.util.PreferencesManager;
import es.estoes.wallpaperDownloader.util.PropertiesManager;
import es.estoes.wallpaperDownloader.util.WDConfigManager;
import es.estoes.wallpaperDownloader.util.WDUtilities;
import es.estoes.wallpaperDownloader.util.WallpaperListRenderer;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import java.awt.AWTException;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.Insets;
import java.awt.PopupMenu;
import java.awt.Rectangle;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowStateListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.NumberFormat;
import javax.swing.JLabel;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tray;
import javax.swing.JComboBox;
import java.text.Format;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JTextPane;
import javax.swing.Icon;
import java.awt.Font;

public class WallpaperDownloader {

	// Constants
	protected static final Logger LOG = Logger.getLogger(WallpaperDownloader.class);
	private static final PropertiesManager pm = PropertiesManager.getInstance();
	
	// Attributes
	protected static WallpaperDownloader window;
	private static JFrame frmWallpaperdownloaderV;
	private static ResourceBundle i18nBundle;
	public static boolean fromSystemTray;
	// diskSpacePB will be an attribute representing disk space occupied within the downloads directory
	// It is static because it will be able to be accessed from any point within the application's code
	public static JProgressBar diskSpacePB = new JProgressBar();
	public static JLabel lblSpaceWarning;
	public static JScrollPane scroll;
	public static JList<ImageIcon> lastWallpapersList;
	public static Harvester harvester;
	public static ChangerDaemon changer;
	private JTextField searchKeywords;
	private static JCheckBox wallhavenCheckbox;
	private static JCheckBox deviantArtCheckbox;
	private static JCheckBox bingCheckbox;
	private static JCheckBox socialWallpaperingCheckbox;
	private JCheckBox socialWallpaperingIgnoreKeywordsCheckbox;
	private static JCheckBox wallpaperFusionCheckbox;
	private static JCheckBox unsplashCheckbox;
	private JComboBox<ComboItem> searchTypeUnsplashComboBox;
	private JButton btnChangeResolution;
	private JButton btnMinimize;
	private JButton btnOpenDownloadsDirectory;
	private JButton btnClipboard;
	private JComboBox<ComboItem> downloadPolicyComboBox;
	private JComboBox<ComboItem> searchTypeWallhavenComboBox;
	private JFormattedTextField widthResolution;
	private JLabel lblX;
	private JFormattedTextField heightResolution;
	private NumberFormat integerFormat;
	private JComboBox<ComboItem> deviantArtSearchTypeComboBox;
	private JButton btnChangeSize;
	private JButton btnApplySize;
	private JFormattedTextField downloadDirectorySize;
	private JPanel miscPanel;
	private JPanel wallpapersPanel;
	private JFormattedTextField downloadsDirectory;
	private JButton btnChangeDownloadsDirectory;
	private JButton btnManageWallpapers;
	private JButton btnRemoveWallpaper;
	private JButton btnSetFavoriteWallpaper;
	private JButton btnSetWallpaper;
	private JButton btnPreviewWallpaper;
	private JPanel aboutPanel;
	private JTextField version;
	private JSeparator aboutSeparator1;
	private JLabel lblDeveloper;
	private JTextField developer;
	private JLabel lblSourceCode;
	private JButton btnRepository;
	private JSeparator aboutSeparator2;
	private JTextField icons;
	private JButton btnIcons;
	private JPanel helpPanel;
	private JCheckBox multiMonitorCheckBox;
	private JButton btnChangeMoveDirectory;
	private JFormattedTextField moveDirectory;
	private JLabel lblMoveHelp;
	private JCheckBox moveFavoriteCheckBox;
	private JButton btnMoveWallpapers;
	private JButton btnRandomWallpaper;
	private JLabel lblNotifications;
	private JComboBox<ComboItem> notificationsComboBox;
	private JComboBox<ComboItem> i18nComboBox;
	private static JCheckBox startMinimizedCheckBox;
	private JComboBox<ComboItem> timeToMinimizeComboBox;
	private JCheckBox stIconCheckBox;
	private static JButton btnPause;
	private static JButton btnPlay;
	private static JPanel providersPanel;
	private static JLabel lblGreenSpot;
	private static JLabel lblRedSpot;
	private JList<String> listDirectoriesToWatch;
	private DefaultListModel<String> listDirectoriesModel;
	private JButton btnAddDirectory;
	private JButton btnRemoveDirectory;
	private JPanel appSettingsPanel;
	private JButton btnChooseWallpaper;
	private static JCheckBox dualMonitorCheckbox;
	private JLabel lblSearchTypeDualMonitor;
	private JComboBox<ComboItem> searchTypeDualMonitorComboBox;
	private JButton btnApplyResolution;
	private JButton btnResetResolution;
	private JButton btnChangeKeywords;
	private JButton btnApplyKeywords;
	private JLabel lblSystemTrayHelp;
	private JPanel changerPanel;
	private JSeparator settingsSeparator4;
	private JSeparator settingsSeparator5;
	private JCheckBox initOnBootCheckBox;
	private JButton btnResetSettings;
	private JButton btnChangeTimer;
	private JButton btnApplyTimer;
	private JComboBox<ComboItem> timeComboBox1;
	private JFormattedTextField downloadingTime;
	JFormattedTextField changingTime;
	private JButton btnChangeChangingTimer;
	private JButton btnApplyChangingTimer;
	private JComboBox<ComboItem> timeComboBox2;
	private JComboBox<ComboItem> modeComboBox;
	private JButton btnRestartWallpapers;
	private JButton btnNextWallpaper;
	private JTextField forbiddenKeywords;
	private JButton btnChangeForbiddenKeywords;
	private JButton btnApplyForbiddenKeywords;
	private JTextField contributors;
	
	// Getters & Setters
	public Harvester getHarvester() {
		return WallpaperDownloader.harvester;
	}

	public void setHarvester(Harvester harvester) {
		WallpaperDownloader.harvester = harvester;
	}

	public ChangerDaemon getChanger() {
		return WallpaperDownloader.changer;
	}

	public void setChanger(ChangerDaemon changer) {
		WallpaperDownloader.changer = changer;
	}

	public JFormattedTextField getDownloadsDirectory() {
		return downloadsDirectory;
	}

	public void setDownloadsDirectory(JFormattedTextField downloadsDirectory) {
		this.downloadsDirectory = downloadsDirectory;
	}

	public JFormattedTextField getMoveDirectory() {
		return moveDirectory;
	}

	public void setMoveDirectory(JFormattedTextField moveDirectory) {
		this.moveDirectory = moveDirectory;
	}

	public DefaultListModel<String> getListDirectoriesModel() {
		return listDirectoriesModel;
	}

	public void setListDirectoriesModel(DefaultListModel<String> listDirectoriesModel) {
		this.listDirectoriesModel = listDirectoriesModel;
	}

	public JButton getBtnRemoveDirectory() {
		return btnRemoveDirectory;
	}

	public void setBtnRemoveDirectory(JButton btnRemoveDirectory) {
		this.btnRemoveDirectory = btnRemoveDirectory;
	}

	public JPanel getChangerPanel() {
		return changerPanel;
	}

	public void setChangerPanel(JPanel changerPanel) {
		this.changerPanel = changerPanel;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				// Log configuration
				WDConfigManager.configureLog();

				// Application configuration
				WDConfigManager.checkConfig();
				
				window = new WallpaperDownloader();
			}
		});
	}

	/**
	 * Create the application.
	 */
	public WallpaperDownloader() {
		// Resource bundle for i18n
		i18nBundle = WDUtilities.getBundle();

		// Creating the main frame
		frmWallpaperdownloaderV = new JFrame();
		
		// Setting the system look & feel for the main frame
		String systemLookAndFeel = UIManager.getSystemLookAndFeelClassName();
		try {
        	if (systemLookAndFeel.equals("javax.swing.plaf.metal.MetalLookAndFeel") || WDUtilities.isSnapPackage()) {
        		UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");                		
        	} else {
        		UIManager.setLookAndFeel(systemLookAndFeel);                		
        	}
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException exception) {
            exception.printStackTrace();
            if (LOG.isInfoEnabled()) {
            	LOG.error("Error in system look and feel definition: Message: " + exception.getMessage());
            }
            try {
				UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
					| UnsupportedLookAndFeelException exception2) {
				exception2.printStackTrace();
	            if (LOG.isInfoEnabled()) {
	            	LOG.error("Error in traditional system look and feel definition: Message: " + exception.getMessage());
	            }
			}
        }
        SwingUtilities.updateComponentTreeUI(frmWallpaperdownloaderV);
		
        // Initializing the main frame
        initialize(frmWallpaperdownloaderV);
		
		// Configuring main frame after initialization
        // Colors, title and icon
		frmWallpaperdownloaderV.setBackground(new Color(255, 255, 255));
		frmWallpaperdownloaderV.setExtendedState(Frame.NORMAL);
		frmWallpaperdownloaderV.setVisible(true);
		frmWallpaperdownloaderV.setTitle("WallpaperDownloader V4.4.2");
        URL iconImageUrl = WallpaperDownloader.class.getResource("/images/desktop/wallpaperdownloader.png");;
		ImageIcon iconImage = new ImageIcon(iconImageUrl);
		frmWallpaperdownloaderV.setIconImage(iconImage.getImage());

		// Minimizing the application if start minimized feature is enable
		if (startMinimizedCheckBox.isSelected()) {
			try {
				// Sleeps during X seconds in order to avoid problems in GNOME 3 minimization
				if (WDUtilities.getWallpaperChanger() instanceof LinuxWallpaperChanger) {
					if (((LinuxWallpaperChanger)WDUtilities.getWallpaperChanger()).getDesktopEnvironment().equals(WDUtilities.DE_GNOME3)
						|| ((LinuxWallpaperChanger)WDUtilities.getWallpaperChanger()).getDesktopEnvironment().equals(WDUtilities.DE_KDE)) {
						PreferencesManager prefm = PreferencesManager.getInstance(); 
						TimeUnit.SECONDS.sleep(new Long(prefm.getPreference("time-to-minimize")));								
					}
				}
			} catch (InterruptedException exception) {
				if (LOG.isInfoEnabled()) {
					LOG.error("Error sleeping for 3 seconds. Message: " + exception.getMessage());
				}
			}
			minimizeApplication();
		}

		// Setting some listeners for the main frame
		// Command comes from system tray
		fromSystemTray = false;
		
		// Adding a listener for knowing when the main window changes its state
		frmWallpaperdownloaderV.addWindowStateListener(new WindowStateListener() {
			@Override
			public void windowStateChanged(WindowEvent windowEvent) {
				final PreferencesManager prefm = PreferencesManager.getInstance();
				String systemTrayIconEnable = prefm.getPreference("system-tray-icon");
				if (SystemTray.isSupported() && 
					!isOldSystemTray() && 
					systemTrayIconEnable.equals(WDUtilities.APP_YES)) {
					// Check if commands comes from system tray
					if (fromSystemTray) {
						fromSystemTray = false;
					} else {
						// If command doesn't come from system tray, then it is necessary to capture the
						// minimize order
						if (frmWallpaperdownloaderV.getExtendedState() == Frame.NORMAL){
							// The user has minimized the window
							try {
								TimeUnit.MILLISECONDS.sleep(100);
							} catch (InterruptedException exception) {
								if (LOG.isInfoEnabled()) {
									LOG.error("Error going to sleep: " + exception.getMessage());
								}
							}								
							minimizeApplication();
						} else {
							frmWallpaperdownloaderV.setExtendedState(Frame.NORMAL);
						}
					}
				}
			}					
		});
		
		// Adding a listener to know when the main window loses or gains focus
		frmWallpaperdownloaderV.addWindowFocusListener(new WindowFocusListener() {
			@Override
			public void windowGainedFocus(WindowEvent arg0) {
				// Nothing to do here
			}

			@Override
			public void windowLostFocus(WindowEvent arg0) {
				final PreferencesManager prefm = PreferencesManager.getInstance();
				String systemTrayIconEnable = prefm.getPreference("system-tray-icon");
				if (SystemTray.isSupported() && 
					!isOldSystemTray() && 
					systemTrayIconEnable.equals(WDUtilities.APP_YES)) {
					frmWallpaperdownloaderV.setExtendedState(Frame.NORMAL);							
				}
			}
		});
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings("serial")
	private void initialize(JFrame frame) {
		// Configuring tooltips
		ToolTipManager.sharedInstance().setInitialDelay(100);
		
		frame.setBounds(100, 100, 694, 445);
		// If the system tray is old, then windows must be bigger in order to paint Minimize button
		if (isOldSystemTray()) {
			frame.setBounds(100, 100, 694, 484);
		}
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{116, 386, 73, 96, 0};
		gridBagLayout.rowHeights = new int[]{338, 53, 25, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		// Centering window
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		int x = (screenSize.width - frame.getWidth()) / 2;
		int y = (screenSize.height - frame.getHeight()) / 2;
		frame.setLocation(x, y);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBorder(null);
		GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
		gbc_tabbedPane.gridheight = 3;
		gbc_tabbedPane.fill = GridBagConstraints.BOTH;
		gbc_tabbedPane.insets = new Insets(0, 0, 5, 0);
		gbc_tabbedPane.gridwidth = 4;
		gbc_tabbedPane.gridx = 0;
		gbc_tabbedPane.gridy = 0;
		frame.getContentPane().add(tabbedPane, gbc_tabbedPane);
		
		// Providers (tab)
		providersPanel = new JPanel();
		providersPanel.setBorder(null);
		tabbedPane.addTab(i18nBundle.getString("providers.title"), null, providersPanel, null);
		providersPanel.setLayout(null);

		wallhavenCheckbox = new JCheckBox(i18nBundle.getString("providers.wallhaven.title"));
		wallhavenCheckbox.setBounds(13, 149, 129, 23);
		providersPanel.add(wallhavenCheckbox);

		JLabel lblKeywords = new JLabel(i18nBundle.getString("providers.keywords"));
		lblKeywords.setBounds(12, 14, 70, 15);
		providersPanel.add(lblKeywords);
		
		searchKeywords = new JTextField();
		searchKeywords.setFont(new Font("Dialog", Font.PLAIN, 11));
		searchKeywords.setBounds(100, 8, 295, 27);
		providersPanel.add(searchKeywords);
		searchKeywords.setColumns(10);

		btnChangeKeywords = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/edit_16px_icon.png"));
			btnChangeKeywords.setIcon(new ImageIcon(img));
			btnChangeKeywords.setToolTipText(i18nBundle.getString("providers.change.keywords"));
			btnChangeKeywords.setBounds(436, 6, 34, 33);
		} catch (IOException ex) {
			btnChangeKeywords.setText(i18nBundle.getString("providers.change.keywords"));
			btnChangeKeywords.setBounds(436, 6, 131, 25);
		}
		providersPanel.add(btnChangeKeywords);
		
		btnApplyKeywords = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/accept_16px_icon.png"));
			btnApplyKeywords.setIcon(new ImageIcon(img));
			btnApplyKeywords.setToolTipText(i18nBundle.getString("providers.save.changes"));
			btnApplyKeywords.setBounds(436, 6, 34, 33);
		} catch (IOException ex) {
			btnApplyKeywords.setText(i18nBundle.getString("providers.save.changes"));
			btnApplyKeywords.setBounds(436, 6, 131, 25);
		}

		forbiddenKeywords = new JTextField();
		forbiddenKeywords.setFont(new Font("Dialog", Font.PLAIN, 11));
		forbiddenKeywords.setEnabled(false);
		forbiddenKeywords.setColumns(10);
		forbiddenKeywords.setBounds(100, 47, 295, 27);
		providersPanel.add(forbiddenKeywords);

		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/help_24px_icon.png"));
			ImageIcon icon = new ImageIcon(img);
			JLabel lblKeywordsHelp = new JLabel(icon);
			lblKeywordsHelp.setToolTipText(i18nBundle.getString("providers.keywords.help"));
			lblKeywordsHelp.setBounds(398, 12, 30, 23);
			providersPanel.add(lblKeywordsHelp);
		} catch (IOException ex) {
			JLabel lblKeywordsHelp = new JLabel("(separated by ;) (Empty->All wallpapers)");
			lblKeywordsHelp.setBounds(362, 39, 70, 15);
			providersPanel.add(lblKeywordsHelp);
		}

		JLabel lblAvoid = new JLabel(i18nBundle.getString("providers.keywords.avoid"));
		lblAvoid.setBounds(12, 52, 70, 15);
		providersPanel.add(lblAvoid);
		
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/help_24px_icon.png"));
			ImageIcon icon = new ImageIcon(img);
			JLabel lblAvoidHelp = new JLabel(icon);
			lblAvoidHelp.setToolTipText(i18nBundle.getString("providers.keywords.avoid.help"));
			lblAvoidHelp.setBounds(398, 47, 30, 23);
			providersPanel.add(lblAvoidHelp);
		} catch (IOException ex) {
			JLabel lblAvoidHelp = new JLabel("(separated by ;) (Empty->All wallpapers)");
			lblAvoidHelp.setBounds(398, 47, 30, 23);
			providersPanel.add(lblAvoidHelp);
		}

		btnChangeForbiddenKeywords = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/edit_16px_icon.png"));
			btnChangeForbiddenKeywords.setIcon(new ImageIcon(img));
			btnChangeForbiddenKeywords.setToolTipText(i18nBundle.getString("providers.change.keywords.avoid"));
			btnChangeForbiddenKeywords.setBounds(436, 42, 34, 33);
		} catch (IOException ex) {
			btnChangeForbiddenKeywords.setText(i18nBundle.getString("providers.change.keywords.avoid"));
			btnChangeForbiddenKeywords.setBounds(436, 42, 34, 33);
		}
		providersPanel.add(btnChangeForbiddenKeywords);
		
		btnApplyForbiddenKeywords = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/accept_16px_icon.png"));
			btnApplyForbiddenKeywords.setIcon(new ImageIcon(img));
			btnApplyForbiddenKeywords.setToolTipText(i18nBundle.getString("providers.save.changes"));
			btnApplyForbiddenKeywords.setBounds(436, 42, 34, 33);
		} catch (IOException ex) {
			btnApplyForbiddenKeywords.setText(i18nBundle.getString("providers.save.changes"));
			btnApplyForbiddenKeywords.setBounds(436, 42, 34, 33);
		}

		JSeparator separator1 = new JSeparator();
		separator1.setBounds(12, 144, 610, 2);
		providersPanel.add(separator1);
				
		// Only integers will be allowed
		integerFormat = NumberFormat.getNumberInstance();
		integerFormat.setParseIntegerOnly(true);
		
		searchTypeWallhavenComboBox = new JComboBox<ComboItem>();
		searchTypeWallhavenComboBox.setBounds(321, 151, 149, 19);
		providersPanel.add(searchTypeWallhavenComboBox);
		JLabel lblSearchTypeWallhaven = new JLabel(i18nBundle.getString("providers.wallhaven.search.type"));
		lblSearchTypeWallhaven.setBounds(224, 154, 94, 15);
		providersPanel.add(lblSearchTypeWallhaven);
		
		integerFormat = NumberFormat.getNumberInstance();
		integerFormat.setParseIntegerOnly(true);

		JSeparator separator2 = new JSeparator();
		separator2.setBounds(13, 175, 610, 2);
		providersPanel.add(separator2);
		
		deviantArtCheckbox = new JCheckBox(i18nBundle.getString("providers.deviantart.title"));
		deviantArtCheckbox.setBounds(13, 178, 129, 23);
		providersPanel.add(deviantArtCheckbox);
		
		JLabel lblDeviantArtSearchType = new JLabel(i18nBundle.getString("providers.wallhaven.search.type"));
		lblDeviantArtSearchType.setBounds(224, 182, 94, 15);
		providersPanel.add(lblDeviantArtSearchType);

		deviantArtSearchTypeComboBox = new JComboBox<ComboItem>();
		deviantArtSearchTypeComboBox.setBounds(321, 180, 150, 19);
		providersPanel.add(deviantArtSearchTypeComboBox);

		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/help_24px_icon.png"));
			ImageIcon icon = new ImageIcon(img);
			JLabel lblDeviantArtHelp = new JLabel(icon);
			lblDeviantArtHelp.setToolTipText(i18nBundle.getString("providers.deviant.art.help"));
			lblDeviantArtHelp.setBounds(488, 178, 30, 23);
			providersPanel.add(lblDeviantArtHelp);
		} catch (IOException ex) {
			JLabel lblDeviantArtHelp = new JLabel(i18nBundle.getString("providers.deviant.art.help"));
			lblDeviantArtHelp.setBounds(488, 178, 30, 23);
			providersPanel.add(lblDeviantArtHelp);
		}

		JSeparator separator3 = new JSeparator();
		separator3.setBounds(13, 202, 610, 2);
		providersPanel.add(separator3);
		
		bingCheckbox = new JCheckBox(i18nBundle.getString("providers.bing.title"));
		bingCheckbox.setBounds(13, 205, 249, 23);
		providersPanel.add(bingCheckbox);
		
		JSeparator separator4 = new JSeparator();
		separator4.setBounds(14, 231, 610, 2);
		providersPanel.add(separator4);
		
		socialWallpaperingCheckbox = new JCheckBox(i18nBundle.getString("providers.social.wallpapering.title"));
		socialWallpaperingCheckbox.setBounds(13, 235, 194, 23);
		providersPanel.add(socialWallpaperingCheckbox);
		
		socialWallpaperingIgnoreKeywordsCheckbox = new JCheckBox(i18nBundle.getString("providers.social.wallpapering.ignore"));
		socialWallpaperingIgnoreKeywordsCheckbox.setBounds(221, 235, 143, 23);
		providersPanel.add(socialWallpaperingIgnoreKeywordsCheckbox);
		
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/help_24px_icon.png"));
			ImageIcon icon = new ImageIcon(img);
			JLabel lblIgnoreKeywordsSocialWallpaperingHelp = new JLabel(icon);
			lblIgnoreKeywordsSocialWallpaperingHelp.setToolTipText(i18nBundle.getString("providers.social.wallpapering.help"));
			lblIgnoreKeywordsSocialWallpaperingHelp.setBounds(365, 235, 30, 23);
			providersPanel.add(lblIgnoreKeywordsSocialWallpaperingHelp);
		} catch (IOException ex) {
			JLabel lblIgnoreKeywordsSocialWallpaperingHelp = new JLabel(i18nBundle.getString("providers.social.wallpapering.ignore"));
			lblIgnoreKeywordsSocialWallpaperingHelp.setBounds(366, 212, 30, 23);
			providersPanel.add(lblIgnoreKeywordsSocialWallpaperingHelp);
		}

		JSeparator separator5 = new JSeparator();
		separator5.setBounds(13, 261, 610, 2);
		providersPanel.add(separator5);
		
		wallpaperFusionCheckbox = new JCheckBox(i18nBundle.getString("providers.wallpaperfusion.title"));
		wallpaperFusionCheckbox.setBounds(13, 263, 210, 23);
		providersPanel.add(wallpaperFusionCheckbox);
		
		JSeparator separator6 = new JSeparator();
		separator6.setBounds(13, 289, 610, 2);
		providersPanel.add(separator6);
		
		dualMonitorCheckbox = new JCheckBox(i18nBundle.getString("providers.dual.monitor.backgrounds.title"));
		dualMonitorCheckbox.setBounds(13, 294, 201, 23);
		providersPanel.add(dualMonitorCheckbox);
		
		lblSearchTypeDualMonitor = new JLabel(i18nBundle.getString("providers.wallhaven.search.type"));
		lblSearchTypeDualMonitor.setBounds(226, 298, 94, 15);
		providersPanel.add(lblSearchTypeDualMonitor);

		searchTypeDualMonitorComboBox = new JComboBox<ComboItem>();
		searchTypeDualMonitorComboBox.setBounds(323, 296, 149, 19);
		providersPanel.add(searchTypeDualMonitorComboBox);

		JSeparator separator8 = new JSeparator();
		separator8.setBounds(13, 319, 610, 2);
		providersPanel.add(separator8);
		
		unsplashCheckbox = new JCheckBox(i18nBundle.getString("providers.unsplash.title"));
		unsplashCheckbox.setBounds(13, 323, 201, 23);
		providersPanel.add(unsplashCheckbox);
		
		JLabel lblSearchTypeUnsplash = new JLabel(i18nBundle.getString("providers.unsplash.search.type"));
		lblSearchTypeUnsplash.setBounds(226, 326, 94, 15);
		providersPanel.add(lblSearchTypeUnsplash);

		searchTypeUnsplashComboBox = new JComboBox<ComboItem>();
		searchTypeUnsplashComboBox.setBounds(323, 324, 149, 19);
		providersPanel.add(searchTypeUnsplashComboBox);

		JLabel lblResolution = new JLabel(i18nBundle.getString("providers.resolution"));
		lblResolution.setBounds(13, 86, 94, 15);
		providersPanel.add(lblResolution);
		
		widthResolution = new JFormattedTextField((Format) null);
		widthResolution.setColumns(4);
		widthResolution.setBounds(100, 83, 53, 28);
		providersPanel.add(widthResolution);
		
		lblX = new JLabel("x");
		lblX.setBounds(153, 88, 12, 15);
		providersPanel.add(lblX);
		
		heightResolution = new JFormattedTextField((Format) null);
		heightResolution.setColumns(4);
		heightResolution.setBounds(159, 83, 53, 28);
		providersPanel.add(heightResolution);
		
		btnChangeResolution = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/edit_16px_icon.png"));
			btnChangeResolution.setIcon(new ImageIcon(img));
			btnChangeResolution.setToolTipText(i18nBundle.getString("providers.change.resolution"));
			btnChangeResolution.setBounds(214, 80, 34, 33);
		} catch (IOException ex) {
			btnChangeResolution.setText("Change Res.");
			btnChangeResolution.setBounds(224, 38, 131, 25);
		}
		providersPanel.add(btnChangeResolution);

		btnResetResolution = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/reset_16px_icon.png"));
			btnResetResolution.setIcon(new ImageIcon(img));
			btnResetResolution.setToolTipText(i18nBundle.getString("providers.reset.resolution"));
			btnResetResolution.setBounds(250, 80, 34, 33);
		} catch (IOException ex) {
			btnResetResolution.setText("Reset");
			btnResetResolution.setBounds(357, 80, 72, 25);
		}

		btnApplyResolution = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/accept_16px_icon.png"));
			btnApplyResolution.setIcon(new ImageIcon(img));
			btnApplyResolution.setToolTipText(i18nBundle.getString("providers.save.changes"));
			btnApplyResolution.setBounds(214, 80, 34, 33);
		} catch (IOException ex) {
			btnApplyResolution.setText("Apply");
			btnApplyResolution.setBounds(224, 80, 131, 25);
		}

		JLabel lblDownloadPolicy = new JLabel(i18nBundle.getString("providers.download.policy"));
		lblDownloadPolicy.setBounds(13, 118, 149, 15);
		providersPanel.add(lblDownloadPolicy);
		
		downloadPolicyComboBox = new JComboBox<ComboItem>();
		downloadPolicyComboBox.setBounds(159, 114, 463, 24);
		providersPanel.add(downloadPolicyComboBox);
						
		btnPause = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/pause_16px_icon.png"));
			btnPause.setIcon(new ImageIcon(img));
			btnPause.setToolTipText(i18nBundle.getString("providers.pause"));
			btnPause.setBounds(552, 6, 34, 33);
		} catch (IOException ex) {
			btnPause.setToolTipText(i18nBundle.getString("providers.pause"));
			btnPause.setBounds(431, 6, 34, 33);
		}

		btnPlay = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/play_16px_icon.png"));
			btnPlay.setIcon(new ImageIcon(img));
			btnPlay.setToolTipText(i18nBundle.getString("providers.resume"));
			btnPlay.setBounds(552, 6, 34, 33);
		} catch (IOException ex) {
			btnPlay.setToolTipText(i18nBundle.getString("providers.resume"));
			btnPlay.setBounds(431, 6, 34, 33);
		}
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/green_spot_24px_icon.png"));
			ImageIcon icon = new ImageIcon(img);
			lblGreenSpot = new JLabel(icon);			
			lblGreenSpot.setToolTipText(i18nBundle.getString("providers.downloading.process.enabled"));
			lblGreenSpot.setBounds(599, 16, 20, 18);
		} catch (IOException ex) {
			lblGreenSpot = new JLabel(i18nBundle.getString("providers.downloading.process.enabled"));
			lblGreenSpot.setBounds(599, 16, 30, 23);
		}

		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/red_spot_24px_icon.png"));
			ImageIcon icon = new ImageIcon(img);
			lblRedSpot = new JLabel(icon);			
			lblRedSpot.setToolTipText(i18nBundle.getString("providers.downloading.process.disabled"));
			lblRedSpot.setBounds(599, 16, 20, 18);
		} catch (IOException ex) {
			lblRedSpot = new JLabel(i18nBundle.getString("providers.downloading.process.disabled"));
			lblRedSpot.setBounds(599, 16, 30, 23);
		}

		// Application Settings (tab)
		appSettingsPanel = new JPanel();
		appSettingsPanel.setBorder(null);
		tabbedPane.addTab(i18nBundle.getString("application.settings.title"), null, appSettingsPanel, null);
		appSettingsPanel.setLayout(null);
		
		JLabel lblTimer = new JLabel(i18nBundle.getString("application.settings.downloading.time"));
		lblTimer.setBounds(12, 7, 439, 19);
		appSettingsPanel.add(lblTimer);
		JLabel lblDownloadDirectorySize = new JLabel(i18nBundle.getString("application.settings.maximum.size"));
		lblDownloadDirectorySize.setBounds(12, 34, 304, 19);
		appSettingsPanel.add(lblDownloadDirectorySize);
		
		downloadDirectorySize = new JFormattedTextField(integerFormat);
		downloadDirectorySize.setColumns(4);
		downloadDirectorySize.setBounds(313, 28, 56, 28);
		appSettingsPanel.add(downloadDirectorySize);
		
		JSeparator settingsSeparator1 = new JSeparator();
		settingsSeparator1.setBounds(12, 62, 631, 2);
		appSettingsPanel.add(settingsSeparator1);
		
		moveFavoriteCheckBox = new JCheckBox(i18nBundle.getString("application.settings.move"));
		moveFavoriteCheckBox.setBounds(12, 72, 226, 23);
		appSettingsPanel.add(moveFavoriteCheckBox);

		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/help_24px_icon.png"));
			ImageIcon icon = new ImageIcon(img);
			lblMoveHelp = new JLabel(icon);
			lblMoveHelp.setToolTipText(i18nBundle.getString("application.settings.move.help"));
			lblMoveHelp.setBounds(251, 72, 30, 23);
			appSettingsPanel.add(lblMoveHelp);
		} catch (IOException ex) {
			JLabel lblMoveHelp = new JLabel(i18nBundle.getString("application.settings.move.help"));
			lblMoveHelp.setBounds(213, 85, 30, 23);
			appSettingsPanel.add(lblMoveHelp);
		}
		
		JLabel lblMoveFavoriteDirectory = new JLabel(i18nBundle.getString("application.settings.move.directory"));
		lblMoveFavoriteDirectory.setBounds(12, 103, 134, 19);
		appSettingsPanel.add(lblMoveFavoriteDirectory);
		moveDirectory = new JFormattedTextField((Format) null);
		moveDirectory.setEditable(false);
		moveDirectory.setColumns(4);
		moveDirectory.setBounds(144, 96, 405, 31);
		appSettingsPanel.add(moveDirectory);
		
		btnChangeMoveDirectory = new JButton();
		
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/change_folder_24px_icon.png"));
			btnChangeMoveDirectory.setIcon(new ImageIcon(img));
			btnChangeMoveDirectory.setToolTipText(i18nBundle.getString("application.settings.move.button.help"));
			btnChangeMoveDirectory.setBounds(561, 95, 34, 33);
		} catch (IOException ex) {
			btnChangeMoveDirectory.setText(i18nBundle.getString("application.settings.move.button.help"));
			btnChangeMoveDirectory.setBounds(561, 107, 34, 33);
		}	
		appSettingsPanel.add(btnChangeMoveDirectory);

		JSeparator settingsSeparator2 = new JSeparator();
		settingsSeparator2.setBounds(12, 134, 631, 2);
		appSettingsPanel.add(settingsSeparator2);
		
		lblNotifications = new JLabel(i18nBundle.getString("application.settings.notifications"));
		lblNotifications.setBounds(12, 143, 126, 19);
		appSettingsPanel.add(lblNotifications);
		
		notificationsComboBox = new JComboBox<ComboItem>();
		notificationsComboBox.setBounds(143, 140, 134, 23);
		appSettingsPanel.add(notificationsComboBox);
		
		btnChangeSize = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/edit_16px_icon.png"));
			btnChangeSize.setIcon(new ImageIcon(img));
			btnChangeSize.setToolTipText(i18nBundle.getString("application.settings.change.size"));
			btnChangeSize.setBounds(376, 26, 34, 33);
		} catch (IOException ex) {
			btnChangeSize.setText(i18nBundle.getString("application.settings.change.size"));
			btnChangeSize.setBounds(376, 26, 131, 25);
		}
		appSettingsPanel.add(btnChangeSize);

		btnApplySize = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/accept_16px_icon.png"));
			btnApplySize.setIcon(new ImageIcon(img));
			btnApplySize.setToolTipText(i18nBundle.getString("providers.save.changes"));
			btnApplySize.setBounds(376, 26, 34, 33);
		} catch (IOException ex) {
			btnApplySize.setText(i18nBundle.getString("providers.save.changes"));
			btnApplySize.setBounds(376, 26, 131, 25);
		}

		JSeparator settingsSeparator3 = new JSeparator();
		settingsSeparator3.setBounds(12, 168, 631, 2);
		appSettingsPanel.add(settingsSeparator3);

		startMinimizedCheckBox = new JCheckBox(i18nBundle.getString("application.settings.start.minimized"));
		startMinimizedCheckBox.setBounds(12, 173, 179, 23);
		appSettingsPanel.add(startMinimizedCheckBox);
				
		JLabel lblTimeToMinimize = new JLabel(i18nBundle.getString("application.settings.time.minimize"));
		lblTimeToMinimize.setBounds(12, 201, 126, 19);
		appSettingsPanel.add(lblTimeToMinimize);
		
		timeToMinimizeComboBox = new JComboBox<ComboItem>();
		timeToMinimizeComboBox.setBounds(144, 199, 134, 24);
		appSettingsPanel.add(timeToMinimizeComboBox);

		settingsSeparator4 = new JSeparator();
		settingsSeparator4.setBounds(12, 228, 631, 2);
		appSettingsPanel.add(settingsSeparator4);

		JLabel lblI18n = new JLabel(i18nBundle.getString("application.settings.i18n"));
		lblI18n.setBounds(12, 235, 104, 19);
		appSettingsPanel.add(lblI18n);

		i18nComboBox = new JComboBox<ComboItem>();
		i18nComboBox.setBounds(144, 233, 134, 23);
		appSettingsPanel.add(i18nComboBox);

		settingsSeparator5 = new JSeparator();
		settingsSeparator5.setBounds(12, 260, 631, 2);
		appSettingsPanel.add(settingsSeparator5);

		initOnBootCheckBox = new JCheckBox(i18nBundle.getString("application.settings.starts.on.boot"));
		initOnBootCheckBox.setBounds(12, 265, 631, 23);
		appSettingsPanel.add(initOnBootCheckBox);

		// Only if system tray is supported, no MacOS and it is AMD/Intel 64 architecture, user will be 
		// able to minimize the application into the system tray
		if (SystemTray.isSupported() && 
				!WDUtilities.getOperatingSystem().equals(WDUtilities.OS_MACOSX) &&
				WDUtilities.getArchitecture().equals(WDUtilities.AMD_ARCHITECTURE)) {
			JSeparator settingsSeparator6 = new JSeparator();
			settingsSeparator6.setBounds(12, 293, 631, 2);
			appSettingsPanel.add(settingsSeparator6);

		    stIconCheckBox = new JCheckBox(i18nBundle.getString("application.settings.system.tray.icon"));
			stIconCheckBox.setBounds(13, 298, 225, 26);
			appSettingsPanel.add(stIconCheckBox);
			
			lblSystemTrayHelp = new JLabel((Icon) null);
			try {
				Image img = ImageIO.read(getClass().getResource("/images/icons/help_24px_icon.png"));
				ImageIcon icon = new ImageIcon(img);
				lblSystemTrayHelp = new JLabel(icon);
				lblSystemTrayHelp.setToolTipText(i18nBundle.getString("application.settings.system.tray.icon.help"));
				lblSystemTrayHelp.setBounds(252, 298, 30, 23);
				appSettingsPanel.add(lblSystemTrayHelp);
				
			} catch (IOException ex) {
				lblSystemTrayHelp = new JLabel(i18nBundle.getString("application.settings.system.tray.icon.help"));
				lblSystemTrayHelp.setBounds(566, 173, 30, 23);
				appSettingsPanel.add(lblSystemTrayHelp);
			}
			
			JSeparator settingsSeparator7 = new JSeparator();
			settingsSeparator7.setBounds(12, 327, 631, 2);
			appSettingsPanel.add(settingsSeparator7);

		}
						
		btnResetSettings = new JButton(i18nBundle.getString("application.settings.reset.settings"));
		btnResetSettings.setBackground(Color.WHITE);
		btnResetSettings.setBounds(12, 332, 189, 25);
		appSettingsPanel.add(btnResetSettings);
		
		btnChangeTimer = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/edit_16px_icon.png"));
			btnChangeTimer.setIcon(new ImageIcon(img));
			btnChangeTimer.setToolTipText(i18nBundle.getString("application.settings.change.downloading.time"));
			btnChangeTimer.setBounds(612, 2, 34, 33);
		} catch (IOException ex) {
			btnChangeTimer.setText(i18nBundle.getString("application.settings.change.downloading.time"));
			btnChangeTimer.setBounds(581, 2, 131, 25);
		}
		appSettingsPanel.add(btnChangeTimer);

		btnApplyTimer = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/accept_16px_icon.png"));
			btnApplyTimer.setIcon(new ImageIcon(img));
			btnApplyTimer.setToolTipText(i18nBundle.getString("providers.save.changes"));
			btnApplyTimer.setBounds(612, 2, 34, 33);
		} catch (IOException ex) {
			btnApplyTimer.setText(i18nBundle.getString("providers.save.changes"));
			btnApplyTimer.setBounds(581, 2, 131, 25);
		}
		appSettingsPanel.add(btnApplyTimer);

		timeComboBox1 = new JComboBox<ComboItem>();
		timeComboBox1.setEnabled(false);
		timeComboBox1.setBounds(533, 5, 74, 24);
		appSettingsPanel.add(timeComboBox1);
		
		// downloadingTime would only be a integer of 4 numbers maximum
		NumberFormat integerFieldFormatter = NumberFormat.getIntegerInstance();
		integerFieldFormatter.setGroupingUsed(false);
		downloadingTime = new JFormattedTextField(integerFieldFormatter);
		downloadingTime.setEnabled(false);
		downloadingTime.setColumns(4);
		downloadingTime.setBounds(470, 3, 56, 28);
		appSettingsPanel.add(downloadingTime);
				
		// Automatic changer (tab)
		// Only visible for DE which allows automated changer
		if (WDUtilities.getWallpaperChanger().isWallpaperChangeable()) {
			changerPanel = new JPanel();
			changerPanel.setBorder(null);
			tabbedPane.addTab(i18nBundle.getString("changer.title"), null, changerPanel, null);
			changerPanel.setLayout(null);

			JLabel lblChanger = new JLabel(i18nBundle.getString("changer.change.every"));
			lblChanger.setBounds(12, 4, 179, 19);
			changerPanel.add(lblChanger);

			JLabel labelChangerMode = new JLabel(i18nBundle.getString("changer.mode"));
			labelChangerMode.setBounds(12, 45, 216, 19);
			changerPanel.add(labelChangerMode);
			
			modeComboBox = new JComboBox<ComboItem>();
			modeComboBox.setBounds(274, 43, 275, 24);
			changerPanel.add(modeComboBox);

			btnRestartWallpapers = new JButton();
			try {
				Image img = ImageIO.read(getClass().getResource("/images/icons/refresh_16px_icon.png"));
				btnRestartWallpapers.setIcon(new ImageIcon(img));
				btnRestartWallpapers.setToolTipText(i18nBundle.getString("changer.restart.wallpapers"));
				btnRestartWallpapers.setBounds(554, 38, 34, 33);
			} catch (IOException ex) {
				btnRestartWallpapers.setToolTipText(i18nBundle.getString("changer.restart.wallpapers"));
				btnRestartWallpapers.setBounds(554, 38, 34, 33);
			}		
			changerPanel.add(btnRestartWallpapers);

			JLabel lblChangerDirectory = new JLabel(i18nBundle.getString("changer.change.directory"));
			lblChangerDirectory.setBounds(12, 73, 537, 19);
			changerPanel.add(lblChangerDirectory);

			btnAddDirectory = new JButton();
			try {
				Image img = ImageIO.read(getClass().getResource("/images/icons/add_16px_icon.png"));
				btnAddDirectory.setIcon(new ImageIcon(img));
				btnAddDirectory.setToolTipText(i18nBundle.getString("changer.change.add.directory"));
				btnAddDirectory.setBounds(554, 164, 34, 33);
			} catch (IOException ex) {
				btnAddDirectory.setToolTipText(i18nBundle.getString("changer.change.add.directory"));
				btnAddDirectory.setBounds(554, 164, 34, 33);
			}		

			btnRemoveDirectory = new JButton();
			try {
				Image img = ImageIO.read(getClass().getResource("/images/icons/remove_16px_icon.png"));
				btnRemoveDirectory.setIcon(new ImageIcon(img));
				btnRemoveDirectory.setToolTipText(i18nBundle.getString("changer.change.remove.directory"));
				btnRemoveDirectory.setBounds(554, 209, 34, 33);
			} catch (IOException ex) {
				btnRemoveDirectory.setToolTipText(i18nBundle.getString("changer.change.remove.directory"));
				btnRemoveDirectory.setBounds(554, 319, 34, 33);
			}		

			JScrollPane listDirectoriesScrollPane = new JScrollPane();
			listDirectoriesScrollPane.setBounds(143, 93, 406, 225);
			changerPanel.add(listDirectoriesScrollPane);
			
			listDirectoriesToWatch = new JList<String>();
			listDirectoriesScrollPane.setColumnHeaderView(listDirectoriesToWatch);
			listDirectoriesToWatch.setBackground(UIManager.getColor("Button.background"));
			listDirectoriesToWatch.setToolTipText(i18nBundle.getString("changer.change.directory.help"));
			listDirectoriesToWatch.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
			listDirectoriesToWatch.setLayoutOrientation(JList.HORIZONTAL_WRAP);
			listDirectoriesToWatch.setVisibleRowCount(-1);
			
			listDirectoriesScrollPane.setViewportView(listDirectoriesToWatch);
			
			// changingTime would only be a integer of 4 numbers maximum
			changingTime = new JFormattedTextField(integerFieldFormatter);
			changingTime.setText((String) null);
			changingTime.setEnabled(false);
			changingTime.setColumns(4);
			changingTime.setBounds(213, 2, 68, 28);
			changerPanel.add(changingTime);
			
			timeComboBox2 = new JComboBox<ComboItem>();
			timeComboBox2.setEnabled(false);
			timeComboBox2.setBounds(287, 4, 78, 24);
			changerPanel.add(timeComboBox2);
			
			btnChangeChangingTimer = new JButton();
			try {
				Image img = ImageIO.read(getClass().getResource("/images/icons/edit_16px_icon.png"));
				btnChangeChangingTimer.setIcon(new ImageIcon(img));
				btnChangeChangingTimer.setToolTipText(i18nBundle.getString("changer.change.changing.time"));
				btnChangeChangingTimer.setBounds(377, 1, 34, 33);
			} catch (IOException ex) {
				btnChangeChangingTimer.setText(i18nBundle.getString("changer.change.changing.time"));
				btnChangeChangingTimer.setBounds(324, 1, 131, 25);
			}
			changerPanel.add(btnChangeChangingTimer);

			btnApplyChangingTimer = new JButton();
			try {
				Image img = ImageIO.read(getClass().getResource("/images/icons/accept_16px_icon.png"));
				btnApplyChangingTimer.setIcon(new ImageIcon(img));
				btnApplyChangingTimer.setToolTipText(i18nBundle.getString("providers.save.changes"));
				btnApplyChangingTimer.setBounds(377, 1, 34, 33);
			} catch (IOException ex) {
				btnApplyChangingTimer.setText(i18nBundle.getString("providers.save.changes"));
				btnApplyChangingTimer.setBounds(324, 1, 131, 25);
			}
			changerPanel.add(btnApplyChangingTimer);
			
			try {
				Image img = ImageIO.read(getClass().getResource("/images/icons/help_24px_icon.png"));
				ImageIcon icon = new ImageIcon(img);
				JLabel lblChangerTimeHelp = new JLabel(icon);
				lblChangerTimeHelp.setToolTipText(i18nBundle.getString("changer.change.changing.time.help"));
				lblChangerTimeHelp.setBounds(420, 6, 30, 23);
				changerPanel.add(lblChangerTimeHelp);				
			} catch (IOException ex) {
				JLabel lblChangerTimeHelp = new JLabel("0 to disable changer");
				lblChangerTimeHelp.setBounds(366, 6, 30, 23);
				changerPanel.add(lblChangerTimeHelp);
			}

			/**
			 * btnAddDirectory Action Listener.
			 */
			btnAddDirectory.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					// Stopping changer process will be done in PathChangerWindow
					// Starting changer process will be done in PatChangerWindow too
					// btnRemoveButton will be added in PathChangerWindow
					PathChangerWindow pathChangerWindow = new PathChangerWindow(window, WDUtilities.CHANGER_DIRECTORY);
					pathChangerWindow.setVisible(true);
	  				// There is a bug with KDE (version 5.9) and the preview window is not painted properly
	  				// It is necessary to reshape this window in order to paint all its components
	  				if (WDUtilities.getWallpaperChanger() instanceof LinuxWallpaperChanger) {
	  					if (((LinuxWallpaperChanger)WDUtilities.getWallpaperChanger()).getDesktopEnvironment() == WDUtilities.DE_KDE) {
	  						pathChangerWindow.setSize(449, 299);  						
	  					}
	  				}
				}
			});

			/**
			 * btnRemoveDirectory Action Listener.
			 */
			btnRemoveDirectory.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					//Stopping changer process
					changer.stop();

					PreferencesManager prefm = PreferencesManager.getInstance();
					String directoryToRemove = listDirectoriesModel.getElementAt(listDirectoriesToWatch.getSelectedIndex());
					listDirectoriesModel.remove(listDirectoriesToWatch.getSelectedIndex());
					String changerFoldersProperty = prefm.getPreference("application-changer-folder");
					String[] changerFolders = changerFoldersProperty.split(";");
					ArrayList<String> changerFoldersList = new ArrayList<String>(Arrays.asList(changerFolders));
					Iterator<String> iterator = changerFoldersList.iterator();
					while (iterator.hasNext()) {
						String directory = iterator.next();
						if (directory.equals(directoryToRemove)) {
							iterator.remove();
						}
					}
					String modifiedChangerFoldersProperty = "";
					iterator = changerFoldersList.iterator();
					while (iterator.hasNext()) {
						String directory = iterator.next();
						modifiedChangerFoldersProperty = modifiedChangerFoldersProperty + directory;
						if (iterator.hasNext()) {
							modifiedChangerFoldersProperty = modifiedChangerFoldersProperty + ";";
						}
					}
					prefm.setPreference("application-changer-folder", modifiedChangerFoldersProperty);
					
					// Removing btnRemoveDirectory if it is needed
					if (listDirectoriesModel.size() <= 1) {
						changerPanel.remove(btnRemoveDirectory);
						changerPanel.repaint();
					}
					
					// Resetting the last wallpaper set
	  				prefm.setPreference("application-changer-mode-sort-last-wallpaper", PreferencesManager.DEFAULT_VALUE);

	  				//Starting changer process
					changer.start();
				}
			});

			/**
			 * Only for Gnomeish DE, a checkbox will be displayed for expanding the wallpaper through all the screens available
			 */
			if (WDUtilities.isGnomeish()) {
				JSeparator changerSeparator1 = new JSeparator();
				changerSeparator1.setBounds(12, 330, 631, 2);
				changerPanel.add(changerSeparator1);		

				multiMonitorCheckBox = new JCheckBox(i18nBundle.getString("changer.change.multimonitor"));
				multiMonitorCheckBox.setBounds(11, 333, 281, 23);
				changerPanel.add(multiMonitorCheckBox);
				
				/**
				 * multiMonitorCheckBox Action Listener.
				 */
				// Clicking event
				multiMonitorCheckBox.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						PreferencesManager prefm = PreferencesManager.getInstance();
						
						if (multiMonitorCheckBox.isSelected()) {
							prefm.setPreference("application-changer-multimonitor", WDUtilities.APP_YES);
							WDUtilities.changeMultiMonitorModeGnomish("spanned");
						} else {
							prefm.setPreference("application-changer-multimonitor", WDUtilities.APP_NO);
							WDUtilities.changeMultiMonitorModeGnomish("stretched");
						}
					}
				});
				
			}
		}

		// Downloads Directory (tab)
		miscPanel = new JPanel();
		miscPanel.setBorder(null);
		tabbedPane.addTab(i18nBundle.getString("downloads.directory.title"), null, miscPanel, null);
		miscPanel.setLayout(null);
		
		JLabel lblDownloadsDirectory = new JLabel(i18nBundle.getString("downloads.directory.title"));
		lblDownloadsDirectory.setBounds(12, 16, 160, 19);
		miscPanel.add(lblDownloadsDirectory);
		
		downloadsDirectory = new JFormattedTextField((Format) null);
		downloadsDirectory.setEditable(false);
		downloadsDirectory.setColumns(4);
		downloadsDirectory.setBounds(174, 11, 405, 31);
		miscPanel.add(downloadsDirectory);
		
		btnOpenDownloadsDirectory = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/open_folder_24px_icon.png"));
			btnOpenDownloadsDirectory.setIcon(new ImageIcon(img));
			btnOpenDownloadsDirectory.setToolTipText(i18nBundle.getString("downloads.directory.open"));
			btnOpenDownloadsDirectory.setBounds(12, 39, 34, 33);
		} catch (IOException ex) {
			btnOpenDownloadsDirectory.setText("Open");
			btnOpenDownloadsDirectory.setBounds(589, 11, 72, 25);
		}		
		if (WDUtilities.isSnapPackage()) {
			btnOpenDownloadsDirectory.setEnabled(false);
		}
		miscPanel.add(btnOpenDownloadsDirectory);
		
		btnClipboard = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/clipboard_24px_icon.png"));
			btnClipboard.setIcon(new ImageIcon(img));
			btnClipboard.setToolTipText(i18nBundle.getString("downloads.directory.copy.clipboard"));
			btnClipboard.setBounds(54, 39, 34, 33);
		} catch (IOException ex) {
			btnClipboard.setText("Clipboard");
			btnClipboard.setBounds(630, 8, 34, 33);
		}
		miscPanel.add(btnClipboard);
		
		btnChangeDownloadsDirectory = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/change_folder_24px_icon.png"));
			btnChangeDownloadsDirectory.setIcon(new ImageIcon(img));
			btnChangeDownloadsDirectory.setToolTipText(i18nBundle.getString("downloads.directory.change"));
			btnChangeDownloadsDirectory.setBounds(588, 10, 34, 33);
		} catch (IOException ex) {
			btnChangeDownloadsDirectory.setText("Change Downloads Directory");
			btnChangeDownloadsDirectory.setBounds(12, 186, 259, 25);
		}	
		miscPanel.add(btnChangeDownloadsDirectory);
		
		diskSpacePB.setBounds(174, 101, 405, 18);
		miscPanel.add(diskSpacePB);
		
		JLabel lblDiskSpace = new JLabel(i18nBundle.getString("downloads.directory.space"));
		lblDiskSpace.setBounds(12, 100, 160, 19);
		miscPanel.add(lblDiskSpace);
		
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/warning_24px_icon.png"));
			ImageIcon icon = new ImageIcon(img);
			lblSpaceWarning = new JLabel(icon);
			lblSpaceWarning.setToolTipText(i18nBundle.getString("downloads.directory.full"));
			lblSpaceWarning.setBounds(588, 98, 30, 23);
			miscPanel.add(lblSpaceWarning);
			
			JSeparator separator7 = new JSeparator();
			separator7.setBounds(12, 83, 610, 2);
			miscPanel.add(separator7);
			// At first, the label won't be visible
			lblSpaceWarning.setVisible(false);
		} catch (IOException ex) {
			lblSpaceWarning = new JLabel("Directory full. Wallpapers will be removed randomly");
			lblSpaceWarning.setBounds(588, 53, 30, 23);
			miscPanel.add(lblSpaceWarning);
			// At first, the label won't be visible
			lblSpaceWarning.setVisible(false);
		}

		// Wallpapers (tab)
		wallpapersPanel = new JPanel();
		wallpapersPanel.setBorder(null);
		tabbedPane.addTab(i18nBundle.getString("wallpapers.title"), null, wallpapersPanel, null);
		wallpapersPanel.setLayout(null);

		JLabel lblLastWallpapers = new JLabel(i18nBundle.getString("wallpapers.last.five"));
		lblLastWallpapers.setBounds(12, 12, 238, 15);
		wallpapersPanel.add(lblLastWallpapers);
		
        scroll = new JScrollPane();
        scroll.setPreferredSize(new Dimension(300, 400));
		scroll.setBounds(12, 36, 652, 105);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		wallpapersPanel.add(scroll);
		
		btnManageWallpapers = new JButton(i18nBundle.getString("wallpapers.downloaded"));
		btnManageWallpapers.setBackground(Color.WHITE);
		btnManageWallpapers.setBounds(12, 201, 214, 25);
		wallpapersPanel.add(btnManageWallpapers);
		
		btnRemoveWallpaper = new JButton();
		
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/delete_24px_icon.png"));
			btnRemoveWallpaper.setIcon(new ImageIcon(img));
			btnRemoveWallpaper.setToolTipText(i18nBundle.getString("wallpapers.delete"));
			btnRemoveWallpaper.setBounds(12, 149, 34, 33);
		} catch (IOException ex) {
			btnRemoveWallpaper.setText("Delete");
			btnRemoveWallpaper.setBounds(12, 149, 34, 33);
		}
		wallpapersPanel.add(btnRemoveWallpaper);
		
		btnSetFavoriteWallpaper = new JButton();
		
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/favourite_24px_icon.png"));
			btnSetFavoriteWallpaper.setIcon(new ImageIcon(img));
			btnSetFavoriteWallpaper.setToolTipText(i18nBundle.getString("wallpapers.favorite"));
			btnSetFavoriteWallpaper.setBounds(53, 149, 34, 33);
		} catch (IOException ex) {
			btnSetFavoriteWallpaper.setText("Set as favorite");
			btnSetFavoriteWallpaper.setBounds(58, 149, 34, 33);
		}
		wallpapersPanel.add(btnSetFavoriteWallpaper);
		
		btnPreviewWallpaper = new JButton();

		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/view_24px_icon.png"));
			btnPreviewWallpaper.setIcon(new ImageIcon(img));
			btnPreviewWallpaper.setToolTipText(i18nBundle.getString("wallpapers.preview"));
			btnPreviewWallpaper.setBounds(95, 149, 34, 33);
		} catch (IOException ex) {
			btnPreviewWallpaper.setText("Preview wallpaper");
			btnPreviewWallpaper.setBounds(95, 149, 34, 33);
		}
		wallpapersPanel.add(btnPreviewWallpaper);
		
		btnMoveWallpapers = new JButton(i18nBundle.getString("wallpapers.move"));
		btnMoveWallpapers.setBackground(Color.WHITE);
		btnMoveWallpapers.setBounds(12, 234, 214, 25);
		wallpapersPanel.add(btnMoveWallpapers);
		
		btnChooseWallpaper = new JButton(i18nBundle.getString("wallpapers.choose"));
		btnChooseWallpaper.setBackground(Color.WHITE);
		btnChooseWallpaper.setBounds(12, 268, 214, 25);
		wallpapersPanel.add(btnChooseWallpaper);
		
        if (WDUtilities.getWallpaperChanger().isWallpaperChangeable()) {
    		btnRandomWallpaper = new JButton(i18nBundle.getString("wallpapers.random"));
    		btnRandomWallpaper.setBackground(Color.WHITE);
    		btnRandomWallpaper.setBounds(12, 303, 214, 25);
    		wallpapersPanel.add(btnRandomWallpaper);
    		
            // Set next wallpaper if changer mode is not randomly
    		btnNextWallpaper = new JButton(i18nBundle.getString("wallpapers.next"));
    		btnNextWallpaper.setBackground(Color.WHITE);
    		btnNextWallpaper.setBounds(12, 337, 214, 25);
    		wallpapersPanel.add(btnNextWallpaper);
        }

        btnSetWallpaper = new JButton();
		try {
			Image img = ImageIO.read(getClass().getResource("/images/icons/desktop_24px_icon.png"));
			btnSetWallpaper.setIcon(new ImageIcon(img));
			btnSetWallpaper.setToolTipText(i18nBundle.getString("wallpapers.set"));
			btnSetWallpaper.setBounds(137, 149, 34, 33);
		} catch (IOException ex) {
			btnSetWallpaper.setText("Set wallpaper");
			btnSetWallpaper.setBounds(99, 149, 34, 33);
		}
		// This button only will be available for those desktops which support setting wallpapers directly
		if (WDUtilities.getWallpaperChanger().isWallpaperChangeable()) {
			wallpapersPanel.add(btnSetWallpaper);			

		}

		// About (tab)
		aboutPanel = new JPanel();
		aboutPanel.setBorder(null);
		tabbedPane.addTab(i18nBundle.getString("about.title"), null, aboutPanel, null);
		aboutPanel.setLayout(null);
		
		JLabel lblVersion = new JLabel(i18nBundle.getString("about.version"));
		lblVersion.setBounds(12, 16, 70, 15);
		aboutPanel.add(lblVersion);
		
		version = new JTextField() {
			// JTextField without border
			@Override public void setBorder(Border border) {
				// No borders!
			}
		};
		version.setHorizontalAlignment(SwingConstants.CENTER);
		version.setEditable(false);
		version.setBounds(73, 14, 47, 19);
		version.setColumns(10);
		version.setOpaque(false);
		version.setBackground(new Color(0, 0, 0, 0));
		aboutPanel.add(version);
		
		aboutSeparator1 = new JSeparator();
		aboutSeparator1.setBounds(11, 43, 610, 2);
		aboutPanel.add(aboutSeparator1);
		
		lblDeveloper = new JLabel(i18nBundle.getString("about.developer"));
		lblDeveloper.setBounds(12, 51, 95, 15);
		aboutPanel.add(lblDeveloper);
		
		developer = new JTextField() {
			// JTextField without border
			@Override public void setBorder(Border border) {
				// No borders!
			}
		};
		developer.setEditable(false);
		developer.setBounds(145, 50, 405, 19);
		developer.setColumns(10);
		developer.setOpaque(false);
		developer.setBackground(new Color(0, 0, 0, 0));
		aboutPanel.add(developer);
		
		lblSourceCode = new JLabel(i18nBundle.getString("about.source.code"));
		lblSourceCode.setBounds(12, 346, 108, 15);
		aboutPanel.add(lblSourceCode);
		
		btnRepository = new JButton("New button");
		btnRepository.setBounds(134, 342, 483, 25);
		btnRepository.setText("<HTML><FONT color=\"#000099\"><U>" + pm.getProperty("repository.code") + "</U></FONT></HTML>");
		btnRepository.setHorizontalAlignment(SwingConstants.LEFT);
		btnRepository.setOpaque(false);
		btnRepository.setContentAreaFilled(false);
		btnRepository.setBorderPainted(false);
		aboutPanel.add(btnRepository);
		
		aboutSeparator2 = new JSeparator();
		aboutSeparator2.setBounds(12, 120, 610, 7);
		aboutPanel.add(aboutSeparator2);
		
		JLabel lblIcons = new JLabel(i18nBundle.getString("about.icons"));
		lblIcons.setBounds(12, 75, 95, 15);
		aboutPanel.add(lblIcons);
		
		icons = new JTextField() {
			public void setBorder(Border border) {
			}
		};
		icons.setHorizontalAlignment(SwingConstants.LEFT);
		icons.setText("Jaime Álvarez; Dave Gandy");
		icons.setEditable(false);
		icons.setColumns(10);
		icons.setBounds(145, 73, 219, 19);
		icons.setOpaque(false);
		icons.setBackground(new Color(0, 0, 0, 0));
		aboutPanel.add(icons);
		
		btnIcons = new JButton("<HTML><FONT color=\"#000099\"><U>http://www.flaticon.com/</U></FONT></HTML>");
		btnIcons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnIcons.setHorizontalAlignment(SwingConstants.LEFT);
		btnIcons.setOpaque(false);
		btnIcons.setContentAreaFilled(false);
		btnIcons.setBorderPainted(false);
		btnIcons.setBounds(381, 70, 229, 25);
		aboutPanel.add(btnIcons);
		
		JLabel lblContributors = new JLabel(i18nBundle.getString("about.contributors"));
		lblContributors.setBounds(11, 97, 95, 15);
		aboutPanel.add(lblContributors);
		
		JLabel lblChangelog = new JLabel(i18nBundle.getString("about.changelog"));
		lblChangelog.setBounds(12, 127, 91, 20);
		aboutPanel.add(lblChangelog);
		
		JScrollPane changelogScrollPane = new JScrollPane();
		changelogScrollPane.setBounds(11, 154, 610, 183);
		aboutPanel.add(changelogScrollPane);
		
		JTextPane changelogTextPane = new JTextPane();
		changelogScrollPane.setViewportView(changelogTextPane);
		
		StyledDocument doc = changelogTextPane.getStyledDocument();

		//  Define a keyword attribute
		SimpleAttributeSet keyWord = new SimpleAttributeSet();
		StyleConstants.setForeground(keyWord, Color.BLUE);
		StyleConstants.setBold(keyWord, true);

		// Changelog
		try
		{
			// Version 4.4.2
			doc.insertString(0, i18nBundle.getString("about.changelog.bugs.4.4.2.title"), keyWord );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.4.4.2.text"), null );

			// Version 4.4.1
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.4.4.1.title"), keyWord );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.4.4.1.text"), null );

			// Version 4.4.0
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.4.4.0.title"), keyWord );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.4.4.0.text"), null );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.4.4.0.title"), keyWord );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.4.4.0.text"), null );

			// Version 4.3
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.4.3.title"), keyWord );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.4.3.text"), null );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.4.3.title"), keyWord );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.4.3.text"), null );

			// Version 4.2
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.4.2.title"), keyWord );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.4.2.text"), null );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.4.2.title"), keyWord );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.4.2.text"), null );

			// Version 4.1
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.4.1.title"), keyWord );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.4.1.text"), null );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.4.1.title"), keyWord );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.4.1.text"), null );

			// Version 4.0
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.4.0.title"), keyWord );
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.4.0.text"), null );

			// Version 3.9
			doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.9.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.9.text"), null );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.9.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.9.text"), null );

		    // Version 3.8
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.8.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.8.text"), null );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.8.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.8.text"), null );

		    // Version 3.7
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.7.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.7.text"), null );

		    // Version 3.6
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.6.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.6.text"), null );

			// Version 3.5
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.5.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.5.text"), null );

			// Version 3.4
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.4.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.4.text"), null );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.4.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.4.text"), null );

		    // Version 3.3
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.3.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.3.text"), null );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.3.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.3.text"), null );

		    // Version 3.2
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.2.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.2.text"), null );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.2.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.2.text"), null );

			// Version 3.1
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.1.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.1.text"), null );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.1.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.1.text"), null );

		    // Version 3.0
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.0.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.3.0.text"), null );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.0.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.3.0.text"), null );
			
			// Version 2.9
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.2.9.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.2.9.text"), null );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.2.9.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.2.9.text"), null );
			
			// Version 2.8
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.2.8.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.2.8.text"), null );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.2.8.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.2.8.text"), null );

		    // Version 2.7
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.2.7.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.features.2.7.text"), null );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.2.7.title"), keyWord );
		    doc.insertString(doc.getLength(), i18nBundle.getString("about.changelog.bugs.2.7.text"), null );
		}
		catch(Exception exception) { 
			if (LOG.isInfoEnabled()) {
				LOG.error("Error rendering jTextPane. Error: " + exception.getMessage());
			}
		}
		// Text to the beginning
		changelogTextPane.setCaretPosition(0);
		
		contributors = new JTextField() {
			public void setBorder(Border border) {
			}
		};
		contributors.setText("Lee Burch; Guilherme Silva");
		contributors.setOpaque(false);
		contributors.setHorizontalAlignment(SwingConstants.LEFT);
		contributors.setEditable(false);
		contributors.setColumns(10);
		contributors.setBackground(new Color(0, 0, 0, 0));
		contributors.setBounds(145, 95, 405, 19);
		aboutPanel.add(contributors);

		// Help (tab)
		helpPanel = new JPanel();
		helpPanel.setBorder(null);
		tabbedPane.addTab(i18nBundle.getString("help.title"), null, helpPanel, null);
		helpPanel.setLayout(null);
		
		JScrollPane helpScrollPane = new JScrollPane();
		helpScrollPane.setBounds(12, 12, 657, 359);
		helpPanel.add(helpScrollPane);
		
		JTextPane helpTextPane = new JTextPane();
		helpTextPane.setCaretPosition(0);

		// Set content as HTML
		helpTextPane.setContentType("text/html");
		helpTextPane.setText(i18nBundle.getString("help.tips"));
		helpTextPane.setEditable(false);//so it's not editable
		helpTextPane.setOpaque(false);//so we don't see white background

		helpTextPane.addHyperlinkListener(new HyperlinkListener() {
            public void hyperlinkUpdate(HyperlinkEvent hle) {
                if (HyperlinkEvent.EventType.ACTIVATED.equals(hle.getEventType())) {
                	try {
						WDUtilities.openLinkOnBrowser(hle.getURL().toURI().toString());
					} catch (URISyntaxException exception) {
						if (LOG.isInfoEnabled()) {
							LOG.error("Error opening a link. Message: " + exception.getMessage());
						}
					}
                }
            }
        });

		// Text to the beginning
		helpTextPane.setCaretPosition(0);

		helpScrollPane.setViewportView(helpTextPane);
		
		// Minimize button will only be available on OS with an
		// old system tray
		btnMinimize = new JButton(i18nBundle.getString("global.minimize"));
		btnMinimize.setBackground(Color.WHITE);
		GridBagConstraints gbc_btnMinimize = new GridBagConstraints();
		gbc_btnMinimize.insets = new Insets(0, 0, 0, 5);
		gbc_btnMinimize.anchor = GridBagConstraints.NORTH;
		gbc_btnMinimize.gridx = 0;
		gbc_btnMinimize.gridy = 3;
		if (isOldSystemTray()) {
			frame.getContentPane().add(btnMinimize, gbc_btnMinimize);
		}
		
		// Setting up configuration
		initializeGUI();
		
		// Setting up listeners
		initializeListeners();
				
		// Starting automated changer process
		initializeChanger();

		// Starting harvesting process
		initializeHarvesting();
		
		// Starting pause / resume feature
		pauseResumeRepaint();
	}

	/**
	 * This method configures all the listeners.
	 */
	private void initializeListeners() {
		
		final PreferencesManager prefm = PreferencesManager.getInstance();
		
		// Listeners
		/**
		 * wallhavenCheckbox Action Listener.
		 */
		// Clicking event
		wallhavenCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (wallhavenCheckbox.isSelected()) {
					searchTypeWallhavenComboBox.setEnabled(true);
					prefm.setPreference("provider-wallhaven", WDUtilities.APP_YES);
				} else {
					searchTypeWallhavenComboBox.setEnabled(false);
					prefm.setPreference("provider-wallhaven", WDUtilities.APP_NO);
				}

				// Restarting harvesting process if it is needed
				restartDownloadingProcess();
			}
		});

		/**
		 * searchTypeWallhavenComboBox Action Listener.
		 */
		// Clicking event
		searchTypeWallhavenComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				prefm.setPreference("wallpaper-search-type", new Integer(searchTypeWallhavenComboBox.getSelectedIndex()).toString());
				// Restarting downloading process
				restartDownloadingProcess();
			}
		});

		/**
		 * deviantArtCheckbox Action Listener.
		 */
		// Clicking event
		deviantArtCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (deviantArtCheckbox.isSelected()) {
					deviantArtSearchTypeComboBox.setEnabled(true);
					prefm.setPreference("provider-devianart", WDUtilities.APP_YES);
				} else {
					deviantArtSearchTypeComboBox.setEnabled(false);
					prefm.setPreference("provider-devianart", WDUtilities.APP_NO);
				}

				// Restarting harvesting process if it is needed
				restartDownloadingProcess();
			}
		});

		/**
		 * deviantArtSearchTypeComboBox Action Listener.
		 */
		// Clicking event
		deviantArtSearchTypeComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				prefm.setPreference("wallpaper-devianart-search-type", new Integer(deviantArtSearchTypeComboBox.getSelectedIndex()).toString());
				// Restarting downloading process
				restartDownloadingProcess();
			}
		});

		/**
		 * bingCheckbox Action Listener.
		 */
		// Clicking event
		bingCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (bingCheckbox.isSelected()) {
					prefm.setPreference("provider-bing", WDUtilities.APP_YES);
				} else {
					prefm.setPreference("provider-bing", WDUtilities.APP_NO);
				}

				// Restarting harvesting process if it is needed
				restartDownloadingProcess();
			}
		});

		/**
		 * socialWallpaperingCheckbox Action Listener.
		 */
		// Clicking event
		socialWallpaperingCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (socialWallpaperingCheckbox.isSelected()) {
					prefm.setPreference("provider-socialWallpapering", WDUtilities.APP_YES);
					socialWallpaperingIgnoreKeywordsCheckbox.setEnabled(true);
				} else {
					prefm.setPreference("provider-socialWallpapering", WDUtilities.APP_NO);
					socialWallpaperingIgnoreKeywordsCheckbox.setEnabled(false);
				}

				// Restarting harvesting process if it is needed
				restartDownloadingProcess();
			}
		});

		/**
		 * socialWallpaperingIgnoreKeywordsCheckbox Action Listener.
		 */
		// Clicking event
		socialWallpaperingIgnoreKeywordsCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (socialWallpaperingIgnoreKeywordsCheckbox.isSelected()) {
					prefm.setPreference("provider-socialWallpapering-ignore-keywords", WDUtilities.APP_YES);
				} else {
					prefm.setPreference("provider-socialWallpapering-ignore-keywords", WDUtilities.APP_NO);
				}

				// Restarting harvesting process if it is needed
				restartDownloadingProcess();
			}
		});

		/**
		 * wallpaperFusionCheckbox Action Listener.
		 */
		// Clicking event
		wallpaperFusionCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (wallpaperFusionCheckbox.isSelected()) {
					prefm.setPreference("provider-wallpaperFusion", WDUtilities.APP_YES);
				} else {
					prefm.setPreference("provider-wallpaperFusion", WDUtilities.APP_NO);
				}

				// Restarting harvesting process if it is needed
				restartDownloadingProcess();
			}
		});
		
		/**
		 * dualMonitorCheckbox Action Listener.
		 */
		// Clicking event
		dualMonitorCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (dualMonitorCheckbox.isSelected()) {
					prefm.setPreference("provider-dualMonitorBackgrounds", WDUtilities.APP_YES);
					searchTypeDualMonitorComboBox.setEnabled(true);
				} else {
					prefm.setPreference("provider-dualMonitorBackgrounds", WDUtilities.APP_NO);
					searchTypeDualMonitorComboBox.setEnabled(false);
				}
				
				// Restarting harvesting process if it is needed
				restartDownloadingProcess();
			}
		});

		/**
		 * searchTypeDualMonitorComboBox Action Listener.
		 */
		// Clicking event
		searchTypeDualMonitorComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				prefm.setPreference("provider-dualMonitorBackgrounds-search-type", new Integer(searchTypeDualMonitorComboBox.getSelectedIndex()).toString());
				// Restarting downloading process
				restartDownloadingProcess();
			}
		});

		/**
		 * unsplashCheckbox Action Listener.
		 */
		// Clicking event
		unsplashCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (unsplashCheckbox.isSelected()) {
					// Checks Unsplash restrictions:
					// Harvesting process can download a new wallpaper from Unsplash
					// every hour at least

					if (!UnsplashProvider.configurationComplyUnsplashPolicy()) {
						prefm.setPreference("provider-unsplash", WDUtilities.APP_NO);
						unsplashCheckbox.setSelected(false);
						searchTypeUnsplashComboBox.setEnabled(false);
						
						// User has to be informed and background harvesting process should be increased
						DialogManager info = new DialogManager(i18nBundle.getString("messages.unsplash.policy"), 8000);
						info.openDialog();						
					} else {
						prefm.setPreference("provider-unsplash", WDUtilities.APP_YES);
						searchTypeUnsplashComboBox.setEnabled(true);

						// Restarting harvesting process if it is needed
						restartDownloadingProcess();
					}
				} else {
					prefm.setPreference("provider-unsplash", WDUtilities.APP_NO);
					searchTypeUnsplashComboBox.setEnabled(false);
					
					// Restarting harvesting process if it is needed
					restartDownloadingProcess();
				}
			}
		});

		/**
		 * searchTypeUnsplashComboBox Action Listener.
		 */
		// Clicking event
		searchTypeUnsplashComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				prefm.setPreference("provider-unsplash-search-type", new Integer(searchTypeUnsplashComboBox.getSelectedIndex()).toString());
				// Restarting downloading process
				restartDownloadingProcess();
			}
		});

		/**
		 * btnMinimize Action Listener.
		 */
		// Clicking event
		btnMinimize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				minimizeApplication();
			}
		
		});
		
		/**
		 * btnChangeResolution Action Listener.
		 */
		// Clicking event
		btnChangeResolution.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				widthResolution.setEnabled(true);
				heightResolution.setEnabled(true);
				providersPanel.remove(btnChangeResolution);
				providersPanel.add(btnApplyResolution);
				providersPanel.add(btnResetResolution);
				providersPanel.repaint();
			}
		});

		/**
		 * btnApplyResolution Action Listener.
		 */
		// Clicking event
		btnApplyResolution.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				String width = widthResolution.getValue().toString();
				String height = heightResolution.getValue().toString();

				// Sanitizing width and height
				width = width.replace(".", "");
				width = width.replace(",", "");
				height = height.replace(".", "");
				height = height.replace(",", "");

				if (!width.isEmpty() && !height.isEmpty()) {
					prefm.setPreference("wallpaper-resolution", width + "x" + height);
				}
				
				widthResolution.setEnabled(false);
				heightResolution.setEnabled(false);
				providersPanel.remove(btnApplyResolution);
				providersPanel.remove(btnResetResolution);
				providersPanel.add(btnChangeResolution);
				providersPanel.repaint();
				// Restarting downloading process if it is needed
				if (areProvidersChecked()) {
					restartDownloadingProcess();
				}
			}
		});

		/**
		 * btnResetResolution Action Listener.
		 */
		// Clicking event
		btnResetResolution.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				String screenResolution = WDUtilities.getResolution();
				String[] resolution = screenResolution.split("x");
		        widthResolution.setValue(new Integer(resolution[0]));
				heightResolution.setValue(new Integer(resolution[1]));
			}
		});
		
		/**
		 * downloadPolicyComboBox Action Listener.
		 */
		// Clicking event
		downloadPolicyComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				prefm.setPreference("download-policy", new Integer(downloadPolicyComboBox.getSelectedIndex()).toString());
				// Restarting downloading process if it is needed
				if (areProvidersChecked()) {
					restartDownloadingProcess();
				}
			}
		});

		/**
		 * btnChangeKeywords Action Listener.
		 */
		// Clicking event
		btnChangeKeywords.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				searchKeywords.setEnabled(true);
				providersPanel.remove(btnChangeKeywords);
				providersPanel.add(btnApplyKeywords);
				providersPanel.repaint();
			}
		});

		/**
		 * btnApplyKeywords Action Listener.
		 */
		// Clicking event
		btnApplyKeywords.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (!searchKeywords.getText().isEmpty()) {
					prefm.setPreference("provider-wallhaven-keywords", searchKeywords.getText());					
				} else {
					prefm.setPreference("provider-wallhaven-keywords", PreferencesManager.DEFAULT_VALUE);
				}
				
				searchKeywords.setEnabled(false);
				providersPanel.remove(btnApplyKeywords);
				providersPanel.add(btnChangeKeywords);
				providersPanel.repaint();
				// Restarting downloading process if it is needed
				if (areProvidersChecked()) {
					restartDownloadingProcess();
				}
			}
		});

		/**
		 * btnChangeForbiddenKeywords Action Listener.
		 */
		// Clicking event
		btnChangeForbiddenKeywords.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				forbiddenKeywords.setEnabled(true);
				providersPanel.remove(btnChangeForbiddenKeywords);
				providersPanel.add(btnApplyForbiddenKeywords);
				providersPanel.repaint();
			}
		});

		/**
		 * btnApplyForbiddenKeywords Action Listener.
		 */
		// Clicking event
		btnApplyForbiddenKeywords.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (!forbiddenKeywords.getText().isEmpty()) {
					prefm.setPreference("avoid-keywords", forbiddenKeywords.getText());					
				} else {
					prefm.setPreference("avoid-keywords", PreferencesManager.DEFAULT_VALUE);
				}
				
				forbiddenKeywords.setEnabled(false);
				providersPanel.remove(btnApplyForbiddenKeywords);
				providersPanel.add(btnChangeForbiddenKeywords);
				providersPanel.repaint();
				// Restarting downloading process if it is needed
				if (areProvidersChecked()) {
					restartDownloadingProcess();
				}
			}
		});

		/**
		 * btnChangeSize Action Listener.
		 */
		// Clicking event
		btnChangeSize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				downloadDirectorySize.setEnabled(true);
				appSettingsPanel.remove(btnChangeSize);
				appSettingsPanel.add(btnApplySize);
				appSettingsPanel.repaint();
			}
		});

		/**
		 * btnApplySize Action Listener.
		 */
		// Clicking event
		btnApplySize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				prefm.setPreference("application-max-download-folder-size", downloadDirectorySize.getValue().toString());
				downloadDirectorySize.setEnabled(false);
				appSettingsPanel.remove(btnApplySize);
				appSettingsPanel.add(btnChangeSize);
				appSettingsPanel.repaint();

				// Refreshing Disk Space Progress Bar
				refreshProgressBar();
				
				// Restarting downloading process if it is needed
				if (areProvidersChecked()) {
					restartDownloadingProcess();
				}
			}
		});

		/**
		 * btnChangeTimer Action Listener.
		 */
		// Clicking event
		btnChangeTimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				downloadingTime.setEnabled(true);
				timeComboBox1.setEnabled(true);
				appSettingsPanel.remove(btnChangeTimer);
				appSettingsPanel.add(btnApplyTimer);
				appSettingsPanel.repaint();
			}
		});

		/**
		 * btnApplyTimer Action Listener.
		 */
		// Clicking event
		btnApplyTimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (!downloadingTime.getText().isEmpty()) {
					Integer timeInterval = Integer.parseInt(downloadingTime.getText().replaceAll(",", ""));
					prefm.setPreference("application-timer", timeInterval.toString());
					prefm.setPreference("application-timer-units", timeComboBox1.getSelectedItem().toString());
					
					// Extra checking Unsplash download policy
					if (unsplashCheckbox.isSelected()) {
						if (!UnsplashProvider.configurationComplyUnsplashPolicy()) {
							// User has to be informed and downloading time will be automatically set to 1 hour
							prefm.setPreference("application-timer", "1");
							prefm.setPreference("application-timer-units", "h");
							downloadingTime.setText(prefm.getPreference("application-timer"));
							timeComboBox1.getModel().setSelectedItem(new ComboItem(prefm.getPreference("application-timer-units"), prefm.getPreference("application-timer-units")));

							DialogManager info = new DialogManager(i18nBundle.getString("messages.unsplash.policy.set.downloading.time"), 8000);
							info.openDialog();						

						}
					}
				} else {
					prefm.setPreference("application-timer", "5");
					prefm.setPreference("application-timer-units", "m");
				}
				
				downloadingTime.setEnabled(false);
				timeComboBox1.setEnabled(false);
				appSettingsPanel.remove(btnApplyTimer);
				appSettingsPanel.add(btnChangeTimer);
				appSettingsPanel.repaint();
				// Restarting downloading process if it is needed
				if (areProvidersChecked()) {
					restartDownloadingProcess();
				}
			}
		});

		if (WDUtilities.getWallpaperChanger().isWallpaperChangeable()) {
			/**
			 * btnChangeChangingTimer Action Listener.
			 */
			// Clicking event
			btnChangeChangingTimer.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					changingTime.setEnabled(true);
					timeComboBox2.setEnabled(true);
					changerPanel.remove(btnChangeChangingTimer);
					changerPanel.add(btnApplyChangingTimer);
					changerPanel.repaint();
				}
			});

			/**
			 * btnApplyChangingTimer Action Listener.
			 */
			// Clicking event
			btnApplyChangingTimer.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					if (!changingTime.getText().isEmpty()) {
						Integer timeInterval = Integer.parseInt(changingTime.getText().replaceAll(",", ""));
						prefm.setPreference("application-changer", timeInterval.toString());
						prefm.setPreference("application-changer-units", timeComboBox2.getSelectedItem().toString());
					} else {
						prefm.setPreference("application-changer", "5");
						prefm.setPreference("application-changer-units", "m");
					}
				
					changingTime.setEnabled(false);
					timeComboBox2.setEnabled(false);
					changerPanel.remove(btnApplyChangingTimer);
					changerPanel.add(btnChangeChangingTimer);
					changerPanel.repaint();

					//Stopping and starting changer process
					changer.stop();
					changer.start();
				}
			});

			/**
			 * modeComboBox Action Listener.
			 */
			// Clicking event
			modeComboBox.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					Integer changerMode = new Integer(modeComboBox.getSelectedIndex());
					if (changerMode == 0) {
						btnRestartWallpapers.setVisible(false);
						btnNextWallpaper.setVisible(false);
					} else {
						btnRestartWallpapers.setVisible(true);
						btnNextWallpaper.setVisible(true);
					}
					prefm.setPreference("application-changer-mode", changerMode.toString());

					// Resetting the last wallpaper set
					prefm.setPreference("application-changer-mode-sort-last-wallpaper", PreferencesManager.DEFAULT_VALUE);

					//Stopping and starting changer process
					changer.stop();
					changer.start();
				}
			});

			/**
			 * btnRestartWallpapers Action Listener.
			 */
			// Clicking event
			btnRestartWallpapers.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					// Resetting the last wallpaper set
					prefm.setPreference("application-changer-mode-sort-last-wallpaper", PreferencesManager.DEFAULT_VALUE);

					//Stopping and starting changer process
					changer.stop();
					changer.start();
				}
			});
		}
		
		/**
		 * startMinimizedCheckBox Action Listener.
		 */
		// Clicking event
		startMinimizedCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (startMinimizedCheckBox.isSelected()) {
					prefm.setPreference("start-minimized", WDUtilities.APP_YES);
					timeToMinimizeComboBox.setEnabled(true);
				} else {
					prefm.setPreference("start-minimized", WDUtilities.APP_NO);
					timeToMinimizeComboBox.setEnabled(false);
				}
				prefm.setPreference("download-policy", new Integer(downloadPolicyComboBox.getSelectedIndex()).toString());
			}
		});

		/**
		 * timeToMinimizeComboBox Action Listener.
		 */
		// Clicking event
		timeToMinimizeComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				Integer timeToMinimize = new Integer(timeToMinimizeComboBox.getSelectedIndex()) + 1;
				prefm.setPreference("time-to-minimize", timeToMinimize.toString());
			}
		});

		if (SystemTray.isSupported() && 
				!WDUtilities.getOperatingSystem().equals(WDUtilities.OS_MACOSX) && 
				WDUtilities.getArchitecture().equals(WDUtilities.AMD_ARCHITECTURE)) {
			/**
			 * stIconCheckBox Action Listener.
			 */
			// Clicking event
			stIconCheckBox.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					if (stIconCheckBox.isSelected()) {
						prefm.setPreference("system-tray-icon", WDUtilities.APP_YES);
					} else {
						prefm.setPreference("system-tray-icon", WDUtilities.APP_NO);
					}
				}
			});
		}

		/**
		 * moveFavoriteCheckBox Action Listener.
		 */
		// Clicking event
		moveFavoriteCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (moveFavoriteCheckBox.isSelected()) {
					prefm.setPreference("move-favorite", WDUtilities.APP_YES);
					prefm.setPreference("move-favorite-folder", moveDirectory.getText());
				} else {
					prefm.setPreference("move-favorite", WDUtilities.APP_NO);
					prefm.setPreference("move-favorite-folder", PreferencesManager.DEFAULT_VALUE);
				}
			}
		});

		/**
		 * notificationsComboBox Action Listener.
		 */
		// Clicking event
		notificationsComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				prefm.setPreference("application-notifications", new Integer(notificationsComboBox.getSelectedIndex()).toString());
			}
		});

		/**
		 * i18nComboBox Action Listener.
		 */
		// Clicking event
		i18nComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				prefm.setPreference("application-i18n", new Integer(i18nComboBox.getSelectedIndex()).toString());
				pauseDownloadingProcess();
				frmWallpaperdownloaderV.dispose();
				frmWallpaperdownloaderV.setVisible(false);
				window = null;
				window = new WallpaperDownloader();
			}
		});

		/**
		 * btnResetSettings Action Listener.
		 */
		// Clicking event
		btnResetSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				// Removing search keywords
				searchKeywords.setText("");
				prefm.setPreference("provider-wallhaven-keywords", PreferencesManager.DEFAULT_VALUE);

				// Removing keywords to avoid
				forbiddenKeywords.setText("");
				prefm.setPreference("avoid-keywords", PreferencesManager.DEFAULT_VALUE);

				// Removing config.txt 
	    		File userConfigFile = new File(WDUtilities.getUserConfigurationFilePath());
	    		try {
					FileUtils.forceDelete(userConfigFile);
		    		LOG.info("Resetting configuration file. Removing : " + userConfigFile);
				} catch (IOException exception) {
					LOG.error("Configuration file couldn't be removed. Error: " + exception.getMessage());
				}
	    		
				// Reconfiguring the application
				WDConfigManager.checkConfig();
				
				// Restarts the application
				pauseDownloadingProcess();
				frmWallpaperdownloaderV.dispose();
				frmWallpaperdownloaderV.setVisible(false);
				window = null;
				window = new WallpaperDownloader();
			}
		});

		/**
		 * btnOpenDownloadsDirectory Action Listener.
		 */
		// Clicking event
		btnOpenDownloadsDirectory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				// It Opens the downloads directory using the default file manager
        		File downloadsDirectory = new File(WDUtilities.getDownloadsPath());
        		Desktop desktop = Desktop.getDesktop();
        		try {
					desktop.open(downloadsDirectory);
				} catch (IOException e) {
					// There was some error trying to open the downloads Directory
					LOG.error("Error trying to open the Downloads directory. Error: " + e.getMessage());
				}
			}
		});

		/**
		 * btnClipboard Action Listener.
		 */
		// Clicking event
		btnClipboard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				// Copy downloads directory path into the clipboard
				Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				StringSelection data = new StringSelection(downloadsDirectory.getText());
				clipboard.setContents(data, data);
				// Information
				if (WDUtilities.getLevelOfNotifications() > 1) {
					DialogManager info = new DialogManager(i18nBundle.getString("messages.downloaded.path.copied"), 2000);
					info.openDialog();
				}
			}
		});

		/**
		 * btnChangeDownloadsDirectory Action Listener.
		 */
		btnChangeDownloadsDirectory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PathChangerWindow pathChangerWindow = new PathChangerWindow(window, WDUtilities.DOWNLOADS_DIRECTORY);
				pathChangerWindow.setVisible(true);
  				// There is a bug with KDE (version 5.9) and the preview window is not painted properly
  				// It is necessary to reshape this window in order to paint all its components
  				if (WDUtilities.getWallpaperChanger() instanceof LinuxWallpaperChanger) {
  					if (((LinuxWallpaperChanger)WDUtilities.getWallpaperChanger()).getDesktopEnvironment() == WDUtilities.DE_KDE) {
  						pathChangerWindow.setSize(449, 299);  						
  					}
  				}
			}
		});
		
		/**
		 * btnManageWallpapers Action Listener.
		 */
		btnManageWallpapers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				WallpaperManagerWindow wmw = new WallpaperManagerWindow();
				wmw.setVisible(true);
			}
		});
		
		  /**
		  * lastWallpapersList Mouse Motion Listener.
		  */
		  changePointerJList();
	      
		 /**
		  * btnRemoveWallpaper Action Listener.
		  */
	      btnRemoveWallpaper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Get the selected wallpapers
				List<ImageIcon> wallpapersSelected = lastWallpapersList.getSelectedValuesList();
				if (!wallpapersSelected.isEmpty() && wallpapersSelected.get(0) != null) {
					List<String> wallpapersSelectedAbsolutePath = new ArrayList<String>();
					Iterator<ImageIcon> wallpapersSelectedIterator = wallpapersSelected.iterator();
					while (wallpapersSelectedIterator.hasNext()) {
						ImageIcon wallpaperSelectedIcon = (ImageIcon) wallpapersSelectedIterator.next();
						if (wallpaperSelectedIcon != null) {
							wallpapersSelectedAbsolutePath.add(wallpaperSelectedIcon.getDescription());
						}
					}
					WDUtilities.removeWallpaper(wallpapersSelectedAbsolutePath, Boolean.TRUE);
				}
			}
	      });
	      
		 /**
		  * btnSetFavoriteWallpaper Action Listener.
		  */
	      btnSetFavoriteWallpaper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Get the selected wallpapers
				List<ImageIcon> wallpapersSelected = lastWallpapersList.getSelectedValuesList();
				if (!wallpapersSelected.isEmpty() && wallpapersSelected.get(0) != null) {
					List<String> wallpapersSelectedAbsolutePath = new ArrayList<String>();
					Iterator<ImageIcon> wallpapersSelectedIterator = wallpapersSelected.iterator();
					while (wallpapersSelectedIterator.hasNext()) {
						ImageIcon wallpaperSelectedIcon = (ImageIcon) wallpapersSelectedIterator.next();
						if (wallpaperSelectedIcon != null) {
							wallpapersSelectedAbsolutePath.add(wallpaperSelectedIcon.getDescription());
						}
					}
					WDUtilities.setFavorite(wallpapersSelectedAbsolutePath);
				}
			}
		  });

		 /**
		  * btnSetWallpaper Action Listener.
		  */
	      btnSetWallpaper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Get the selected wallpapers
				List<ImageIcon> wallpapersSelected = lastWallpapersList.getSelectedValuesList();
				if (!wallpapersSelected.isEmpty() && wallpapersSelected.get(0) != null) {
					List<String> wallpapersSelectedAbsolutePath = new ArrayList<String>();
					Iterator<ImageIcon> wallpapersSelectedIterator = wallpapersSelected.iterator();
					while (wallpapersSelectedIterator.hasNext()) {
						ImageIcon wallpaperSelectedIcon = (ImageIcon) wallpapersSelectedIterator.next();
						if (wallpaperSelectedIcon != null) {
							wallpapersSelectedAbsolutePath.add(wallpaperSelectedIcon.getDescription());
						}
					}
					WDUtilities.getWallpaperChanger().setWallpaper(wallpapersSelectedAbsolutePath.get(0));
				}
			}
		  });	      

	      /**
		  * btnRepository Action Listener.
		  */
	      btnRepository.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					WDUtilities.openLinkOnBrowser(pm.getProperty("repository.code"));
				}
	      });

		 /**
		  * btnIcons Action Listener.
		  */
	      btnIcons.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					WDUtilities.openLinkOnBrowser(pm.getProperty("repository.icons"));
				}
	      });
		      
	      /**
	       * btnPreviewWallpaper Action Listener.
	       */
	      btnPreviewWallpaper.addActionListener(new ActionListener() {
	    	  public void actionPerformed(ActionEvent arg0) {
  				// Get the selected wallpaper
  				ImageIcon wallpaperSelected = lastWallpapersList.getSelectedValue();
  				if (wallpaperSelected != null) {
  	  				String wallpaperSelectedAbsolutePath = wallpaperSelected.getDescription();
  	  				// Opens the preview window
  	  				PreviewWallpaperWindow previewWindow = new PreviewWallpaperWindow(wallpaperSelectedAbsolutePath, null);
  	  				previewWindow.setVisible(true);
  	  				// There is a bug with KDE (version 5.9) and the preview window is not painted properly
  	  				// It is necessary to reshape this window in order to paint all its components
  	  				if (WDUtilities.getWallpaperChanger() instanceof LinuxWallpaperChanger) {
  	  					if (((LinuxWallpaperChanger)WDUtilities.getWallpaperChanger()).getDesktopEnvironment() == WDUtilities.DE_KDE) {
  	  						previewWindow.setSize(1023, 767);  						
  	  					}
  	  				}
  				}
 	    	  }
	      });

	      /**
	       * moveFavoriteCheckBox Action Listener.
	       */
	      // Clicking event
	      moveFavoriteCheckBox.addActionListener(new ActionListener() {
	    	  public void actionPerformed(ActionEvent evt) {
				if (moveFavoriteCheckBox.isSelected()) {
					moveFavoriteCheckBox.setSelected(true);
					moveDirectory.setEnabled(true);
					moveDirectory.setText(WDUtilities.getDownloadsPath());
					btnChangeMoveDirectory.setEnabled(true);
					btnMoveWallpapers.setEnabled(true);
				} else {
					moveFavoriteCheckBox.setSelected(false);
					moveDirectory.setEnabled(false);
					moveDirectory.setText("");
					btnChangeMoveDirectory.setEnabled(false);
					btnMoveWallpapers.setEnabled(false);
				}
			}
	      });
	      
	      /**
	       * btnChangeMoveDirectory Action Listener.
	       */
	      btnChangeMoveDirectory.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					PathChangerWindow pathChangerWindow = new PathChangerWindow(window, WDUtilities.MOVE_DIRECTORY);
					pathChangerWindow.setVisible(true);
	  				// There is a bug with KDE (version 5.9) and the preview window is not painted properly
	  				// It is necessary to reshape this window in order to paint all its components
	  				if (WDUtilities.getWallpaperChanger() instanceof LinuxWallpaperChanger) {
	  					if (((LinuxWallpaperChanger)WDUtilities.getWallpaperChanger()).getDesktopEnvironment() == WDUtilities.DE_KDE) {
	  						pathChangerWindow.setSize(449, 299);  						
	  					}
	  				}
				}
	      });
	      
	      /**
	       * btnMoveWallpapers Action Listener.
	       */
	      btnMoveWallpapers.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					moveFavoriteWallpapers();
				}
	      });

	      /**
	       * btnPause Action Listener.
	       */
	      btnPause.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					pauseDownloadingProcess();
				}
	      });

	      /**
	       * btnPlay Action Listener.
	       */
	      btnPlay.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					resumeDownloadingProcess();
				}
	      });

			/**
			 * btnChooseWallpaper Action Listener.
			 */
			btnChooseWallpaper.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					ChooseWallpaperWindow wmw = new ChooseWallpaperWindow();
					wmw.setVisible(true);
				}
			});
			
  	        /**
		     * btnRandomWallpaper Action Listener.
		     */
		    // Clicking event
			if (WDUtilities.getWallpaperChanger().isWallpaperChangeable()) {
				btnRandomWallpaper.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						WDUtilities.getWallpaperChanger().setRandomWallpaper();
					}
				});
			}

  	        /**
		     * btnNextWallpaper Action Listener.
		     */
		    // Clicking event
			if (WDUtilities.getWallpaperChanger().isWallpaperChangeable()) {
				btnNextWallpaper.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						WDUtilities.getWallpaperChanger().setSortedWallpaper();
					}
				});
			}

			/**
			 * initOnBootCheckBox Action Listener.
			 */
			// Clicking event
			initOnBootCheckBox.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					if (initOnBootCheckBox.isSelected()) {
						// It is necessary to copy wallpaperdownloader.desktop file to ~/.config/autostart
						File srcFile = new File(WDUtilities.getAppPath() + File.separator + WDUtilities.WD_DESKTOP_FILE);
						File destFile = new File(WDUtilities.getAutostartFilePath() + File.separator + WDUtilities.WD_DESKTOP_FILE);
						try {
							FileUtils.copyFile(srcFile, destFile);
							DialogManager info = new DialogManager(i18nBundle.getString("messages.autostart.ok"), 2000);
							info.openDialog();
						} catch (IOException exception) {
							initOnBootCheckBox.setSelected(false);
							if (LOG.isInfoEnabled()) {
								LOG.error("wallpaperdownloader.sh couldn't be copied. Error: " + exception.getMessage());
							}
							DialogManager info = new DialogManager(i18nBundle.getString("messages.autostart.ko"), 2000);
							info.openDialog();
						}
					} else {
						// It is necessary to remove wallpaperdownloader.desktop file from ~/.config/autostart
						File destFile = new File(WDUtilities.getAutostartFilePath() + File.separator + WDUtilities.WD_DESKTOP_FILE);
						try {
							FileUtils.forceDelete(destFile);
							DialogManager info = new DialogManager(i18nBundle.getString("messages.autostart.removed.ok"), 2000);
							info.openDialog();
						} catch (IOException exception) {
							initOnBootCheckBox.setSelected(true);
							if (LOG.isInfoEnabled()) {
								LOG.error("wallpaperdownloader.sh couldn't be copied. Error: " + exception.getMessage());
							}
							DialogManager info = new DialogManager(i18nBundle.getString("messages.autostart.ko"), 2000);
							info.openDialog();
						}
					}
				}
			});
	}

	/**
	 * Pauses downloading process.
	 */
	public static void pauseDownloadingProcess() {
		final PreferencesManager prefm = PreferencesManager.getInstance();
		
		// Downloading process is paused
		prefm.setPreference("downloading-process", WDUtilities.APP_NO);
		
		// Stops harvesting process
		harvester.stop();
		
		// Repainting buttons
		providersPanel.add(btnPlay);
		providersPanel.remove(btnPause);
		providersPanel.add(lblRedSpot);
		providersPanel.remove(lblGreenSpot);
		providersPanel.repaint();
		
		// Information
		if (WDUtilities.getLevelOfNotifications() > 0) {
			DialogManager info = new DialogManager(i18nBundle.getString("messages.downloading.process.paused"), 2000);
			info.openDialog();
		}
	}

	/**
	 * Resumes downloading process.
	 */
	public static void resumeDownloadingProcess() {
		final PreferencesManager prefm = PreferencesManager.getInstance();
		
		// Downloading process is resumed
		prefm.setPreference("downloading-process", WDUtilities.APP_YES);
		
		// Starts harvesting process
		harvester.start();

		// Repainting buttons
		providersPanel.add(btnPause);
		providersPanel.remove(btnPlay);
		providersPanel.add(lblGreenSpot);
		providersPanel.remove(lblRedSpot);
		providersPanel.repaint();

		// Information
		if (WDUtilities.getLevelOfNotifications() > 0) {
			DialogManager info = new DialogManager(i18nBundle.getString("messages.downloading.process.resumed"), 2000);
			info.openDialog();
		}
	}
	
	/**
	 * Restarts downloading process.
	 */
	public static void restartDownloadingProcess() {
		final PreferencesManager prefm = PreferencesManager.getInstance();
		if (prefm.getPreference("downloading-process").equals(WDUtilities.APP_YES)) {
			// The recalibration dialog will be performed within a Swing Timer for avoiding to
			// freeze the entire UI. The event will wait only 1 millisecond in order to display
			// the message. The dialog is important in order to block the entire UI and avoid the
			// user modifies anything else until the harvesting process is stopped and started
			// again
			// one time (setRepeats is false)
			Timer dialogTimer = new Timer(1, new ActionListener() {
			    public void actionPerformed(ActionEvent evt) {
					DialogManager info = new DialogManager(i18nBundle.getString("messages.harvesting.process.recalibrating"), 5000);
					info.openDialog();
			    }    
			});
			dialogTimer.setRepeats(false);
			dialogTimer.start();
			harvester.stop();
			// The start of the harvester will be performed within a Swing Timer for avoiding to
			// freeze the entire UI. The event will wait 5000 milliseconds in order to let the
			// stopping process ends successfully and only will be done
			// one time (setRepeats is false)
			Timer timer = new Timer(5000, new ActionListener() {
			    public void actionPerformed(ActionEvent evt) {
			    	harvester.start();
					// Repaint pause/resume buttons
					pauseResumeRepaint();
			    }    
			});
			timer.setRepeats(false);
			timer.start();
		} else {
			// Repaint pause/resume buttons
			pauseResumeRepaint();
		}

	}

	/**
	 * Minimizes application.
	 */
	public static void minimizeApplication() {
		final PreferencesManager prefm = PreferencesManager.getInstance();
		// The application is minimized within System Tray
        // Check the SystemTray is supported or user has selected no system tray icon
		String systemTrayIconEnable = prefm.getPreference("system-tray-icon");
        if (!SystemTray.isSupported() || !WDUtilities.isMinimizable() || systemTrayIconEnable.equals(WDUtilities.APP_NO)) {
            LOG.error("SystemTray is not supported. Frame is traditionally minimized");
            // Frame is traditionally minimized
            frmWallpaperdownloaderV.setExtendedState(Frame.ICONIFIED);
            return;
        } else {
        	if (isOldSystemTray()) {
    			// For OS and DE which have an old system tray, legacy mode will be used
                final PopupMenu popup = new PopupMenu();
                URL systemTrayIcon = null;
                // System tray icon
    			if (areProvidersChecked()) {
                    if (!prefm.getPreference("downloading-process").equals(WDUtilities.APP_NO)) {
                    	systemTrayIcon = WallpaperDownloader.class.getResource("/images/icons/wd_systemtray_icon_green.png");
                    } else {
                    	systemTrayIcon = WallpaperDownloader.class.getResource("/images/icons/wd_systemtray_icon_red.png");
                    }
    			} else {
                	systemTrayIcon = WallpaperDownloader.class.getResource("/images/icons/wd_systemtray_icon.png");
    			}
                
                final TrayIcon trayIcon = new TrayIcon(new ImageIcon(systemTrayIcon, "Wallpaper Downloader").getImage(), "Wallpaper Downloader");
                final SystemTray tray = SystemTray.getSystemTray();
               
                // Create a pop-up menu components -- BEGIN
                // Maximize
                java.awt.MenuItem maximizeItem = new java.awt.MenuItem(i18nBundle.getString("system.tray.maximize"));
                maximizeItem.addActionListener(new ActionListener() {
                	public void actionPerformed(ActionEvent evt) {
                    	int state = frmWallpaperdownloaderV.getExtendedState();  
                    	state = state & ~Frame.ICONIFIED;  
                    	frmWallpaperdownloaderV.setExtendedState(state);  
                    	frmWallpaperdownloaderV.setVisible(true);
                    	
                    	// Removing system tray icon
                    	tray.remove(trayIcon);
                	}
                });
                popup.add(maximizeItem);
                
                // Open downloads directory
                if (!WDUtilities.isSnapPackage()) {
                    java.awt.MenuItem browseItem = new java.awt.MenuItem(i18nBundle.getString("system.tray.open"));
                    browseItem.addActionListener(new ActionListener() {
                    	public void actionPerformed(ActionEvent evt) {
                    		File downloadsDirectory = new File(WDUtilities.getDownloadsPath());
                    		Desktop desktop = Desktop.getDesktop();
                    		try {
        						desktop.open(downloadsDirectory);
        					} catch (IOException e) {
        						// There was some error trying to open the downloads Directory
        						LOG.error("Error trying to open the Downloads directory. Error: " + e.getMessage());
        					}
                    	}
                    });
                    popup.add(browseItem);
                }

        		// Pause / Resume
				// Resume/Pause downloading process
    			if (areProvidersChecked()) {
                    java.awt.MenuItem resumeItem = new java.awt.MenuItem(i18nBundle.getString("system.tray.resume"));
                    java.awt.MenuItem pauseItem = new java.awt.MenuItem(i18nBundle.getString("system.tray.pause"));

                    resumeItem.addActionListener(new ActionListener() {
                    	public void actionPerformed(ActionEvent evt) {
                    		resumeDownloadingProcess();
                    		
                    		// Refreshing system tray icon. It will be necessary to
                    		// remove icon and minimize the application again
                    		// Removing system tray icon
                        	tray.remove(trayIcon);

                        	minimizeApplication();
                    	}
                    });

                    pauseItem.addActionListener(new ActionListener() {
                    	public void actionPerformed(ActionEvent evt) {
                    		pauseDownloadingProcess();

                    		// Refreshing system tray icon. It will be necessary to
                    		// remove icon and minimize the application again
                        	// Removing system tray icon
                        	tray.remove(trayIcon);

                        	minimizeApplication();
                    	}
                    });

        			// Checking downloading process
        			if (prefm.getPreference("downloading-process").equals(WDUtilities.APP_NO)) {
        				popup.add(resumeItem);
        			} else {
        				popup.add(pauseItem);
        			}
    			}

                // Change wallpaper randomly and next wallpaper
                if (WDUtilities.getWallpaperChanger().isWallpaperChangeable()) {
                	// Change wallpaper randomly
                	java.awt.MenuItem changeItem = new java.awt.MenuItem(i18nBundle.getString("system.tray.change"));
    	            changeItem.addActionListener(new ActionListener() {
    	            	public void actionPerformed(ActionEvent evt) {
    	            		WDUtilities.getWallpaperChanger().setRandomWallpaper();
    	            	}
    	            });
    	            popup.add(changeItem);
    	            
    	            // Set next wallpaper if changer mode is not randomly
    				String changerMode = prefm.getPreference("application-changer-mode");
    				if (changerMode.equals("1") || changerMode.equals("2")) {
                    	java.awt.MenuItem nextItem = new java.awt.MenuItem(i18nBundle.getString("system.tray.next"));
        	            nextItem.addActionListener(new ActionListener() {
        	            	public void actionPerformed(ActionEvent evt) {
        	            		WDUtilities.getWallpaperChanger().setSortedWallpaper();
        	            	}
        	            });
        	            popup.add(nextItem);
    				}
                }

                // Move favorite wallpapers
        		String moveFavoriteEnable = prefm.getPreference("move-favorite");
        		if (moveFavoriteEnable.equals(WDUtilities.APP_YES)) {
        			java.awt.MenuItem moveItem = new java.awt.MenuItem(i18nBundle.getString("system.tray.move"));
    	            moveItem.addActionListener(new ActionListener() {
    	            	public void actionPerformed(ActionEvent evt) {
    	            		moveFavoriteWallpapers();
    	            	}
    	            });
    	            popup.add(moveItem);
                }

        		// Manage downloaded wallpapers
                java.awt.MenuItem manageItem = new java.awt.MenuItem(i18nBundle.getString("system.tray.manage"));
                manageItem.addActionListener(new ActionListener() {
                	public void actionPerformed(ActionEvent evt) {
        				WallpaperManagerWindow wmw = new WallpaperManagerWindow();
        				wmw.setVisible(true);                    	
        			}
                });
                popup.add(manageItem);

        		// Choose wallpaper
                java.awt.MenuItem chooseItem = new java.awt.MenuItem(i18nBundle.getString("system.tray.choose"));
                chooseItem.addActionListener(new ActionListener() {
                	public void actionPerformed(ActionEvent evt) {
        				ChooseWallpaperWindow cww = new ChooseWallpaperWindow();
        				cww.setVisible(true);
        			}
                });
                popup.add(chooseItem);

                // Separator
                popup.addSeparator();
                
                // Exit
                java.awt.MenuItem exitItem = new java.awt.MenuItem(i18nBundle.getString("system.tray.exit"));
                exitItem.addActionListener(new ActionListener() {
                	public void actionPerformed(ActionEvent evt) {
                    	// Removing system tray icon
                    	tray.remove(trayIcon);

        				// The application is closed
        				System.exit(0);		                	
                	}
                });

                popup.add(exitItem);

                // Create a pop-up menu components -- END

                trayIcon.setPopupMenu(popup);                
                trayIcon.setImageAutoSize(true);
               
                try {
                    tray.add(trayIcon);
                } catch (AWTException e) {
                    LOG.error("TrayIcon could not be added.");
                }
                
                // Hiding window
                frmWallpaperdownloaderV.setVisible(false);        		
        	} else {
    			// GTK3 integration is going to be used for Plasma 5 and GNOME Shell
        		// Changing state
        		frmWallpaperdownloaderV.setExtendedState(Frame.ICONIFIED);
    			// Hiding window
    			frmWallpaperdownloaderV.setVisible(false);
    			Display.setAppName("WallpaperDownloader");
    			Display display = new Display();
    			Shell shell = new Shell(display);
    			// System tray icon
    			InputStream iconInputStream = null;
    			if (areProvidersChecked()) {
        			if (!prefm.getPreference("downloading-process").equals(WDUtilities.APP_NO)) {
                    	iconInputStream = WallpaperDownloader.class.getResourceAsStream("/images/icons/wd_systemtray_icon_green.png");
                    } else {
                    	iconInputStream = WallpaperDownloader.class.getResourceAsStream("/images/icons/wd_systemtray_icon_red.png");
                    }
    			} else {
    				iconInputStream = WallpaperDownloader.class.getResourceAsStream("/images/icons/wd_systemtray_icon.png");
    			}
    			org.eclipse.swt.graphics.Image icon = new org.eclipse.swt.graphics.Image(display, iconInputStream);
    			final Tray tray = display.getSystemTray();
    			if (tray == null) {
    				System.out.println ("The system tray is not available");
    			} else {
    				// Creating the pop up menu
    				final Menu menu = new Menu (shell, SWT.POP_UP);
    				
    				final org.eclipse.swt.widgets.TrayItem item = new org.eclipse.swt.widgets.TrayItem (tray, SWT.NONE);
    				item.setToolTipText("WallpaperDownloader");
    				item.addListener (SWT.DefaultSelection, new Listener () {          
    		            public void handleEvent (Event e) {        	                	
    	                	// Removing system tray icon and all stuff related
    		            	item.dispose();
    		            	icon.dispose();
    		            	menu.dispose();
    	                	tray.dispose();
    	                	shell.dispose();
    	                	display.dispose();

    	                	// frame.setExtendedState(Frame.NORMAL) will be set in the WindowsListener
    	                	frmWallpaperdownloaderV.setVisible(true);
    		            }
    				});
    				
    				// Adding options to the menu
    				// Maximize
    				MenuItem maximize = new MenuItem (menu, SWT.PUSH);
    				maximize.setText (i18nBundle.getString("system.tray.maximize"));
    				maximize.addListener (SWT.Selection, new Listener () {          
    		            public void handleEvent (Event e) {
    	                	// Removing system tray icon and all stuff related
    		            	item.dispose();
    		            	icon.dispose();
    		            	menu.dispose();
    	                	tray.dispose();
    	                	shell.dispose();
    	                	display.dispose();
    	                	
    	                	// frame.setExtendedState(Frame.NORMAL) will be set in the WindowsListener
    	                	frmWallpaperdownloaderV.setVisible(true);
    		            }
    				});

    				// Open downloaded wallpapers
    				if (!WDUtilities.isSnapPackage()) {
        				MenuItem open = new MenuItem (menu, SWT.PUSH);
        				open.setText (i18nBundle.getString("system.tray.open"));
        				open.addListener (SWT.Selection, new Listener () {          
        		            public void handleEvent (Event e) {
                        		File downloadsDirectory = new File(WDUtilities.getDownloadsPath());
                        		Desktop desktop = Desktop.getDesktop();
                        		try {
            						desktop.open(downloadsDirectory);
            					} catch (IOException exception) {
            						// There was some error trying to open the downloads Directory
            						LOG.error("Error trying to open the Downloads directory. Error: " + exception.getMessage());
            					}
        		            }
        				});
    				}

    				// Resume/Pause downloading process
        			if (areProvidersChecked()) {
        				MenuItem resume = new MenuItem (menu, SWT.PUSH);
        				MenuItem pause = new MenuItem (menu, SWT.PUSH);

        				resume.setText (i18nBundle.getString("system.tray.resume"));
        				resume.addListener (SWT.Selection, new Listener () {          
        		            public void handleEvent (Event e) {
                        		resumeDownloadingProcess();
                        		
        	                	// Removing system tray icon and all stuff related
        		            	item.dispose();
        		            	icon.dispose();
        		            	menu.dispose();
        	                	tray.dispose();
        	                	shell.dispose();
        	                	display.dispose();

        	                	// Minimizing again the application to refresh the icon
        	                	// in the system tray
        	                	minimizeApplication();
        		            }
        				});

    					pause.setText (i18nBundle.getString("system.tray.pause")); 
    					pause.addListener (SWT.Selection, new Listener () {          
        		            public void handleEvent (Event e) {
                        		pauseDownloadingProcess();

        	                	// Removing system tray icon and all stuff related
        		            	item.dispose();
        		            	icon.dispose();
        		            	menu.dispose();
        	                	tray.dispose();
        	                	shell.dispose();
        	                	display.dispose();

        	                	// Minimizing again the application to refresh the icon
        	                	// in the system tray
        	                	minimizeApplication();
        		            }
        				});
        				
            			// Checking downloading process
            			if (prefm.getPreference("downloading-process").equals(WDUtilities.APP_NO)) {
            				pause.setEnabled(false);
            				resume.setEnabled(true);
            			} else {
            				resume.setEnabled(false);
            				pause.setEnabled(true);
            			}
        			}

                    // Change wallpaper randomly and set next wallpaper
                    if (WDUtilities.getWallpaperChanger().isWallpaperChangeable()) {
        				// Change wallpaper randomly
                    	MenuItem change = new MenuItem (menu, SWT.PUSH);
    					change.setText (i18nBundle.getString("system.tray.change"));
        				change.addListener (SWT.Selection, new Listener () {          
        		            public void handleEvent (Event e) {
        	            		WDUtilities.getWallpaperChanger().setRandomWallpaper();
        		            }
        				});
        				// Set next wallpaper if changer mode is not randomly
        				String changerMode = prefm.getPreference("application-changer-mode");
        				if (changerMode.equals("1") || changerMode.equals("2")) {
                        	MenuItem next = new MenuItem (menu, SWT.PUSH);
        					next.setText (i18nBundle.getString("system.tray.next"));
            				next.addListener (SWT.Selection, new Listener () {          
            		            public void handleEvent (Event e) {
            	            		WDUtilities.getWallpaperChanger().setSortedWallpaper();
            		            }
            				});
        				}
                    }

                    // Move favorite wallpapers
    				String moveFavoriteEnable = prefm.getPreference("move-favorite");
            		if (moveFavoriteEnable.equals(WDUtilities.APP_YES)) {
        				MenuItem move = new MenuItem (menu, SWT.PUSH);
    					move.setText (i18nBundle.getString("system.tray.move"));
        				move.addListener (SWT.Selection, new Listener () {          
        		            public void handleEvent (Event e) {
        	            		moveFavoriteWallpapers();
        		            }
        				});
                    }

            		// Manage downloaded wallpapers
    				MenuItem manage = new MenuItem (menu, SWT.PUSH);
					manage.setText (i18nBundle.getString("system.tray.manage"));
    				manage.addListener (SWT.Selection, new Listener () {          
    		            public void handleEvent (Event e) {
    	                	// Removing system tray icon and all stuff related
    		            	item.dispose();
    		            	icon.dispose();
    		            	menu.dispose();
    	                	tray.dispose();
    	                	shell.dispose();
    	                	display.dispose();
    	                	
    	                	fromSystemTray = true;
    	                	frmWallpaperdownloaderV.setExtendedState(Frame.NORMAL); 
    	                	frmWallpaperdownloaderV.setVisible(true);

            				WallpaperManagerWindow wmw = new WallpaperManagerWindow();
            				wmw.setVisible(true);
    		            }
    				});

            		// Choose wallpaper
    				MenuItem choose = new MenuItem (menu, SWT.PUSH);
					choose.setText (i18nBundle.getString("system.tray.choose"));
    				choose.addListener (SWT.Selection, new Listener () {          
    		            public void handleEvent (Event e) {
    	                	// Removing system tray icon and all stuff related
    		            	item.dispose();
    		            	icon.dispose();
    		            	menu.dispose();
    	                	tray.dispose();
    	                	shell.dispose();
    	                	display.dispose();
    	                	
    	                	fromSystemTray = true;
    	                	frmWallpaperdownloaderV.setExtendedState(Frame.NORMAL);
    	                	frmWallpaperdownloaderV.setVisible(true);

            				ChooseWallpaperWindow cww = new ChooseWallpaperWindow();
            				cww.setVisible(true);
    		            }
    				});

                    // Exit
    				MenuItem exit = new MenuItem (menu, SWT.PUSH);
					exit.setText (i18nBundle.getString("system.tray.exit"));
    				exit.addListener (SWT.Selection, new Listener () {          
    		            public void handleEvent (Event e) {
    	                	// Removing system tray icon and all stuff related
    		            	icon.dispose();
    		            	item.dispose();
    		            	menu.dispose();
    	                	tray.dispose();
    	                	shell.dispose();
    	                	display.dispose();

            				// The application is closed
            				System.exit(0);		                	
    		            }
    				});

    			    item.addListener(SWT.MenuDetect, new Listener() {
    			    	public void handleEvent(Event event) {
    			    		menu.setVisible(true);
    			        }
    			    });
    			    
    				item.setImage(icon);
    			}
    			
    			while (!shell.isDisposed ()) {
    				if (!display.readAndDispatch ()) display.sleep ();
    			}
    			
    			icon.dispose();
    			display.dispose ();
        	}
        }
	}
	
	private static void moveFavoriteWallpapers() {

		final PreferencesManager prefm = PreferencesManager.getInstance();
		
		// Dialog for please wait. It has to be executed on a SwingWorker (another Thread) in 
		// order to not block the entire execution of the application
		final DialogManager pleaseWaitDialog = new DialogManager(i18nBundle.getString("messages.wait"));

	    SwingWorker<String, Void> worker = new SwingWorker<String, Void>() {
	        @Override
	        protected String doInBackground() throws InterruptedException  {
				WDUtilities.moveFavoriteWallpapers(prefm.getPreference("move-favorite-folder"));
				refreshProgressBar();
	        	return null;
	        }
	        @Override
	        protected void done() {
	            pleaseWaitDialog.closeDialog();
	        }
	    };
	    worker.execute();
	    pleaseWaitDialog.openDialog();
	    try {
	        worker.get();
	    } catch (Exception e1) {
	        e1.printStackTrace();
	    }
		
		// Information
		if (WDUtilities.getLevelOfNotifications() > 1) {
			DialogManager info = new DialogManager(i18nBundle.getString("messages.moved"), 2000);
			info.openDialog();
		}
		
	}

	/**
	 * This method configures GUI according to user configuration file preferences
	 */
	private void initializeGUI() {

		final PreferencesManager prefm = PreferencesManager.getInstance();

		// ---------------------------------------------------------------------
		// Checking providers
		// ---------------------------------------------------------------------
		// Search keywords
		if (!prefm.getPreference("provider-wallhaven-keywords").equals(PreferencesManager.DEFAULT_VALUE)) {
			searchKeywords.setText(prefm.getPreference("provider-wallhaven-keywords"));			
		} else {
			searchKeywords.setText("");
		}
		searchKeywords.setEnabled(false);

		// Keywords to avoid
		if (!prefm.getPreference("avoid-keywords").equals(PreferencesManager.DEFAULT_VALUE)) {
			forbiddenKeywords.setText(prefm.getPreference("avoid-keywords"));			
		} else {
			forbiddenKeywords.setText("");
		}
		forbiddenKeywords.setEnabled(false);

		// Resolution
		String[] resolution = prefm.getPreference("wallpaper-resolution").split("x");
        widthResolution.setValue(new Integer(resolution[0]));
        widthResolution.setEnabled(false);
		heightResolution.setValue(new Integer(resolution[1]));
        heightResolution.setEnabled(false);

        // Download policy
        downloadPolicyComboBox.addItem(new ComboItem(i18nBundle.getString("providers.download.policy.0"), "0"));
        downloadPolicyComboBox.addItem(new ComboItem(i18nBundle.getString("providers.download.policy.1"), "1"));
        downloadPolicyComboBox.addItem(new ComboItem(i18nBundle.getString("providers.download.policy.2"), "2"));
        downloadPolicyComboBox.setSelectedIndex(new Integer(prefm.getPreference("download-policy")));
        
		// Wallhaven.cc
		String wallhavenEnable = prefm.getPreference("provider-wallhaven");
		if (wallhavenEnable.equals(WDUtilities.APP_YES)) {
			wallhavenCheckbox.setSelected(true);
			searchTypeWallhavenComboBox.setEnabled(true);
		} else {
			searchTypeWallhavenComboBox.setEnabled(false);
		}
		searchTypeWallhavenComboBox.addItem(new ComboItem(i18nBundle.getString("providers.wallhaven.policy.0"), "0"));
		searchTypeWallhavenComboBox.addItem(new ComboItem(i18nBundle.getString("providers.wallhaven.policy.1"), "1")); 
		searchTypeWallhavenComboBox.addItem(new ComboItem(i18nBundle.getString("providers.wallhaven.policy.2"), "2")); 
		searchTypeWallhavenComboBox.addItem(new ComboItem(i18nBundle.getString("providers.wallhaven.policy.3"), "3")); 
		searchTypeWallhavenComboBox.addItem(new ComboItem(i18nBundle.getString("providers.wallhaven.policy.4"), "4"));
		searchTypeWallhavenComboBox.setSelectedIndex(new Integer(prefm.getPreference("wallpaper-search-type")));
		
		// DeviantArt
		String deviantArtEnable = prefm.getPreference("provider-devianart");
		if (deviantArtEnable.equals(WDUtilities.APP_YES)) {
			deviantArtCheckbox.setSelected(true);
			deviantArtSearchTypeComboBox.setEnabled(true);
		} else {
			deviantArtSearchTypeComboBox.setEnabled(false);
		}
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.0"), "0")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.1"), "1")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.2"), "2")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.3"), "3")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.4"), "4")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.5"), "5")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.6"), "6")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.7"), "7")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.8"), "8")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.9"), "9")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.10"), "10")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.11"), "11")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.12"), "12")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.13"), "13")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.14"), "14")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.15"), "15")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.16"), "16")); 
		deviantArtSearchTypeComboBox.addItem(new ComboItem(i18nBundle.getString("providers.deviantart.policy.17"), "17")); 
		deviantArtSearchTypeComboBox.setSelectedIndex(new Integer(prefm.getPreference("wallpaper-devianart-search-type")));

		// Bing
		String bingEnable = prefm.getPreference("provider-bing");
		if (bingEnable.equals(WDUtilities.APP_YES)) {
			bingCheckbox.setSelected(true);
		}

		// Social Wallpapering
		String socialWallpaperingEnable = prefm.getPreference("provider-socialWallpapering");
		if (socialWallpaperingEnable.equals(WDUtilities.APP_YES)) {
			socialWallpaperingCheckbox.setSelected(true);
			socialWallpaperingIgnoreKeywordsCheckbox.setEnabled(true);
		} else {
			socialWallpaperingIgnoreKeywordsCheckbox.setEnabled(false);
		}
		if (prefm.getPreference("provider-socialWallpapering-ignore-keywords").equals(WDUtilities.APP_YES)) {
			socialWallpaperingIgnoreKeywordsCheckbox.setSelected(true);
		} else {
			socialWallpaperingIgnoreKeywordsCheckbox.setSelected(false);
		}
		
		// WallpaperFusion
		String wallpaperFusionEnable = prefm.getPreference("provider-wallpaperFusion");
		if (wallpaperFusionEnable.equals(WDUtilities.APP_YES)) {
			wallpaperFusionCheckbox.setSelected(true);
		}

		// DualMonitorBackground
		String dualMonitorEnable = prefm.getPreference("provider-dualMonitorBackgrounds");
		if (dualMonitorEnable.equals(WDUtilities.APP_YES)) {
			dualMonitorCheckbox.setSelected(true);
			searchTypeDualMonitorComboBox.setEnabled(true);
		} else {
			searchTypeDualMonitorComboBox.setEnabled(false);
		}
		searchTypeDualMonitorComboBox.addItem(new ComboItem(i18nBundle.getString("providers.dual.monitor.policy.0"), "0")); 
		searchTypeDualMonitorComboBox.addItem(new ComboItem(i18nBundle.getString("providers.dual.monitor.policy.1"), "1")); 
		searchTypeDualMonitorComboBox.addItem(new ComboItem(i18nBundle.getString("providers.dual.monitor.policy.2"), "2")); 
		searchTypeDualMonitorComboBox.addItem(new ComboItem(i18nBundle.getString("providers.dual.monitor.policy.3"), "3")); 
		searchTypeDualMonitorComboBox.setSelectedIndex(new Integer(prefm.getPreference("provider-dualMonitorBackgrounds-search-type")));

		// Unsplash
		String UnsplashEnable = prefm.getPreference("provider-unsplash");
		if (UnsplashEnable.equals(WDUtilities.APP_YES)) {
			unsplashCheckbox.setSelected(true);
			searchTypeUnsplashComboBox.setEnabled(true);
		} else {
			searchTypeUnsplashComboBox.setEnabled(false);
		}
		searchTypeUnsplashComboBox.addItem(new ComboItem(i18nBundle.getString("providers.unsplash.policy.0"), "0")); 
		searchTypeUnsplashComboBox.addItem(new ComboItem(i18nBundle.getString("providers.unsplash.policy.1"), "1")); 
		searchTypeUnsplashComboBox.setSelectedIndex(new Integer(prefm.getPreference("provider-unsplash-search-type")));
		
		// ---------------------------------------------------------------------
		// Checking user settings
		// ---------------------------------------------------------------------
		downloadingTime.setText(prefm.getPreference("application-timer"));
		timeComboBox1.addItem(new ComboItem(i18nBundle.getString("common.time.unit.seconds"), i18nBundle.getString("common.time.unit.seconds")));
		timeComboBox1.addItem(new ComboItem(i18nBundle.getString("common.time.unit.minutes"), i18nBundle.getString("common.time.unit.minutes")));
		timeComboBox1.addItem(new ComboItem(i18nBundle.getString("common.time.unit.hours"), i18nBundle.getString("common.time.unit.hours")));
		timeComboBox1.getModel().setSelectedItem(new ComboItem(prefm.getPreference("application-timer-units"), prefm.getPreference("application-timer-units")));
		
		String moveFavoriteEnable = prefm.getPreference("move-favorite");
		if (moveFavoriteEnable.equals(WDUtilities.APP_YES)) {
			moveFavoriteCheckBox.setSelected(true);
			moveDirectory.setEnabled(true);
			moveDirectory.setText(prefm.getPreference("move-favorite-folder"));
			btnChangeMoveDirectory.setEnabled(true);
			btnMoveWallpapers.setEnabled(true);
		} else {
			moveFavoriteCheckBox.setSelected(false);
			moveDirectory.setEnabled(false);
			btnChangeMoveDirectory.setEnabled(false);
			btnMoveWallpapers.setEnabled(false);
		}

		// Move favorite feature
		if (prefm.getPreference("move-favorite").equals(PreferencesManager.DEFAULT_VALUE)) {
			prefm.setPreference("move-favorite", WDUtilities.APP_NO);
			prefm.setPreference("move-favorite-folder", PreferencesManager.DEFAULT_VALUE);
		}
		
		// Notifications
		notificationsComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.notifications.0"), "0"));
		notificationsComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.notifications.1"), "1"));
		notificationsComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.notifications.2"), "2"));
		notificationsComboBox.setSelectedIndex(new Integer(prefm.getPreference("application-notifications")));

		// i18n
		i18nComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.i18n.0"), "0"));
		i18nComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.i18n.1"), "1"));
		i18nComboBox.setSelectedIndex(new Integer(prefm.getPreference("application-i18n")));

		// Start minimized feature
		String startMinimizedEnable = prefm.getPreference("start-minimized");
		if (startMinimizedEnable.equals(WDUtilities.APP_YES)) {
			startMinimizedCheckBox.setSelected(true);
			timeToMinimizeComboBox.setEnabled(true);
		} else {
			startMinimizedCheckBox.setSelected(false);
			timeToMinimizeComboBox.setEnabled(false);
		}
		
		// Time to minimize
		timeToMinimizeComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.time.minimize.0"), "1"));
		timeToMinimizeComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.time.minimize.1"), "2"));
		timeToMinimizeComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.time.minimize.2"), "3"));
		timeToMinimizeComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.time.minimize.3"), "4"));
		timeToMinimizeComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.time.minimize.4"), "5"));
		timeToMinimizeComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.time.minimize.5"), "6"));
		timeToMinimizeComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.time.minimize.6"), "7"));
		timeToMinimizeComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.time.minimize.7"), "8"));
		timeToMinimizeComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.time.minimize.8"), "9"));
		timeToMinimizeComboBox.addItem(new ComboItem(i18nBundle.getString("application.settings.time.minimize.9"), "10"));
		timeToMinimizeComboBox.setSelectedIndex((new Integer(prefm.getPreference("time-to-minimize")) - 1));

		// System tray icon
		if (SystemTray.isSupported() && 
				!WDUtilities.getOperatingSystem().equals(WDUtilities.OS_MACOSX) &&
				WDUtilities.getArchitecture().equals(WDUtilities.AMD_ARCHITECTURE)) {
			String systemTrayIconEnable = prefm.getPreference("system-tray-icon");
			if (systemTrayIconEnable.equals(WDUtilities.APP_YES)) {
				stIconCheckBox.setSelected(true);
			} else {
				stIconCheckBox.setSelected(false);
			}
		}
		
		// Starts on boot
		if (WDUtilities.getOperatingSystem().equals(WDUtilities.OS_WINDOWS) || 
			WDUtilities.getOperatingSystem().equals(WDUtilities.OS_WINDOWS_7) || 
			WDUtilities.getOperatingSystem().equals(WDUtilities.OS_WINDOWS_10) || 
			WDUtilities.isSnapPackage() || 
			WDUtilities.isFlatpakPackage() || 
			WDUtilities.getOperatingSystem().equals(WDUtilities.OS_MACOSX)) {
			// In these OS, users won't be able to start the application when the system is booted checking
			// the GUI option
			// This option is not supported within snap and flatpak packages either
			initOnBootCheckBox.setEnabled(false);
		} else {
			String autostartFilePath = WDUtilities.getAutostartFilePath();
			File autostartFile = new File(autostartFilePath + WDUtilities.WD_DESKTOP_FILE);
			if (autostartFile.exists()) {
				initOnBootCheckBox.setSelected(true);
			} else {
				initOnBootCheckBox.setSelected(false);
			}
		}
		
		// Changer
		if (WDUtilities.getWallpaperChanger().isWallpaperChangeable()) {
			changingTime.setText(prefm.getPreference("application-changer"));
			timeComboBox2.addItem(new ComboItem(i18nBundle.getString("common.time.unit.seconds"), i18nBundle.getString("common.time.unit.seconds")));
			timeComboBox2.addItem(new ComboItem(i18nBundle.getString("common.time.unit.minutes"), i18nBundle.getString("common.time.unit.minutes")));
			timeComboBox2.addItem(new ComboItem(i18nBundle.getString("common.time.unit.hours"), i18nBundle.getString("common.time.unit.hours")));
			timeComboBox2.getModel().setSelectedItem(new ComboItem(prefm.getPreference("application-changer-units"), prefm.getPreference("application-changer-units")));

			modeComboBox.addItem(new ComboItem(i18nBundle.getString("changer.mode.0"), "0"));
			modeComboBox.addItem(new ComboItem(i18nBundle.getString("changer.mode.1"), "1"));
			modeComboBox.addItem(new ComboItem(i18nBundle.getString("changer.mode.2"), "2"));
			modeComboBox.setSelectedIndex(new Integer(prefm.getPreference("application-changer-mode")));
			
			if (prefm.getPreference("application-changer-mode").equals("0")) {
				btnRestartWallpapers.setVisible(false);
			} else {
				btnRestartWallpapers.setVisible(true);
			}
			
			listDirectoriesModel = new DefaultListModel<String>();
			String changerFoldersProperty = prefm.getPreference("application-changer-folder");
			String[] changerFolders = changerFoldersProperty.split(";");
			for (int i = 0; i < changerFolders.length; i ++) {
				listDirectoriesModel.addElement(changerFolders[i]);
			}
			listDirectoriesToWatch.setModel(listDirectoriesModel);
			changerPanel.add(btnAddDirectory);
			if (listDirectoriesModel.size() > 1) {
				changerPanel.add(btnRemoveDirectory);				
			}

			// Multi monitor support
			if (WDUtilities.isGnomeish()) {
				String multiMonitorSupport = prefm.getPreference("application-changer-multimonitor");
				if (multiMonitorSupport.equals(WDUtilities.APP_YES)) {
					multiMonitorCheckBox.setSelected(true);
				} else {
					multiMonitorCheckBox.setSelected(false);
				}
			}

			// Next wallpaper
			String changerMode = prefm.getPreference("application-changer-mode");
			if (changerMode.equals("1") || changerMode.equals("2")) {
				btnNextWallpaper.setVisible(true);
			} else {
				btnNextWallpaper.setVisible(false);
			}
		}

		// Directory size
		downloadDirectorySize.setValue(new Integer(prefm.getPreference("application-max-download-folder-size")));
		downloadDirectorySize.setEnabled(false);
		
		// ---------------------------------------------------------------------
		// Checking Miscellanea
		// ---------------------------------------------------------------------
		downloadsDirectory.setValue(WDUtilities.getDownloadsPath());
		// ---------------------------------------------------------------------
		// Checking Disk Space Progress Bar
		// ---------------------------------------------------------------------
		refreshProgressBar();
		// ---------------------------------------------------------------------
		// Populating JScrollPane with the last 5 wallpapers downloaded
		// ---------------------------------------------------------------------
		refreshJScrollPane();
		// ---------------------------------------------------------------------
		// Checking About tab
		// ---------------------------------------------------------------------
		version.setText("4.4.2");
		developer.setText("Eloy Garcia Almaden (eloy.garcia.pca@gmail.com)");
	}
	
	/**
	 * This method starts the harvesting process.
	 */
	private void initializeHarvesting() {
		harvester = Harvester.getInstance();
		harvester.start();
	}

	/**
	 * Starts automated wallpaper changing process.
	 */
	private void initializeChanger() {
		changer = ChangerDaemon.getInstance();
		changer.start();
	}

	/**
	 * This method refreshes the progress bar representing the space occupied within the downloads directory.
	 */
	public static void refreshProgressBar() {
		int percentage = WDUtilities.getPercentageSpaceOccupied(WDUtilities.getDownloadsPath());
		// If percentage is 90% or higher, the warning label and icon will be shown
		if (percentage >= 90) {
			lblSpaceWarning.setVisible(true);
		} else {
			lblSpaceWarning.setVisible(false);
		}
		diskSpacePB.setValue(percentage);
	}
	
	/**
	 * This method refreshes the JScrollPane with the last 5 wallpapers downloaded
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	public static void refreshJScrollPane() {
		ImageIcon[] wallpapers = ImageUtilities.getImageIconWallpapers(5, 0, ImageUtilities.SORTING_BY_DATE, WDUtilities.WD_PREFIX);
		lastWallpapersList = new JList(wallpapers);
		lastWallpapersList.setFixedCellHeight(100);
		lastWallpapersList.setFixedCellWidth(128);
		lastWallpapersList.setSelectionBackground(Color.cyan);

		changePointerJList();
		scroll.setViewportView(lastWallpapersList);
		// JList single selection
		lastWallpapersList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		// JList horizontal orientation
		lastWallpapersList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		// Only 1 row to display
		lastWallpapersList.setVisibleRowCount(1);
		// Using a custom render to render every element within JList
		lastWallpapersList.setCellRenderer(new WallpaperListRenderer(WallpaperListRenderer.WITHOUT_TEXT));
	}
	
	/**
	 * This method changes the pointer when the user moves the mouse over the JList
	 */
	private static void changePointerJList() {
	  lastWallpapersList.addMouseMotionListener(new MouseMotionListener() {
    	    public void mouseMoved(MouseEvent e) {
    	        final int x = e.getX();
    	        final int y = e.getY();
    	        // only display a hand if the cursor is over the items
    	        final Rectangle cellBounds = lastWallpapersList.getCellBounds(0, lastWallpapersList.getModel().getSize() - 1);
    	        if (cellBounds != null && cellBounds.contains(x, y)) {
    	        	lastWallpapersList.setCursor(new Cursor(Cursor.HAND_CURSOR));
    	        } else {
    	        	lastWallpapersList.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    	        }
    	    }

    	    public void mouseDragged(MouseEvent e) {
    	    }
	  });		
	}
	
	/**
	 * Is an old system tray?.
	 * If OS is Windows or Desktop Environment is MATE, XFCE or Unity, then it is
	 * considered an old system tray
	 * @return boolean
	 */
	private static boolean isOldSystemTray() {
		boolean oldSystemTray = false;
		if (WDUtilities.getOperatingSystem().equals(WDUtilities.OS_WINDOWS) || 
        		WDUtilities.getOperatingSystem().equals(WDUtilities.OS_WINDOWS_7) ||	
        		WDUtilities.getOperatingSystem().equals(WDUtilities.OS_WINDOWS_10) ||	
        		(WDUtilities.getWallpaperChanger() instanceof LinuxWallpaperChanger 
        				&& !((LinuxWallpaperChanger)WDUtilities.getWallpaperChanger()).getDesktopEnvironment().equals(WDUtilities.DE_GNOME3) 
        				&& !((LinuxWallpaperChanger)WDUtilities.getWallpaperChanger()).getDesktopEnvironment().equals(WDUtilities.DE_KDE) 
        				&& !((LinuxWallpaperChanger)WDUtilities.getWallpaperChanger()).getDesktopEnvironment().equals(WDUtilities.DE_CINNAMON)
						&& !((LinuxWallpaperChanger)WDUtilities.getWallpaperChanger()).getDesktopEnvironment().equals(WDUtilities.DE_PANTHEON))
        		) {

			oldSystemTray = true;
		}
		return oldSystemTray;
	}
	
	/**
	 * Pauses and resumes the harvesting process and repaints the display if it is needed.
	 */
	private static void pauseResumeRepaint() {
		PreferencesManager prefm = PreferencesManager.getInstance();		
		if (harvester.getStatus() == Harvester.STATUS_ENABLED) {
			// Harvesting process is enabled
			// Checking downloading process
			if (prefm.getPreference("downloading-process").equals(WDUtilities.APP_NO)) {
				providersPanel.add(btnPlay);
				providersPanel.add(lblRedSpot);
				providersPanel.remove(lblGreenSpot);
			} else {
				providersPanel.add(btnPause);
				providersPanel.add(lblGreenSpot);
				providersPanel.remove(lblRedSpot);
			}
		} else {
			// Harvesting process is disabled
			if (areProvidersChecked()) {
				// There are providers checked
				providersPanel.remove(btnPause);
				providersPanel.remove(lblGreenSpot);
				providersPanel.add(btnPlay);
				providersPanel.add(lblRedSpot);
			} else {
				// There aren't providers checked
				providersPanel.remove(btnPause);
				providersPanel.remove(btnPlay);
				providersPanel.add(lblRedSpot);
				providersPanel.remove(lblGreenSpot);
			}
		}
		providersPanel.repaint();
	}

	/**
	 * Checks if there are providers scheduled.
	 * @return
	 */
	private static boolean areProvidersChecked() {
		boolean areProvidersChecked = false;
		if (wallhavenCheckbox.isSelected() || 
			deviantArtCheckbox.isSelected() || 
			bingCheckbox.isSelected() || 
			socialWallpaperingCheckbox.isSelected() || 
			dualMonitorCheckbox.isSelected() || 
			wallpaperFusionCheckbox.isSelected() ||
			unsplashCheckbox.isSelected()) {
			areProvidersChecked = true;
		}
		return areProvidersChecked;
	}
}
