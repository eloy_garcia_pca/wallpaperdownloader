/**
 * Copyright 2016-2021 Eloy García Almadén <eloy.garcia.pca@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.estoes.wallpaperDownloader.harvest;

import java.util.LinkedList;

import javax.swing.SwingWorker;

import org.apache.log4j.Logger;

import es.estoes.wallpaperDownloader.exception.ProviderException;
import es.estoes.wallpaperDownloader.provider.Provider;
import es.estoes.wallpaperDownloader.util.PreferencesManager;
import es.estoes.wallpaperDownloader.util.WDUtilities;

/**
* This class implements SwingWorker abstract class. It is used to execute a independent 
* Thread from the main one (Event Dispatch Thread) which renders the GUI, listens to the
* events produced by keyboard and elements of the GUI, etc...
* The Thread created by SwingWorker class is a Thread in which a heavy process can be 
* executed in background and return control to EDT until it finishes
* More information: http://chuwiki.chuidiang.org/index.php?title=Ejemplo_sencillo_con_SwingWorker
 */
public class BackgroundHarvestingProcess extends SwingWorker<Void, Void> {

	// Constants
	private static final Logger LOG = Logger.getLogger(BackgroundHarvestingProcess.class);
	
	// Attributes
	/**
	 * Providers selected to download wallpapers.
	 */
	private LinkedList<Provider> providers = null;
	/**
	 * Time in milliseconds when the next wallpaper will be downloaded.
	 */
	private Long timer = null;

	// Getters & Setters
	
	/**
	 * Set {@link #providers}.
	 * @param providers
	 */
	public void setProviders(LinkedList<Provider> providers) {
		this.providers = providers;
	}

	// Methods
	/**
	 * This method executes the background process and returns control to EDT
	 * @return 
	 */
	@Override
	protected Void doInBackground() throws Exception {
		PreferencesManager prefm = PreferencesManager.getInstance();
		// Getting timer
		Integer timeInterval = new Integer(prefm.getPreference("application-timer"));
		String timeUnit = prefm.getPreference("application-timer-units");
		switch (timeUnit) {
		case "s":
			this.timer = Long.valueOf(timeInterval * 1000);
			break;
		case "m":
			this.timer = Long.valueOf(timeInterval * 60 * 1000);
			break;
		case "h":
			this.timer = Long.valueOf(timeInterval * 60 * 60 * 1000);
			break;
		default:
			this.timer = Long.valueOf(timeInterval * 1000);
			break;
		}
		LOG.info("Downloading timer set every " + this.timer + " milliseconds");
		// For every Provider
		// 1.- Getting 1 wallpaper per defined keyword
		// 2.- When all the keywords have been used, take provider and put it at the end of 
		// the list
		// 3.- Starting again with the next provider
		while (providers.size()>0) {
			// First, Internet connection is tested
			if (WDUtilities.checkConnectivity()) {
				// If there is connectivity, then the process must go on
				Provider provider = providers.removeFirst(); 
				provider.obtainKeywords();
				try {
					while (!provider.getAreKeywordsDone()) {
						provider.getWallpaper();
						Thread.sleep(timer);
					}				
				} catch (ProviderException pe) {
					// Do nothing
				}
				providers.addLast(provider);					
			} else {
				// If there is no connectivity, wait for 60 seconds and try again
				if(LOG.isInfoEnabled()) {
					LOG.error("No connection to Internet. Harvesting process will try to download more wallpapers in 60 seconds. Please wait...");
				}
				Thread.sleep(Long.valueOf(60000));
				
			}
		}
		return null;
	}


}
