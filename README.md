# WallpaperDownloader Application #

## Home

This is the Wiki related to WallpaperDownloader Project. It gathers all the important information attached to this software project.

## Quick summary
This is a JAVA software project developed for downloading, managing and changing your favorite wallpapers from different sources in Internet via GUI. It is a crossed-platform standalone application: it 
runs in Linux (GNOME Shell, KDE Plasma 5.8 (and higher), Unity, MATE, XFCE, Cinnamon, Pantheon and Budgie DE supported), Microsoft Windows (7,8 and 10) and macOS.

## Current Version
4.4.2

## Installation
You can install WallpaperDownloader using different ways.

### Arch Linux and derivatives (Manjaro, Garuda, EndevourOS, PeuxOS...)
It can be installed natively from the [AUR](https://aur.archlinux.org/packages/wallpaperdownloader):

    git clone https://aur.archlinux.org/wallpaperdownloader.git
    cd wallpaperdownloader
    makepkg -si

 You can also use an [AUR helper](https://wiki.archlinux.org/index.php/AUR_helpers), such as [yay](https://aur.archlinux.org/packages/yay/) or [paru](https://aur.archlinux.org/packages/paru/):

    yay -S wallpaperdownloader

### Flatpak package
[![Get it on Flathub](https://flathub.org/api/badge?locale=en)](https://flathub.org/apps/es.estoes.wallpaperDownloader)

This is the preferred method if you cannot install it natively. This application is packaged in Flatpak and published in [Flathub](https://flathub.org/apps/details/es.estoes.wallpaperDownloader). If the distro of your choice is integrated with Flatpak out of the box (ElementaryOS or PopOS! for example), you will be able to install it from the **Software center**. If don't, you can follow the [official guide in order to enable Flathub](https://flatpak.org/setup/) and then install the application.

Flatpak version doesn't have any of the limitations that snap package presents and it works flawlessly in all Desktop Environments.

### Snap package
[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-black.svg)](https://snapcraft.io/wallpaperdownloader)

Due to some limitations introduced by the snap packages, WallpaperDownloader could not work properly depending on your Desktop Environment. This application is packaged using [snapcraft tool](http://snapcraft.io/) and it is published in Canonical's [snap store](https://snapcraft.io/wallpaperdownloader). If you are using Ubuntu you can install it directly from the **Software Center**. If don't you can get it from the **Snap store**.

If you want to install WallpaperDownloader from the terminal:

    sudo snap install wallpaperdownloader

**Only for Ubuntu users (16.04 and above):** If you install the snap package in Ubuntu, it is recommended to install **snapd-xdg-open** package to allow WallpaperDownloader snap 
application to open some links in your browser.

    sudo apt install snapd-xdg-open

> *Caveats*: Snap package fully supports **GNOME Shell**, **Unity**, **MATE**, **Budgie** and **Pantheon** desktop environments. If you are using **KDE Plasma 5 (version 5.8 or greater) and 6** or **XFCE** 
and your distro of choice is **Ubuntu**, then installation via **Flatpak** package is recommended, because it doesn't have any limitations.

### PPA (Ubuntu and derivatives)
> *Important*: Due to the inclusion of new libraries in version 4.2 that are not packaged natively in Debian repositories and my lack of time to investigate and maintain the official deb package via PPA, **the last version of WallpaperDownloader available in the official PPA will be 4.1**. If you want to continue using the application and receiving updates, please install it via **Flatpak** (it supports all Desktop Environments out of the box) or via **Snap** (all the features works flawlessly on all Desktop Environments except **KDE Plasma 5** and **XFCE**)

There is an official **PPA repository** for installing WallpaperDownloader in Ubuntu (16.04 and greater) and derivatives natively. It is the preferred method for enabling all the features of the application and it is recommended for **KDE Plasma 5** and **XFCE** users. First, open a terminal and type:

    sudo add-apt-repository ppa:eloy-garcia-pca/wallpaperdownloader

Hit enter. Then type:

    sudo apt update

Hit enter. Then type:

    sudo apt install wallpaperdownloader

Hit enter.

### JAR file (Linux, OSX, Windows with Java)
- Download and install [java](https://java.com/en/download/).
- Download and store wallpaperdownloaded.jar from [wallpaperdownloader repository](https://bitbucket.org/eloy_garcia_pca/wallpaperdownloader/src)
- Go to the directory where you stored the file and double click on it. (If you are using Linux, you can execute it via terminal. Open a terminal, cd to the directory where you
stored it and execute it using *java -Xmx256m -Xms128m -jar wallpaperdownloader.jar*)

## Version 4.4.2
Bugs fixed:

- WallpaperDownloader via Flatpak now works properly in XFCE desktop environment (thank you very very much for the hard work Guilherme!).

## Version 4.4.1
Bugs fixed:

- Issue #79. The changer module of WallpaperDownloader can change the wallpaper again in GNOME (dark mode enabled).

## Version 4.4.0
New features:

- KDE Plasma 6 support has been implemented.
- The application icon based on the WallpaperDownloader logo has been defined.

Bugs fixed:

- The width of some combo boxes have been increased in order to improve their display in the Flatpak package.

## Version 4.3
New features:

- AUR package has been improved. Thank you very much Felipe (cbr) for your time and effort!.

Bugs fixed:

- Issue #69: Now, WallpaperDownloader can change the wallpaper in GNOME 42 and higher (dark mode).

## Version 4.2
New features:

- New provider implemented (Unsplash). WallpaperDownloader has passed all the requirements imposed by Unsplash in order to gain access to its official API.

- Preview window has been redesigned and now it is possible to see some interesting information about the wallpaper downloaded like the absolute path where it is stored, width, heigh and extended information in case the wallpaper comes from Unsplash like the author, profile URL, etc.

- Autostart option has been disabled in Flatpak version (Thanks Guilherme Silva!).

Bugs fixed:

- Version key within .desktop file has been removed.

- The problem with the layout when user clicks on change resolution button has been fixed.

- Option "-Dappdynamics.force.default.ssl.certificate.validation=false" within the script that runs WallpaperDownloader has been removed.

- System tray now works in Flatpak version.

- Flag -Dsun.java2d.xrender=f is not needed anymore because org.eclipse.swt dependency has been changed to org.eclipse.platform. This library is in charge of the system tray icon support and it is compatible with GTK3. This improvement potentially fixes several problems related to the system tray icon on different Linux desktop environments.

- No more NullPointerExceptions within the log when removing or previewing without any wallpaper selected.

## Version 4.1
New features:

- Up to 17 new wallpaper fetching profiles have been implemented for the DeviantArt provider. Attention: the only profile that supports the keywords entered by the user is 'All'. All other profiles will ignore these search terms.

Bugs fixed:

- DeviantArt provider is fully working again

- Bing provider is fully working again.

## Version 4.0
Bugs fixed:

- Problems with automated changer in XFCE (detected in Manjaro) have been fixed. It has been implemented a more robust method to detect the right device to perform the wallpaper changing.

## Version 3.9
New features:

- Implemented code fixes for supporting Flatpak.

- Flatpak package has been created (thank you very very much for the hard work Guilherme!).

Bugs fixed:

- Some typos fixed (Thanks Guilherme Silva!).

- Problems with automated changer in XFCE fixed.

- Feature of adding and removing folders for automated changer now works flawlessly.

- Minimize feature now works on MacOSX (system tray icon has been disabled in order to avoid problems in this Operating System). Thanks to my personal beta testers Jonathan Rebollo and Fernando Arroyo for your tests and patience!

## Version 3.8
New features:

- Implemented 'Avoid' field within 'Providers' tab to add keywords to avoid. Candidate wallpapers to be downloaded containing any of the terms entered in this field will be automatically discarded. For example, if you are looking for images using the term 'nature' and want to avoid flowers, add the word 'flower' to the field 'Avoid'.

Bugs fixed:

- DeviantArt provider is fully working again.

## Version 3.7
Bugs fixed:

- DeviantArt has changed the structure of the site and because of that, the application didn't download any wallpaper from there. Thanks to Lee Burch for reporting the issue and even to provide a fix in the code to resolve this problem.

## Version 3.6
New features:

- Implemented a new functionality that allows selecting the changer mode (thanks César for the suggestion).

- Three new modes are available for the changer: random (which allows you to change the wallpaper automatically randomly) and two ordered modes. Background change from the oldest to the newest and vice versa.

- New button in the Wallpapers tab that allows you to set the next wallpaper on request if the mode of the chosen changer is not random.

- New option in the system tray icon that allows to set the following wallpaper on request if the mode of the chosen changer is not random.

## Version 3.5
Bugs fixed:

- Wallhaven provider has finished its trial period and now the stable version of the site has been launched. Therefore, it has been necessary to re-implement the code related to this provider so that the search and download of wallpapers will work properly again

## Version 3.4
New Features:

- Deepin desktop environment supported.

- Time in seconds between downloads and wallpaper changing can now be defined manually by the user.

Bugs fixed:

- Fixed timeout on DualMonitorsBackground provider.

## Version 3.3
New Features:

- New texts in help for users who have installed the application via snap package and want to open the links of the application directly in the browser.

Bugs fixed:

- Wallpapers downloaded from DualMonitorBackgrounds provider are not blank anymore.

## Version 3.2
New Features:

- All provider URLs have been changed to https except DualMonitorBackground that does not support it.

- A small indicator has been added to the system tray icon so the user can know, at a glance, if the downloading process is enabled or not.

- New reset settings button within 'Application Settings' tab.

Bugs fixed:

- Fixed certain errors in the system tray icon menu related to the pausing and resuming of the downloading process.

- The 'Changer' tab is again enabled for Ubuntu 18.04 users who continue to use Unity as their Desktop Environment.

- Ubuntu 18.04 users with Unity Desktop Environment can again minimize the application on the system tray.

## Version 3.1
New Features:

- New process of re-adjustment "on the fly" when the user changes parameters in the providers tab.

- Restructuring the 'Application Settings' tab.

- New tab 'Changer' with all the settings for the automated changer.

- New option to start the application automatically once the operating system has booted (this option is only available for Linux users who have installed the application natively and not through the snap package)

Bugs fixed:

- The WallpaperFusion provider now correctly discriminates downloaded wallpapers when the user has selected the policy 'Only wallpapers with the resolution set by the user'.

- Now, when the user unchecks a provider or pauses the download process, the harvester stops the process immediately.

## Version 3.0
New Features:

- New look and feel. System look and feel will be inherited (from the operating system) if it is available or Nimbus if not.

- New icon and system tray icon. Thanks to Jaime Álvarez Fernández!.

- Close buttons have been removed.

- Apply button has been removed. Now, all the changes in the GUI will be directly saved and applied.

- System tray icon enable/disable functionality implemented.

- Buttons to edit and save some fields have been implemented.

- Minimize button has been removed for KDE and GNOME desktop environments.

- New help tab implemented.

- Cinnamon desktop environment support implemented.

- Budgie desktop environment support implemented.

- Pantheon desktop environment support implemented.

Bugs fixed:

- Scrolls and jlists have been polished.

- Snap detection has been improved and some problems related to the daemon which checks Internet connectivity in the snap package have been fixed.

- Fixed http URL for flaticon website.

- Fixed a bug when user removes certain directories for the changer daemon.

- Fixed a bug in changer daemon when selecting a random wallpaper in directories where there are no images.

## Version 2.9
New Features:

- Providers tab gets a new design.

- User can set a global preferred resolution for wallpapers.

- Download policy implemented which will affect all the providers.

- User can set the time to minimize the application when starts minimized.

Bugs fixed:

- Wallpapers from Social Wallpapering provider are now retrieved correctly.

- Search type in DeviantArt provider now works correctly.

- Wallpapers from Bing provider are now retrieved correctly when the original resolution doesn't match the one defined by the user.

- WallpaperDownloader can be minimized again in Windows systems.

## Version 2.8
New Features:

- GNOME  Shell and KDE Plasma icon tray support added.

- New provider implemented (DualBackgroundMonitors).

- New window to choose the wallpaper to be set from all the sources defined.

Bugs fixed:

- Social Wallpapering provider now paginates correctly.

- Thumbnails preview re-implemented (much more better performance).

- Implemented a daemon which checks Internet connectivity and starts the harvesting process when detects it.

## Version 2.7
New Features:

- KDE support added (not available in snap package version).

- Now, user can define several different directories for the automated changer.

- Pause/resume functionality to download wallpapers.

- New option to start the application minimized.

- Changelog added

Bugs fixed:

- Social Wallpapering provider now stores the image files with the correct suffix

## Version 2.6
New Features:

- Windows 10 support added.

- Now, user can define the level of notifications.

## Features (V 2.5)

New Features:

- New providers implemented: [DeviantArt](http://www.deviantart.com/), [Bing](http://www.bing.com/), [Social Wallpapering](http://www.socwall.com/) and 
[WallpaperFusion](https://www.wallpaperfusion.com/)

- **Ubuntu users** who have installed the application via snap package can now click on the links in About tab and the browser will be opened. For this integration, it is necessary 
to install **snapd-xdg-open** package in the host system.

Bugs fixed:

- Now, downloads directory size defined by the user is accurately calculated and the application removes no favorite wallpapers randomly until it fits this size.

## Version 2.4
New Features:

- User can define a custom directory for the changer process and it can be different from the download directory. This feature is useful for users who want to download wallpapers in 
one directory but move them to another one for keeping their collection in a separate location.

- New button to move all the favorite wallpapers to a custom directory set by the user. It is useful for users who keep another location to save all their wallpapers.

Bugs fixed:

- Changer now sets favorite and non favorite wallpapers.

## Version 2.3
New Features:

- Programmable changer: Now, you can program a changer background process to automatically change your wallpaper every 1, 5, 10, 20, 30 or 60 minutes.
- Changer implemented for XFCE desktop environment (only for native version). not implemented in snap package yet.
- A new option to change the wallpaper has been implemented from system tray icon.

Bugs Fixed: 

- Now, if a wallpaper is set as favorite, it isn't downloaded again. 
- Progress bar which shows the space occupied by the wallpapers downloaded is more accurate and it is properly refreshed when new wallpapers are downloaded or removed. 
- Main window is minimized instead of hiding in system tray in desktop environments that don't support traditional system tray icons (such as GNOME 3 and KDE Plasma 5)

## Version 2.2
New Features:

- Image preview: New button to preview the wallpaperdownloaded. Delete, set the current wallpaper or set as favorite or unset it directly from the preview window.
- Changer: A new button has been implemented to set the current wallpaper from the application. It works for GNOME 3, Unity and MATE desktop environments and Windows 7 too.

## Version 2.1
New Features:

- About tab to display contact info implemented.
- The application now remembers the wallpapers removed by the user. This way, it won't download them again.
- Incomplete downloads for some wallpapers and PNG downloads fixed.
- Some parameters used to search within Wallhaven have been either removed or changed in order to work properly.

## Version 2.0
New Features:

- Wallpaper management revamped. A new interface for managing (delete, set favorite and set no favorite) wallpapers has been implemented. All these tasks are now available in a new 
window.

## Version 1.7
New Features:

- A new tab (Wallpapers) has been implemented
- Last 5 wallpapers feature: Now, the user can see the last 5 wallpapers downloaded and interact with them directly (setting the wallpapers as favorite or removing them)
- A button (and tool) for managing favorite wallpapers has been implemented
- A button (and tool) for managing no favorite wallpapers has been implemented
- A button (and tool) for deleting wallpapers has been implemented

## Version 1.5
New Features:

- A new feature for changing the downloads directory has been implemented
- A progress bar is displayed to let the user watch the amount of space occupied within the downloads directory
- A warning system has been implemented. If the occupied space is higher than 90% (within the downloads directory), the user will be warned

## Version 1.1
New Features:

- A new button for copying downloads path into the clipboard has been added
- Some icons have been added
- DialogManager has been implemented in order to popup information dialogs on the screen

## Version 1.0
New Features:

- Only one provider implemented ([Wallhaven.cc](http://alpha.wallhaven.cc/))
- You can type several keywords to search for wallpapers (separated by ***;***)
- You can search specific resolution wallpapers or all resolutions available
- Search type implemented: Relevance, Date added, Views, Favorites, Random
- You can set the period of time to download the next wallpaper (1, 5, 10, 20, 30 min)
- You can set the maximum size for download directory (in MBytes)
- You can open directly from the application the directory where all the wallpapers are downloaded

## How do I get set up locally?

* [Prerequisites](https://bitbucket.org/eloy_garcia_pca/wallpaperdownloader/wiki/Prerequisites)
* [Cloning project](https://bitbucket.org/eloy_garcia_pca/wallpaperdownloader/wiki/Clonig%20project)
* [Setting up Wallpaper Downloader project in Eclipse](https://bitbucket.org/eloy_garcia_pca/wallpaperdownloader/wiki/SettingUp)
* [Deployment instructions](https://bitbucket.org/eloy_garcia_pca/wallpaperdownloader/wiki/Deployment)

## Who do I talk to?

* Repo owner and developer: <eloy.garcia.pca@gmail.com>
* For more information, please check my [portfolio web page at https://egara.github.io](https://egara.github.io)

## Issues

Please, report issues using [Wallpaper Downloader issue-tracking tool](https://bitbucket.org/eloy_garcia_pca/wallpaperdownloader/issues)
